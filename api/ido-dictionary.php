<?php
header("Content-Type:application/json");
require "data.php";

if( isset( $_GET['search'] ) )
{    
    $searchTerm = $_GET['search'];      // search term
    $searchType = $_GET['lookat'];      // english, ido, all
    $startsWith = $_GET['startswith'];  // true, false
    $wholeWord = $_GET['wholeword'];    // true, false
 
    if ( !isset( $dictionary ) )    { $dictionary = "ido-to-en"; }   
    if ( !isset( $searchType ) )    { $searchType = "all"; }
    if ( !isset( $startsWith ) )    { $startsWith = false; }
    else                            { $startsWith = ( $startsWith == "true" ); }
    if ( !isset( $wholeWord ) )     { $wholeWord = false; }
    else                            { $wholeWord = ( $wholeWord == "true" ); }
    
    $result = ido_dict_search( array( "search" => $searchTerm, "dictionary" => $dictionary, "lookat" => $searchType, "startswith" => $startsWith, "wholeword" => $wholeWord ) );
    
    if( empty( $result ) )
    {
        response( 200, "No results returned", NULL );
    }
    else
    {
        response( 200, "Result", $result );
    }
    
}
else
{
    response( 400, "Invalid Request", NULL );
}

function response( $status, $status_message, $data )
{
    header("HTTP/1.1 ".$status);
    
    $response['status']=$status;
    $response['status_message']=$status_message;
    $response['data']=$data;
    
    $json_response = json_encode($response);
    
    echo $json_response;
}
?>
