<?php

/*
 * Replace accented letters and capital letters
 * */
function formatString( $str )
{
    $str = strtolower( $str );
    $str = str_replace( "á", "a", $str );
    $str = str_replace( "é", "a", $str );
    $str = str_replace( "í", "a", $str );
    $str = str_replace( "ó", "a", $str );
    $str = str_replace( "ú", "a", $str );
    $str = str_replace( "-", "", $str );
    return $str;
}
 
/*
 * $options["term"]
 * $options["type"]     english, laadan, all
 * */
 
function laadan_dict_search( $options )
{
    $dictString = file_get_contents( 'http://www.ayadanconlangs.com/content/data/laadan-to-english.json' );
    $json = json_decode( $dictString, true );

    //print_r( $options );

    // Search the dictionary
    $results = array();
    $searchTerm = formatString( $options["search"] );
    foreach ( $json as $key => $data )
    {
        $compareStrings = array();
        if ( $options["lookat"] == "laadan" || $options["lookat"] == "láadan" )
        {
            array_push( $compareStrings, formatString( $data["láadan"] ) );
        }
        else if ( $options["lookat"] == "english" )
        {
            array_push( $compareStrings, formatString( $data["english"] ) );
        }
        else
        {
            array_push( $compareStrings, formatString( $data["láadan"] ) );
            array_push( $compareStrings, formatString( $data["english"] ) );
            array_push( $compareStrings, formatString( $data["description"] ) );
        }

        foreach ( $compareStrings as $compareString )
        {
            $pos = strpos( $compareString, $searchTerm );
            
            if ( $pos !== false )
            {
                //echo( "\n\n English: " . $data["english"] );
                //echo( "\n\t Pos: " . $pos );
                //echo( "\n\t\t startswith: " . $options["startswith"] . ", wholeword: " . $options["wholeword"] );
                //echo( "\n\t\t\t startswith == true: " . ($options["startswith"] == true) );
                //echo( "\n\t\t\t startswith == false: " . ($options["startswith"] == false) );
                //echo( "\n\t\t\t wholeword == true: " . ($options["wholeword"] == true) );
                //echo( "\n\t\t\t wholeword == false: " . ($options["wholeword"] == false) );
            
                // It's somewhere in the string
                if ( $options["startswith"] == false && $options["wholeword"] == false )
                {
                    // Just looking for CONTAINS
                    array_push( $results, $data ); break;
                }
                else if ( $options["startswith"] == true && $options["wholeword"] == false )
                {
                    // Just looking for STARTS WITH
                    if ( $pos == 0 )
                    {
                        array_push( $results, $data ); break;
                    }
                }
                else if ( $options["startswith"] == true && $options["wholeword"] == true )
                {
                    // Looking for STARTS WITH and WHOLE WORD
                    if ( $pos == 0 && strlen( $compareString ) == strlen( $searchTerm ) )
                    {
                        array_push( $results, $data ); break;
                    }
                }

                //echo( "\n\t\t store: " . $store );
                //echo( "\n\t\t result length: " . sizeof( $results ) );
            }
        }
    }

    array_push( $results, array( "credit", "laadanlanguage.org" ) );
    return $results;
}

function esperanto_dict_search( $options )
{
    $dictString = file_get_contents( 'http://www.ayadanconlangs.com/content/data/espdic.json' );
    $json = json_decode( $dictString, true );

    // Search the dictionary
    $results = array();
    $searchTerm = formatString( $options["search"] );

    foreach ( $json as $key => $data )
    {        
        $compareStrings = array();
        if ( $options["lookat"] == "esperanto" )
        {
            array_push( $compareStrings, formatString( $data["esperanto"] ) );
        }
        else if ( $options["lookat"] == "english" )
        {
            array_push( $compareStrings, formatString( $data["english"] ) );
        }
        else
        {
            array_push( $compareStrings, formatString( $data["esperanto"] ) );
            array_push( $compareStrings, formatString( $data["english"] ) );
        }

        foreach ( $compareStrings as $compareString )
        {            
            $pos = strpos( $compareString, $searchTerm );
            
            if ( $pos !== false )
            {
            
                // It's somewhere in the string
                if ( $options["startswith"] == false && $options["wholeword"] == false )
                {
                    // Just looking for CONTAINS
                    array_push( $results, $data ); break;
                }
                else if ( $options["startswith"] == true && $options["wholeword"] == false )
                {
                    // Just looking for STARTS WITH
                    if ( $pos == 0 )
                    {
                        array_push( $results, $data ); break;
                    }
                }
                else if ( $options["startswith"] == true && $options["wholeword"] == true )
                {
                    // Looking for STARTS WITH and WHOLE WORD
                    if ( $pos == 0 && strlen( $compareString ) == strlen( $searchTerm ) )
                    {
                        array_push( $results, $data ); break;
                    }
                }
            }
        }
    }

    array_push( $results, array( "credit", "ESPDIC (Esperanto – English Dictionary) – 2 August 2015 - Paul Denisowski (www.denisowski.org)" ) );
    return $results;
}


function ido_dict_search( $options )
{
    $dictString = file_get_contents( 'http://www.ayadanconlangs.com/content/data/' . $options["dictionary"] . '.json' );
    $json = json_decode( $dictString, true );

    // Search the dictionary
    $results = array();
    $searchTerm = formatString( $options["search"] );

    foreach ( $json as $key => $data )
    {        
        $compareStrings = array();
        if ( $options["lookat"] == "ido" )
        {
            array_push( $compareStrings, formatString( $data["ido"] ) );
        }
        else if ( $options["lookat"] == "english" )
        {
            array_push( $compareStrings, formatString( $data["english"] ) );
        }
        else
        {
            array_push( $compareStrings, formatString( $data["ido"] ) );
            array_push( $compareStrings, formatString( $data["english"] ) );
        }

        foreach ( $compareStrings as $compareString )
        {
            $pos = strpos( $compareString, $searchTerm );
            
            if ( $pos !== false )
            {            
                // It's somewhere in the string
                if ( $options["startswith"] == false && $options["wholeword"] == false )
                {
                    // Just looking for CONTAINS
                    array_push( $results, $data ); break;
                }
                else if ( $options["startswith"] == true && $options["wholeword"] == false )
                {
                    // Just looking for STARTS WITH
                    if ( $pos == 0 )
                    {
                        array_push( $results, $data ); break;
                    }
                }
                else if ( $options["startswith"] == true && $options["wholeword"] == true )
                {
                    // Looking for STARTS WITH and WHOLE WORD
                    if ( $pos == 0 && strlen( $compareString ) == strlen( $searchTerm ) )
                    {
                        array_push( $results, $data ); break;
                    }
                }
            }
        }
    }

    array_push( $results, array( "credit", "TBD" ) );
    return $results;
}
?>
