<!DOCTYPE html>
<head>
    <title>Ido Dictionary</title>
    
    <link href="http://ayadanconlangs.com/content/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="http://ayadanconlangs.com/content/bootstrap/js/bootstrap.min.js"></script>

    <script src="http://ayadanconlangs.com/content/jquery/jquery-3.3.1.min.js"></script>
    <script src="http://ayadanconlangs.com/content/js/expander.js"></script>

    <link href="http://ayadanconlangs.com/content/css/style.css" rel="stylesheet">
    <link href="http://ayadanconlangs.com/content/css/dashboard.css" rel="stylesheet">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Baloo+Bhaijaan|KoHo|Mali|PT+Sans+Narrow|Varela+Round" rel="stylesheet">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-22400179-13"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-22400179-13');
    </script>
</head>
<body>

<script>
    $( document ).ready( function() {
        $( "#ido-hide-results" ).click( function() {
            $( ".aya-ido-search-results" ).fadeOut( "fast" );
            $( "#ido-hide-results" ).fadeOut( "fast" );
        } );
        
        $( "#ido-ido-search-go" ).click( function() {
            var search = $( "#ido-ido-search" ).val();
            var lookat = "all";

            if ( $( "#ido-search-english" ).is(':checked') )
            {
                lookat = "english";
            }
            else if ( $( "#ido-search-ido" ).is(':checked') )
            {
                lookat = "ido"
            }
            else if ( $( "#ido-search-all" ).is(':checked') )
            {
                lookat = "all"
            }

            var apiString = "api/ido-dictionary.php?search=" + search + "&lookat=" + lookat;

            console.log( apiString );

            $.get( apiString, function( data ) {
                console.log( "Got result..." );
                console.log( data );
                $( ".aya-ido-search-results" ).empty();

                jQuery.each( data["data"], function( key, value ) {
                    if ( value[0] != "credit" ) {
                        var entryString = "<div class='entry'>";
                            entryString += "<p class='ido conlang'>" + value["ido"] + "</p>";
                            entryString += "<p class='english'>" + value["english"] + "</p>";
                        entryString += "</div>";

                        $( ".aya-ido-search-results" ).append( entryString );
                        console.log( value );
                    }
                } );
                
                $( ".aya-ido-search-results" ).fadeIn( "fast" );
                $( "#ido-hide-results" ).fadeIn( "fast" );

            } );
        } );
    } );
</script>

<div class="quick-search ido container">
    <h3>ido Dictionary</h3>

    <div class="row">
        <div class="col-md-12">
            <input type="text" id="ido-ido-search" class="form-control" placeholder="Enter text">
        </div>
    </div>
    
    <div class="row aya-spacer">&nbsp;</div>
    
    <div class="row">
        <div class="col-md-4">
            <input type="radio" id="ido-search-all" name="ido-search-what" value="all" checked="checked"> <label class="aya-small-tool-text" for="ido-search-all">All</label>
        </div>
        <div class="col-md-4">
            <input type="radio" id="ido-search-english" name="ido-search-what" value="english"> <label class="aya-small-tool-text" for="ido-search-english">English</label>
        </div>
        <div class="col-md-4">
            <input type="radio" id="ido-search-ido" name="ido-search-what" value="laadan"> <label class="aya-small-tool-text" for="ido-search-ido">Esperato</label>
        </div>
    </div>
    
    <div class="row aya-spacer">&nbsp;</div>
    
    <div class="row">
        <div class="col-md-12">
            <input type="button" id="ido-ido-search-go" value="Search" class="form-control btn-primary">
        </div>
    </div>

    <div class="row aya-spacer">&nbsp;</div>
    
    <div class="row">
        <div class="col-md-12">
            <input type="button" id="ido-hide-results" value="Hide results" class="form-control btn-primary" style="display: none;">
        </div>
    </div>
    
    <div class="row aya-spacer">&nbsp;</div>
    
    
<!--
    <div class="row view-all-link">
        <div class="col-md-12">
            <p><a href="http://ayadanconlangs.com/tools/laadan-dictionary/">Go to full dictionary &gt;&gt;</a></p>
        </div>
    </div>
-->

    <div class="row view-all-link">
        <div class="col-md-12">
            <p><a href="http://www.ayadanconlangs.com/tools/ido-vortolibro/">Go to full dictionary &gt;&gt;</a></p>
        </div>
    </div>
    
    <div class="aya-ido-search-results aya-search-results" style="display:none;">
        <div class="entry">
            <p class="ido conlang">qwerqwer</p>
            <p class="english">asdfasdf</p>
            <p class="description">zxcvzxcv</p>
        </div>
    </div>   
</div>

</body>
