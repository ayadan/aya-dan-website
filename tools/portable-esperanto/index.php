<!DOCTYPE html>
<head>
    <title>Esperanto Dictionary</title>
    
    <link href="http://ayadanconlangs.com/content/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="http://ayadanconlangs.com/content/bootstrap/js/bootstrap.min.js"></script>

    <script src="http://ayadanconlangs.com/content/jquery/jquery-3.3.1.min.js"></script>
    <script src="http://ayadanconlangs.com/content/js/expander.js"></script>

    <link href="http://ayadanconlangs.com/content/css/style.css" rel="stylesheet">
    <link href="http://ayadanconlangs.com/content/css/dashboard.css" rel="stylesheet">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Baloo+Bhaijaan|KoHo|Mali|PT+Sans+Narrow|Varela+Round" rel="stylesheet">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-22400179-13"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-22400179-13');
    </script>
</head>
<body>

<script>
$( function() {
    $( "#eo-result" ).css( "display", "none" );

    $( "#eo-type-box" ).keyup( function() {
        text = $( "#eo-type-box" ).val();
        formatted = text;
        formatted = replaceAll( "c'", '&#265;', formatted );
        formatted = replaceAll( "g'", '&#285;', formatted );
        formatted = replaceAll( "h'", '&#293;', formatted );
        formatted = replaceAll( "j'", '&#309;', formatted );
        formatted = replaceAll( "s'", '&scirc;', formatted );
        formatted = replaceAll( "u'", '&#365;', formatted );
        formatted = replaceAll( "C'", '&#264;', formatted );
        formatted = replaceAll( "G'", '&#284;', formatted );
        formatted = replaceAll( "H'", '&#292;', formatted );
        formatted = replaceAll( "J'", '&#308;', formatted );
        formatted = replaceAll( "S'", '&Scirc;', formatted );
        formatted = replaceAll( "U'", '&#364;', formatted );
        $( "#eo-result" ).html( formatted );
        $( "#eo-converted" ).val( $("#eo-result").html() );
    } );

    function replaceAll(find, replace, str) {
      return str.replace(new RegExp(find, 'g'), replace);
    }

} );

</script>

<div class="accenter-laadan container">
    <h3>Esperanto Letter Accenter</h3>
    <div class="row">
        <div class="col-md-12">
            <p>Put a ' after a vowel to auto-accent it: c', g', h', j', s', u'</p>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <textarea id="eo-type-box" class="form-control" placeholder="Type here"></textarea>
        </div>
    </div>

    <div class="row aya-spacer">&nbsp;</div>
    
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <p id="eo-result">&nbsp;</p>
            <textarea id="eo-converted" class="form-control" placeholder="Formatted text appears here"></textarea>
        </div>
    </div>
</div>


<script>
    $( document ).ready( function() {
        $( "#esperanto-hide-results" ).click( function() {
            $( ".aya-esperanto-search-results" ).fadeOut( "fast" );
            $( "#esperanto-hide-results" ).fadeOut( "fast" );
        } );
        
        $( "#espdic-esperanto-search-go" ).click( function() {
            var search = $( "#espdic-esperanto-search" ).val();
            var lookat = "all";

            if ( $( "#espdic-search-english" ).is(':checked') )
            {
                lookat = "english";
            }
            else if ( $( "#espdic-search-esperanto" ).is(':checked') )
            {
                lookat = "esperanto"
            }
            else if ( $( "#espdic-search-all" ).is(':checked') )
            {
                lookat = "all"
            }

            var apiString = "api/esperanto-dictionary.php?search=" + search + "&lookat=" + lookat;

            console.log( apiString );

            $.get( apiString, function( data ) {
                console.log( "Got result..." );
                console.log( data );
                $( ".aya-esperanto-search-results" ).empty();

                jQuery.each( data["data"], function( key, value ) {
                    if ( value[0] != "credit" ) {
                        var entryString = "<div class='entry'>";
                            entryString += "<p class='esperanto conlang'>" + value["esperanto"] + "</p>";
                            entryString += "<p class='english'>" + value["english"] + "</p>";
                        entryString += "</div>";

                        $( ".aya-esperanto-search-results" ).append( entryString );
                        console.log( value );
                    }
                } );
                
                $( ".aya-esperanto-search-results" ).fadeIn( "fast" );
                $( "#esperanto-hide-results" ).fadeIn( "fast" );

            } );
        } );
    } );
</script>

<div class="quick-search esperanto container">
    <h3>Esperanto Dictionary</h3>

    <div class="row">
        <div class="col-md-12">
            <input type="text" id="espdic-esperanto-search" class="form-control" placeholder="Enter text">
        </div>
    </div>
    
    <div class="row aya-spacer">&nbsp;</div>
    
    <div class="row">
        <div class="col-md-4">
            <input type="radio" id="espdic-search-all" name="eo-search-what" value="all" checked="checked"> <label class="aya-small-tool-text" for="eo-search-all">All</label>
        </div>
        <div class="col-md-4">
            <input type="radio" id="espdic-search-english" name="eo-search-what" value="english"> <label class="aya-small-tool-text" for="eo-search-english">English</label>
        </div>
        <div class="col-md-4">
            <input type="radio" id="espdic-search-esperanto" name="eo-search-what" value="laadan"> <label class="aya-small-tool-text" for="eo-search-esperanto">Esperato</label>
        </div>
    </div>
    
    <div class="row aya-spacer">&nbsp;</div>
    
    <div class="row">
        <div class="col-md-12">
            <input type="button" id="espdic-esperanto-search-go" value="Search" class="form-control btn-primary">
        </div>
    </div>

    <div class="row aya-spacer">&nbsp;</div>
    
    <div class="row">
        <div class="col-md-12">
            <input type="button" id="esperanto-hide-results" value="Hide results" class="form-control btn-primary" style="display: none;">
        </div>
    </div>
    
    <div class="row aya-spacer">&nbsp;</div>
    
    
<!--
    <div class="row view-all-link">
        <div class="col-md-12">
            <p><a href="http://ayadanconlangs.com/tools/laadan-dictionary/">Go to full dictionary &gt;&gt;</a></p>
        </div>
    </div>
-->

    <div class="row view-all-link">
        <div class="col-md-12">
            <p><a href="http://www.denisowski.org/Esperanto/ESPDIC/espdic_readme.html">Go to full dictionary &gt;&gt;</a></p>
        </div>
    </div>
    
    <div class="aya-esperanto-search-results aya-search-results" style="display:none;">
        <div class="entry">
            <p class="esperanto conlang">qwerqwer</p>
            <p class="english">asdfasdf</p>
            <p class="description">zxcvzxcv</p>
        </div>
    </div>   
</div>


</body>
