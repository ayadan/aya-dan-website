<?
$title = "Ido Quick Search Dictionary - By Rachel Singh";
?>
<? include_once( "../../content/layout/sub-sub-header.php" ); ?>
<? include_once( "../../content/php/functions.php" ); ?>
<? include_once( "backend.php" ); ?>
	  
	<div class="everything-container" style="margin: 0 auto; width: 90%;">
		<div class="container">
			<div class="row text-center">
				<h1>Ido Quick Search Dictionary</h1>
			</div>
		</div>
		
		<div class="container">
			<div class="row">
				<h2>Linguo <small>(Language)</small></h2>
				
				<select id="language" class="form-control">
					<option value="english_to_ido">English - Ido</option>
					<option value="ido_to_english">Ido - English</option>
					<option value="espanol_to_ido">Español - Ido</option>
					<option value="ido_to_espanol">Ido - Español</option>
					<option value="deutsch_to_ido">Deutsch - Ido</option>
					<option value="ido_to_deutsch">Ido - Deutsch</option>
					<option value="nihongo_to_ido">日本語 - Ido</option>
					<option value="ido_to_nihongo">Ido - 日本語</option>
					<option value="ido_to_russian">Ido - русский</option>
					<option value="ido_to_francais">Ido - Français</option>
					<option value="ido_to_italiano">Ido - Italiano</option>
					<option value="ido_to_portugues">Ido - Português</option>
					<option value="ido_to_nederlands">Ido - Nederlands</option>
					<option value="ido_to_suomi">Ido - Suomi</option>
					<option value="ido_to_esperanto">Ido - Esperanto</option>
					<option value="esperanto_to_ido">Esperanto - Ido</option>
					<option value="ido_to_interlingua">Ido - Interlingua</option>
					<option value="en_to_ido_phrases">English - Ido (phrases)</option>
				</select>
			</div>
			
			<div class="row">
				<h2>Vorti <small>(Words)</small></h2>
				<input type="text" id="searchText" class="form-control"/>
			</div>
			
			<div class="row">
				<h2>Maniero <small>(Method)</small></h2>
				
				<select id="method" class="form-control">
					<option value="starting">Word Beginning - Display only if beginning of entry matches</option>
					<option value="liberal">Full Search - Display if any part of the entry matches</option>
					<option value="regex">Regex - PHP Regex</option>
				</select>
			</div>
			
			<div class="row" style="margin-bottom:50px;"></div>
			
			<div class="row">
				<button id="search" class="form-control btn btn-primary">Serchar</button>
			</div>
		</div>
		
		<div class="row" style="margin-bottom:50px;"></div>
			
		<div class="row" style="margin-bottom:50px;"></div>

		<div class="container" id="results" style="margin: 0 auto; width: 90%;">
			<div class="row text-center">
				
			</div>
		</div>
		
		<div class="row">
			<p>Dictionaries:</p>
			<ul>
				<li><a href="http://www.idolinguo.org.uk/eniddyer.htm" title="">English - Ido</a></li>
				<li><a href="http://www.idolinguo.org.uk/idendyer.htm" title="">Ido - English</a></li>
				<li><a href="http://kanaria1973.ido.li/krayono/dicespido.html" title="">Español - Ido</a></li>
				<li><a href="http://kanaria1973.ido.li/krayono/dicidoesp.html" title="">Ido - Español</a></li>
				<li><a href="http://kanaria1973.ido.li/dicjaponaido.html" title="">日本語 - Ido</a></li>
				<li><a href="http://kanaria1973.ido.li/dicidojapona.html" title="">Ido - 日本語</a></li>
				<li><a href="http://idolinguo.org.uk/idger.htm" title="">Deutsch - Ido</a></li>
				<li><a href="http://kanaria1973.ido.li/dicidogermana.html" title="">Ido - Deutsch</a></li>
				<li><a href="http://kanaria1973.ido.li/dicidofranca.html" title="">Ido - Français</a></li>
				
				<li><a href="http://kanaria1973.ido.li/krayono/dicidorusa.html" title="">Ido - русский</a></li>
				<li><a href="http://kanaria1973.ido.li/dicidoitaliano.html" title="">Ido - Italiano</a></li>
				<li><a href="http://kanaria1973.ido.li/dicidoportugues.html" title="">Ido - Portugues</a></li>
				<li><a href="http://kanaria1973.ido.li/dicidonederlandana.html" title="">Ido - Nederlands</a></li>
				<li><a href="http://kanaria1973.ido.li/dicidofinlandana.html" title="">Ido - Suomi</a></li>
				<li><a href="http://kanaria1973.ido.li/dicidoesperanto.html" title="">Ido - Esperanto</a></li>
				<li>Esperanto - Ido (kanaria, but from wayback machine)</li>
				<li>Ido - Interlingua (kanaria, but from wayback machine)</li>
			</ul>
		</div>
		
	</div>

    <script src="content/js/jquery-2.1.4.min.js"></script>
    <script src="content/js/bootstrap.min.js"></script>
    <script>
		
	$( "#searchText" ).keydown( function( e ) {
		if ( e.which === 13 )
		{
			e.preventDefault();
			DoSearch();
		}
	} );
		
	$( "#search" ).click( function( e ) {
		e.preventDefault();
		DoSearch();	
	} ); 
	
	function DoSearch()
	{
		
		info = {
			language: $("#language").val(),
			text: $("#searchText").val(),
			method: $("#method").val()
			};
		
		$.get( "lookup.php", info, function( data ) {
				console.log( "Result" );
				console.log( data );			
			
				resultTable = $( "<table>" ).addClass( "table table-striped" );
				
				for ( var key in data )
				{					
					var headerTag = $( "<tr>" );
					$( "<th>" ).text( key ).addClass( "h2" ).appendTo( headerTag );
					$( headerTag ).appendTo( resultTable );
					
					data[key].forEach( function( translation ) {
						var trTag = $( "<tr>" );
						$( "<td>" ).text( translation ).appendTo( trTag );
						$( trTag ).appendTo( resultTable );
					} );
				}
				
				$( "#results" ).html( resultTable );
			
			}).fail( function() {
				console.log( "Failure" );
				
			} );
		
	}
    
    </script>
    
<? include_once( "../../content/layout/sub-sub-footer.php" ); ?>
