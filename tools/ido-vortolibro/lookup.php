<?
	include_once( "dictionary.php" );
	
	// $_GET["language"]
	// $_GET["text"]
	// $_GET["method"]
	
	$results = Array();
	
	$dict = new Dictionary();
	
	$dict->Initialize( Array(
		"language" => $_GET["language"]
		) );
		
	$results = $dict->FindMatches( Array( 
		"type" => $_GET["method"], 
		"search" => $_GET["text"] 
		) );
		
	if ( count( $results ) == 0 )
	{
		$results[0] = Array( 
			0 => "Nuli trovi por \"" . $_GET["text"] . "\". (No results)" 
		);
	}

	$json = json_encode( $results );
	
	header( 'Content-Type: application/json' );
	echo( $json );
?>
