<?
$title = "Láadan Translator - By Rachel Singh";
$sidebar = "laadan-reference-sidebar";
?>
<? include_once( "../../content/layout/sub-sub-header.php" ); ?>
<? include_once( "../../content/php/functions.php" ); ?>

<script>
$( document ).ready( function() {
    function sanitize( str ) {
        str = str.toLowerCase();
        str = str.replace( "?", "" );
        str = str.replace( "!", "" );
        str = str.replace( ",", "" );
        str = str.replace( ".", "" );
        return str;
    }
    
    $( "#translate-from-laadan" ).click( function() {
        var text = $( "#laadan-input" ).val();
        console.log( "Translate:", text );

        var values = text.split(" ");
        $( ".laadan-to-english-results" ).empty();

        $.each( values, function( key, value ) {

            // Place in order
            $( ".laadan-to-english-results" ).append( "<li><strong>" + value + "</strong><ul class='" + sanitize( value ) + "'></ul></li>" );
            
            console.log( key, value, sanitize( value ) );
            var apiString = "../../api/laadan-dictionary.php?search=" + sanitize( value ) + "&lookat=laadan&startswith=true&wholeword=true";

            $.get( apiString, function( data ) {
                
                console.log( "Got result..." );
                console.log( data );

                $( "#laadan-text" ).html( text );

                var listString = "";

                var atLeastOne = false;
                jQuery.each( data["data"], function( key, value ) {
                    atLeastOne = true;
                    listString += "<li><em>" + value["láadan"] + "</em> = <em>" + value["english"] + "</em>";
                    if ( value["description"] != "" ) {
                        listString += "; " + value["description"];
                    }
                    listString += "</li>";
                } );

                if ( atLeastOne == false )
                {
                    // Try to split up the word?
                    listString += "<li>This is probably a compound word; try splitting it up (add spaces in the word). Suggestions...<ul>";
                    listString += "<li>-(e)th = object marker</li>"
                    listString += "<li>-lh- = evil/bad marker</li>"
                    listString += "<li>wo- = adjective marker</li>"
                    listString += "<li>me- = plural marker</li>"
                    listString += "</ul></li>";
                }

                $( "." + sanitize( value ) ).append( listString );
            } );
        } );
    } );
} );
</script>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Láadan Translator</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h2>Láadan to English</h2>
                <p>Type in some Láadan to get a breakdown of the text.</p>

                <textarea id="laadan-input" class="form-control"></textarea>
                <br>
                <input type="button" class="btn-primary btn form-control" id="translate-from-laadan" value="Translate">

                <hr>

                <h3>Breakdown</h3>
                <p>Láadan: <span id="laadan-text"></span></p>
                <ul class="laadan-to-english-results">
                </ul>
            </div>
        </div>
    </div>

<? include_once( "../../content/layout/sub-sub-footer.php" ); ?>
