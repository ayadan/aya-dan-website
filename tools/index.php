<? $title = "Community"; ?>
<? include_once( "../content/layout/sub-header.php" ); ?>

<div class="container">

    <div class="row"> <h1>Tools</h1> </div>

    <div class="row">
        <div class="quick-nav"> Quick Nav:
        <a href="#laadan">Láadan</a>
        <a href="#ido">Ido</a>
        <a href="#apis">APIs</a>
        <a href="#os">Open Source</a> </div>
    </div>

    <a name="laadan"></a><h2>Láadan Tools</h2>

    <div class="row laadan-tolls">
        <div class="col-md-6 row">
            <div class="col-md-3">
                <a href="http://ayadanconlangs.com/tools/laadan-dictionary/"><img src="../content/images/webpage/big-icon-laadan-dictionary.png"></a>
            </div>
            <div class="col-md-9">
                <h3><a href="http://ayadanconlangs.com/tools/laadan-dictionary/">Láadan Quick Search Dictionary &raquo;</a></h3>
                <p>Useful quick-search dictionary for Láadan.</p>
            </div>
        </div>
        
        <div class="col-md-6 row">
            <div class="col-md-3">
                <a href="http://ayadanconlangs.com/tools/laadan-accenter/"><img src="../content/images/webpage/big-icon-laadan-accenter.png"></a>
            </div>
            <div class="col-md-9">
                <h3><a href="http://ayadanconlangs.com/tools/laadan-accenter/">Láadan Letter Accenter &raquo;</a></h3>
                <p>Type text in with ' to auto-accent letters to work with Láadan</p>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-6 row">
            <div class="col-md-3">
                <a href="http://ayadanconlangs.com/tools/laadan-translator/"><img src="../content/images/webpage/big-icon-laadan-translator.png"></a>
            </div>
            <div class="col-md-9">
                <h3><a href="http://ayadanconlangs.com/tools/laadan-translator/">Láadan-to-English Translator &raquo;</a></h3>
                <p>Translates each word in a Láadan phrase to English</p>
            </div>
        </div>
        
        <div class="col-md-6 row">
            <div class="col-md-3">
                <a href="http://ayadanconlangs.com/tools/sentence-builder/"><img src="../content/images/webpage/big-icon-laadan-sentence.png"></a>
            </div>
            <div class="col-md-9">
                <h3><a href="http://ayadanconlangs.com/tools/sentence-builder/">Láadan Sentence Builder (WIP) &raquo;</a></h3>
                <p>A tool to build basic, grammatically-correct Láadan sentences.</p>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-6 row">
            <div class="col-md-3">
                <a href="http://ayadanconlangs.com/tools/calendar.html"><img src="../content/images/webpage/big-icon-laadan-calendar.png"></a>
            </div>
            <div class="col-md-9">
                <h3><a href="http://ayadanconlangs.com/tools/calendar.html">Láadan Calendar Generator &raquo;</a></h3>
                <p>A tool to generate a calendar in Láadan for any year.</p>
            </div>
        </div>
        
        <div class="col-md-6 row">
            <div class="col-md-3">
                <a href="http://ayadanconlangs.com/tools/poetry-generator.html"><img src="../content/images/webpage/big-icon-laadan-poetry.png"></a>
            </div>
            <div class="col-md-9">
                <h3><a href="http://ayadanconlangs.com/tools/poetry-generator.html">Láadan Poetry Generator &raquo;</a></h3>
                <p>Randomly generate silly Láadan poems.</p>
            </div>
        </div>
    </div>


    <hr>

    <a name="ido"></a><h2>Ido Tools</h2>

    <div class="row laadan-tolls">
        <div class="col-md-6 row">
            <div class="col-md-3">
                <a href="http://ayadanconlangs.com/tools/ido-vortolibro/"><img src="../content/images/webpage/big-icon-ido-dictionary.png"></a>
            </div>
            <div class="col-md-9">
                <h3><a href="http://ayadanconlangs.com/tools/ido-vortolibro/">Ido Dictionary &raquo;</a></h3>
                <p>A quick-search dictionary for Ido with support for multiple languages.</p>
            </div>
        </div>
    </div>

    <hr><a name="apis"><h2>APIs</h2></a>

    <p>You can find the API code in the <a href="https://bitbucket.org/ayadan/aya-dan-website/src/master/api/data.php">repository here</a>.</p>

    <h3><img src="../content/images/webpage/icon-laadan.png" > Láadan Dictionary</h3>

    <p><strong>URL form:</strong></p>

    <pre>http://www.ayadanconlangs.com/api/laadan-dictionary.php?search=love&lookat=english&startswith=true&wholeword=true</pre>

    <p><strong>Parameters:</strong></p>

    <table class="table">
        <tr><th>Parameter</th><th>Possible values</th><th>Default, if omitted</th><th>Description</th></tr>
        <tr>
            <td><span class="typewriter">search</span></td>
            <td>Anything</td>
            <td>None</td>
            <td>The search term/text you're looking up.</td>
        </tr>
        <tr>
            <td><span class="typewriter">lookat</span></td>
            <td><span class="typewriter">english</span>, <span class="typewriter">laadan</span>, or <span class="typewriter">all</span></td>
            <td><span class="typewriter">all</span></td>
            <td>Whether to search through the English or Láadan columns, or English, Láadan, and the Description (All).</td>
        </tr>
        <tr>
            <td><span class="typewriter">startswith</span></td>
            <td><span class="typewriter">true</span> or <span class="typewriter">false</span></td>
            <td><span class="typewriter">false</span></td>
            <td>Whether to only return results that <em>start with</em> the search term (i.e., &quot;cat&quot; returns &quot;CATerpillar&quot; but not &quot;abdiCATe&quot;)</td>
        </tr>
        <tr>
            <td><span class="typewriter">wholeword</span></td>
            <td><span class="typewriter">true</span> or <span class="typewriter">false</span></td>
            <td><span class="typewriter">false</span></td>
            <td>Whether to only return results that contain only the whole word and no more (i.e. &quot;cat&quot; returning only &quot;cat&quot;). Must be used with <span class="typewriter">startswith=true</span></td>
        </tr>
        <tr>
        </tr>
    </table>

    <p><strong>Credit:</strong> Dictionary from laadanlanguage.wordpress.com, by Suzette Haden Elgin and others</p>

    <hr>
    
    <h3><img src="../content/images/webpage/icon-esperanto.png"> Esperanto Dictionary</h3>

    <p><strong>URL form:</strong></p>

    <pre>http://www.ayadanconlangs.com/api/esperanto-dictionary?search=kato&lookat=esperanto&startswith=false&wholeword=false</pre>

    <p><strong>Parameters:</strong></p>

    <table class="table">
        <tr><th>Parameter</th><th>Possible values</th><th>Default, if omitted</th><th>Description</th></tr>
        <tr>
            <td><span class="typewriter">search</span></td>
            <td>Anything</td>
            <td>None</td>
            <td>The search term/text you're looking up.</td>
        </tr>
        <tr>
            <td><span class="typewriter">lookat</span></td>
            <td><span class="typewriter">english</span>, <span class="typewriter">esperanto</span>, or <span class="typewriter">all</span></td>
            <td><span class="typewriter">all</span></td>
            <td>Whether to search through the English or Esperanto columns, or search both columns.</td>
        </tr>
        <tr>
            <td><span class="typewriter">startswith</span></td>
            <td><span class="typewriter">true</span> or <span class="typewriter">false</span></td>
            <td><span class="typewriter">false</span></td>
            <td>Whether to only return results that <em>start with</em> the search term (i.e., &quot;cat&quot; returns &quot;CATerpillar&quot; but not &quot;abdiCATe&quot;)</td>
        </tr>
        <tr>
            <td><span class="typewriter">wholeword</span></td>
            <td><span class="typewriter">true</span> or <span class="typewriter">false</span></td>
            <td><span class="typewriter">false</span></td>
            <td>Whether to only return results that contain only the whole word and no more (i.e. &quot;cat&quot; returning only &quot;cat&quot;). Must be used with <span class="typewriter">startswith=true</span></td>
        </tr>
        <tr>
        </tr>
    </table>

    <p><strong>Credit:</strong> Dictionary from <a href="http://www.denisowski.org/Esperanto/ESPDIC/espdic_readme.html">http://www.denisowski.org/Esperanto/ESPDIC/espdic_readme.html</a> by Paul Denisowski is licensed under a Creative Commons Attribution 3.0 Unported License.</p>

    <hr>
    
    <h3><img src="../content/images/webpage/icon-ido.png"> Ido Dictionary</h3>

    <p><strong>URL form:</strong></p>

    <pre>http://www.ayadanconlangs.com/api/ido-dictionary?search=cat&lookat=esperanto&dictionary=ido-to-en&startswith=false&wholeword=false</pre>

    <p><strong>Parameters:</strong></p>

    <table class="table">
        <tr><th>Parameter</th><th>Possible values</th><th>Default, if omitted</th><th>Description</th></tr>
        <tr>
            <td><span class="typewriter">search</span></td>
            <td>Anything</td>
            <td>None</td>
            <td>The search term/text you're looking up.</td>
        </tr>
        <tr>
            <td><span class="typewriter">dictionary</span></td>
            <td><span class="typewriter">ido-to-en</span></td>
            <td><span class="typewriter">ido-to-en</span></td>
            <td>The dictionary to look at</td>
        </tr>
        <tr>
            <td><span class="typewriter">lookat</span></td>
            <td>
                <span class="typewriter">ido</span>, second language name, or <span class="typewriter">all</span>
                <br>Other languages: <span class="typewriter">english</span>
            </td>
            <td><span class="typewriter">all</span></td>
            <td>Whether to search through Ido columns, the other language columns, or search both columns.</td>
        </tr>
        <tr>
            <td><span class="typewriter">startswith</span></td>
            <td><span class="typewriter">true</span> or <span class="typewriter">false</span></td>
            <td><span class="typewriter">false</span></td>
            <td>Whether to only return results that <em>start with</em> the search term (i.e., &quot;cat&quot; returns &quot;CATerpillar&quot; but not &quot;abdiCATe&quot;)</td>
        </tr>
        <tr>
            <td><span class="typewriter">wholeword</span></td>
            <td><span class="typewriter">true</span> or <span class="typewriter">false</span></td>
            <td><span class="typewriter">false</span></td>
            <td>Whether to only return results that contain only the whole word and no more (i.e. &quot;cat&quot; returning only &quot;cat&quot;). Must be used with <span class="typewriter">startswith=true</span></td>
        </tr>
        <tr>
        </tr>
    </table>

    <p><strong>Credit:</strong> </p>

    <ul>
        <li>English - Ido from http://www.idolinguo.org.uk/eniddyer.htm</li>
        <li>Ido - English from http://www.idolinguo.org.uk/idendyer.htm</li>
        <li>Español - Ido from http://kanaria1973.ido.li/krayono/dicespido.html</li>
        <li>Ido - Español from http://kanaria1973.ido.li/krayono/dicidoesp.html</li>
        <li>日本語 - Ido from http://kanaria1973.ido.li/dicjaponaido.html</li>
        <li>Ido - 日本語 from http://kanaria1973.ido.li/dicidojapona.html</li>
        <li>Deutsch - Ido from http://idolinguo.org.uk/idger.htm</li>
        <li>Ido - Deutsch from http://kanaria1973.ido.li/dicidogermana.html</li>
        <li>Ido - Français from http://kanaria1973.ido.li/dicidofranca.html</li>
        <li>Ido - русский from http://kanaria1973.ido.li/krayono/dicidorusa.html</li>
        <li>Ido - Italiano from http://kanaria1973.ido.li/dicidoitaliano.html</li>
        <li>Ido - Portugues from http://kanaria1973.ido.li/dicidoportugues.html</li>
        <li>Ido - Nederlands from http://kanaria1973.ido.li/dicidonederlandana.html</li>
        <li>Ido - Suomi from http://kanaria1973.ido.li/dicidofinlandana.html</li>
        <li>Ido - Esperanto from http://kanaria1973.ido.li/dicidoesperanto.html</li>
        <li>Esperanto - Ido (kanaria, but from wayback machine)</li>
        <li>Ido - Interlingua (kanaria, but from wayback machine)</li>
    </ul>



    <hr><a name="os"><h2>Open Source</h2></a>

    <h3>Áya Dan Project Page</h3>

    <p><a href="https://bitbucket.org/ayadan/">https://bitbucket.org/ayadan/</a></p>

    <p>The Áya Dan project page contains all of the conlang projects created by Rachel Singh.</p>

    <h3>Specific Repositories</h3>

    <ul>
        <li><strong><a href="https://bitbucket.org/ayadan/laadan-dictionary/src/master/">laadan-dictionary</a></strong> - Láadan Dictionary in various formats</li>
        <li><strong><a href="https://bitbucket.org/ayadan/laadan-helper">laadan-helper</a></strong> - Láadan helper app</li>
        <li><strong><a href="https://bitbucket.org/ayadan/laadan-library/src">laadan-library</a></strong> - Written Láadan content</li>
        <li><strong><a href="https://bitbucket.org/ayadan/laadan-twitter-bot/src/master/">laadan-twitter-bot</a></strong> - My old Twitter bot that tweets sample phrases in Láadan</li>
        <li><strong><a href="https://bitbucket.org/ayadan/nia-ido-dictionary/src/master/">nia-ido-dictionary</a></strong> - The Nia Ido dictionary</li>
        <li><strong><a href="https://bitbucket.org/ayadan/nia-ido-android/src/master/">nia-ido-android</a></strong> - The Android helper app</li>
    </ul>
</div>


<? include_once( "../content/layout/sub-footer.php" ); ?>

