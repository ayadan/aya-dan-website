
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.png">

    <title>Láadan Sentence Builder - by Rachel Singh</title>
    <script src="content/jquery-2.2.4/jquery-2.2.4.min.js"></script>
        
    <link href="content/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- <link href="content/bootstrap-3.3.6-dist/css/ie10-viewport-bug-workaround.css" rel="stylesheet"> -->
    <!-- <link href="content/jumbotron-narrow.css" rel="stylesheet"> -->
    <!-- <script src="content/bootstrap-3.3.6-dist/js/ie-emulation-modes-warning.js"></script> -->

    <style type="text/css">
        .word-bit { margin: 10px; border-radius: 5px; padding: 5px; text-align: center; font-size: 0.8em; background: #ddd; border: solid 1px #000000aa;
            margin-right: 10px; width: 10%; vertical-align: center; display: block; float: left; }
        .sentence-types li {clear: both; }
        .jumbotron p { margin: 0; padding: 0; font-size: 1em; font-weight: normal; }

        .word-bit.sa    { background: #ffcccc; border-color: #ff5858; }
        .word-bit.aux   { background: #ffeecc; border-color: #b76401; }
        .word-bit.verb  { background: #fffecc; border-color: #aaac14; }
        .word-bit.neg   { background: #c3ff92; border-color: #4fa50a; }
        .word-bit.cps   { background: #bdfff0; border-color: #0aa582; }
        .word-bit.cp2   { background: #bde8ff; border-color: #0e5190; }
        .word-bit.cp3   { background: #bdbdff; border-color: #0e0e90; }
        .word-bit.ev    { background: #e7b2ff; border-color: #7914a8; }
        .word-bit.embed { background: #b500b3; border-color: #490048; color: #ffffff; font-style: italic; }

        #laadan-to-english select { font-size: 10pt; }
    </style>
  </head>

  <body>

    <? include_once( "backend.php" ); ?>
    
    <script type="text/javascript">
        $( function() {

            function StringContains( fullString, subString )
            {
                return ( fullString.indexOf( subString ) >= 0 );
            }

            function CheckSpeechAct( laadanSentence, englishSentence )
            {
                var speechAct   = $('.laadan-speech-act').find(":selected");
                if ( speechAct.text() == "Speech Act Morpheme" ) return;
                laadanSentence["speechact"] = speechAct.attr( "laadan" );
                englishSentence["speechact"] = speechAct.attr( "english" );
            }

            function CheckTense( laadanSentence, englishSentence )
            {
                var tense       = $('.laadan-tense').find(":selected");
                if ( tense.text() == "Tense" ) return;
                laadanSentence["tense"] = tense.attr( "laadan" );
                englishSentence["tense"] = tense.attr( "english" );
            }

            function CheckVerb( laadanSentence, englishSentence )
            {
                var verbT        = $('.laadan-tverb').find(":selected");
                var verbI        = $('.laadan-iverb').find(":selected");
                
                if ( verbT == "Transitive Verb" && verbI == "Intransitive Verb" ) return;

                if ( verbT.text() == "Transitive Verb" && verbI.text() != "Intransitive Verb" )
                {
                    // Use intransitive
                    laadanSentence["verb"] = verbI.attr( "laadan" );
                    englishSentence["verb"] = verbI.attr( "english" );
                }
                else if ( verbT.text() != "Transitive Verb" && verbI.text() == "Intransitive Verb" )
                {
                    // Use transitive
                    laadanSentence["verb"] = verbT.attr( "laadan" );
                    englishSentence["verb"] = verbT.attr( "english" );
                }
            }

            function CheckNegation( laadanSentence, englishSentence )
            {
                var negation    = $('.laadan-negation').find(":selected");
                if ( typeof negation === "undefined" ) return;
                if ( negation.text() == "No negation" )
                {
                    laadanSentence["negation"] = "";
                    englishSentence["negation"] = "";
                }
                else
                {
                    laadanSentence["negation"] = "ra";
                    englishSentence["negation"] = "not";
                }
            }

            function CheckSubject( laadanSentence, englishSentence )
            {
                var subject     = $('.laadan-subject').find(":selected");
                if ( subject.text() == "Case Phrase-Subject" ) return;
                laadanSentence["subject"] = subject.attr( "laadan" );
                englishSentence["subject"] = subject.attr( "english" );
            }

            function CheckEvidence( laadanSentence, englishSentence )
            {
                var evidence    = $('.laadan-evidence').find(":selected");
                if ( evidence.text() == "Evidence" ) return;
                laadanSentence["evidence"] = evidence.attr( "laadan" );
                englishSentence["evidence"] = evidence.attr( "english" );
            }

            function BuildLaadanSentence( ls )
            {
                var negation = false;
                var question = false;
                
                if ( ls["negation"] != "" )
                {
                    negation = true;
                }

                if ( ls["speechact"] == "báa" )
                {
                    question = true;
                }
                
                var laadanString =  ls["speechact"] + " " +
                                    ls["tense"] + " " +
                                    ls["verb"] + " ";

                if ( negation )
                {
                    laadanString += ls["negation"] + " ";
                }
                
                laadanString += ls["subject"];
                if ( question )
                {
                    laadanString += "?";
                }
                else
                {
                    laadanString += " " + ls["evidence"] + "."
                }
                
                return laadanString;
            }
            
            function BuildEnglishSentence( es )
            {
                var englishString = es["speechact"] + ": ";
                var properNoun = false;
                var negation = false;
                var question = false;
                
                if ( es["subject"] == "I" || es["subject"] == "you" )
                {
                    properNoun = true;
                }
                else if ( es["subject"] == "she/it/he" )
                {
                    properNoun = true;
                    es["subject"] = "they";
                }

                if ( es["negation"] != "" )
                {
                    negation = true;
                }

                if ( es["speechact"] == "I ask" )
                {
                    question = true;
                }

                if ( question )
                {
                    if ( properNoun )   { englishString += "do " + es["subject"] + " "; }
                    else                { englishString += "does the " + es["subject"] + " "; }
                    
                    if ( properNoun && negation )   { englishString += "not "; }
                    else if ( negation )            { englishString += "not "; }
                    
                    if ( properNoun )       { englishString += es["verb"].replace( "to ", "" ) + " "; }
                    else if ( negation )    { englishString += es["verb"].replace( "to ", "" ) + " "; }
                    else                    { englishString += es["verb"].replace( "to ", "" ) + " "; }
                }
                else
                {
                    if ( properNoun )   { englishString += es["subject"] + " "; }
                    else                { englishString += "the " + es["subject"] + " "; }
                    
                    if ( properNoun && negation )   { englishString += "don't "; }
                    else if ( negation )            { englishString += "doesn't "; }
                    
                    if ( properNoun )       { englishString += es["verb"].replace( "to ", "" ) + " "; }
                    else if ( negation )    { englishString += es["verb"].replace( "to ", "" ) + " "; }
                    else                    { englishString += es["verb"].replace( "to ", "" ) + "s "; }
                }
                
                englishString +=    es["tense"];

                if ( question )
                {
                    englishString += "?";
                }
                else
                {
                    englishString += "; " + es["evidence"] + ".";
                }
                
                return englishString;
            }

            $( "select" ).change( function() {
    
                var laadanSentence = { "speechact" : "", "tense" : "", "verb" : "", "negation" : "", "subject" : "", "evidence" : "", };
                var englishSentence = { "speechact" : "", "tense" : "", "verb" : "", "negation" : "", "subject" : "", "evidence" : "", };

                CheckSpeechAct( laadanSentence, englishSentence );
                CheckTense( laadanSentence, englishSentence );
                CheckVerb( laadanSentence, englishSentence );
                CheckNegation( laadanSentence, englishSentence );
                CheckSubject( laadanSentence, englishSentence );
                CheckEvidence( laadanSentence, englishSentence );

                console.log( englishSentence, laadanSentence );

                var englishString = BuildEnglishSentence( englishSentence );
                var laadanString = BuildLaadanSentence( laadanSentence );

                console.log( englishString, laadanString );

                $( ".laadan-sentence" ).html( laadanString );
                $( ".english-sentence" ).html( englishString );
            } );
            
            
        } ); // ready
        
    </script>

    <div class="container" style="width:95%;">
        <div class="header clearfix">
            <h3 class="text-muted">Láadan Sentence Builder</h3>
            <p>By Rachel Wil Sha Singh</p>
            <p><a href="https://bitbucket.org/ayadan/laadan-sentence-builder/">BitBucket Repository</a></p>
        </div>

        <div class="row sentence-builder">
            <div class="col-md-12">
                <div class="row">
                    <h3>Form 1: The [SUBJECT] [VERBS]</h3>

                    <span class="word-bit sa">
                        Speech-Act
                        <select class="laadan-speech-act form-control">
                            <option>Speech Act Morpheme</option>
                            <? foreach( $laadan_speechact_json as $index=>$entry ) { ?>
                                <option laadan="<?=$entry["láadan"]?>" english="<?=$entry["english"]?>"><?=$entry["láadan"]?> = <?=$entry["english"]?></option>
                            <? } ?>
                        </select>
                    </span>
                    
                    <span class="word-bit aux">
                        Tense
                        <select class="laadan-tense form-control">
                            <option>Tense</option>
                            <? foreach( $laadan_tense_json as $index=>$entry ) { ?>
                                <option laadan="<?=$entry["láadan"]?>" english="<?=$entry["english"]?>"><?=$entry["láadan"]?> = <?=$entry["english"]?></option>
                            <? } ?>
                        </select>
                    </span>
                    <span class="word-bit verb">
                        Verb
                        <select class="form-control laadan-tverb">
                            <option>Transitive Verb</option>
                            <? foreach( $laadan_transverbs_json as $index=>$entry ) { ?>
                                <option laadan="<?=$entry["láadan"]?>" english="<?=$entry["english"]?>"><?=$entry["láadan"]?> = <?=$entry["english"]?></option>
                            <? } ?>
                        </select>
                        <p>or</p>
                        <select class="form-control laadan-iverb">
                            <option>Intransitive Verb</option>
                            <? foreach( $laadan_inverbs_json as $index=>$entry ) { ?>
                                <option laadan="<?=$entry["láadan"]?>" english="<?=$entry["english"]?>"><?=$entry["láadan"]?> = <?=$entry["english"]?></option>
                            <? } ?>
                        </select>
                    </span>
                    <span class="word-bit neg">
                        (Negation)
                        <select class="form-control laadan-negation">
                            <option>No negation</option>
                            <option laadan="ra" english="not">ra = non</option>
                        </select>
                    </span>
                    <span class="word-bit cps">
                        Case-Phrase-Subject
                        <select class="laadan-subject form-control">
                            <option>Case Phrase-Subject</option>
                            <? foreach( $laadan_pronouns_json as $index=>$entry ) { ?>
                                <option laadan="<?=$entry["láadan"]?>" english="<?=$entry["english"]?>">
                                <?=$entry["láadan"]?> = <?=$entry["english"]?> (<?=$entry["description"]?>)
                                </option>
                            <? } ?>
                            <? foreach( $laadan_nouns_json as $index=>$entry ) { ?>
                                <option laadan="<?=$entry["láadan"]?>" english="<?=$entry["english"]?>"><?=$entry["láadan"]?> = <?=$entry["english"]?></option>
                            <? } ?>
                        </select>
                    </span>
                    <span class="word-bit ev">
                        (Evidence)
                        <select class="form-control laadan-evidence">
                            <option>Evidence</option>
                            <? foreach( $laadan_evidence_json as $index=>$entry ) { ?>
                                <option laadan="<?=$entry["láadan"]?>" english="<?=$entry["english"]?>"><?=$entry["láadan"]?> = <?=$entry["english"]?></option>
                            <? } ?>
                        </select>
                    </span>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <p>Láadan</p>
                        <textarea class="laadan-sentence form-control">
                        </textarea>
                    </div>
                    <div class="col-md-6">
                        <p>English</p>
                        <textarea class="english-sentence form-control">
                        </textarea>
                    </div>
                </div>

                <hr>

<!--
                <div class="row">
                    <h3>Form 2: The SUBJECT VERBS a NOUN</h3>

                    <div class="col-md-2">
                        <select id="laadan-speech-act" class="form-control">
                            <option>Speech Act Morpheme</option>
                            <? foreach( $laadan_speechact_json as $index=>$entry ) { ?>
                                <option><?=$entry["láadan"]?> = <?=$entry["english"]?></option>
                            <? } ?>
                        </select>
                        <p>What kind of sentence?</p>
                    </div>
                    
                    <div class="col-md-1">
                        <select id="laadan-speech-act" class="form-control">
                            <option>Tense</option>
                            <? foreach( $laadan_tense_json as $index=>$entry ) { ?>
                                <option><?=$entry["láadan"]?> = <?=$entry["english"]?></option>
                            <? } ?>
                        </select>
                        <p>When?</p>
                    </div>
                    
                    <div class="col-md-2">
                        <select id="laadan-speech-act" class="form-control">
                            <option>Transitive Verb</option>
                            <? foreach( $laadan_transverbs_json as $index=>$entry ) { ?>
                                <option><?=$entry["láadan"]?> = <?=$entry["english"]?></option>
                            <? } ?>
                        </select>
                        <p>or</p>
                        <select id="laadan-speech-act" class="form-control">
                            <option>Intransitive Verb</option>
                            <? foreach( $laadan_inverbs_json as $index=>$entry ) { ?>
                                <option><?=$entry["láadan"]?> = <?=$entry["english"]?></option>
                            <? } ?>
                        </select>
                    </div>
                    
                    <div class="col-md-1">
                        <select id="laadan-speech-act" class="form-control">
                            <option>No negation</option>
                            <option>ra = non</option>
                        </select>
                    </div>
                    
                    <div class="col-md-2">
                        <select id="laadan-speech-act" class="form-control">
                            <option>Case Phrase-Subject</option>
                            <? foreach( $laadan_pronouns_json as $index=>$entry ) { ?>
                                <option><?=$entry["láadan"]?> = <?=$entry["english"]?></option>
                            <? } ?>
                            <? foreach( $laadan_nouns_json as $index=>$entry ) { ?>
                                <option><?=$entry["láadan"]?> = <?=$entry["english"]?></option>
                            <? } ?>
                        </select>
                        <p>Subject</p>
                    </div>
                    
                    <div class="col-md-2">
                        <select id="laadan-speech-act" class="form-control">
                            <option>Case Phrase-Object</option>
                            <? foreach( $laadan_pronouns_json as $index=>$entry ) { ?>
                                <option><?=$entry["láadan"]?> = <?=$entry["english"]?></option>
                            <? } ?>
                            <? foreach( $laadan_nouns_json as $index=>$entry ) { ?>
                                <option><?=$entry["láadan"]?> = <?=$entry["english"]?></option>
                            <? } ?>
                        </select>
                        <p>Object</p>
                    </div>
                    
                    <div class="col-md-6">
                        <p>Láadan</p>
                        <textarea class="laadan-sentence form-control">
                        </textarea>
                    </div>
                    <div class="col-md-6">
                        <p>English</p>
                        <textarea class="english-sentence form-control">
                        </textarea>
                    </div>
                </div>

                <div class="row">
                    <h3>Form 3: The SUBJECT VERBS TO-VERB</h3>
                    
                    <div class="col-md-6">
                        <p>Láadan</p>
                        <textarea class="laadan-sentence form-control">
                        </textarea>
                    </div>
                    <div class="col-md-6">
                        <p>English</p>
                        <textarea class="english-sentence form-control">
                        </textarea>
                    </div>
                </div>
-->

            </div>
        </div>
        

        <div class="row">  
          <div class="col-md-12">
              <h2>Other Láadan sentence forms...</h2>
              <p>(Coming later?)</p>

              <ul class="sentence-types">
                  <li><span class="word-bit sa">Speech-Act</span> <span class="word-bit aux">(Auxiliary)</span> <span class="word-bit verb">Verb</span> <span class="word-bit neg">(Negation)</span> <span class="word-bit cps">Case-Phrase-Subject</span> <span class="word-bit cp2">Case-Phrase-Object</span> <span class="word-bit ev">(Evidence)</span></li>
                  <li><span class="word-bit sa">Speech-Act</span> <span class="word-bit aux">(Auxiliary)</span> <span class="word-bit verb">Verb</span> <span class="word-bit neg">(Negation)</span> <span class="word-bit cps">Case-Phrase-Subject</span> <span class="word-bit cp2">(Case-Phrase-Object)</span> <span class="word-bit cp3">Case-Phrase-Source</span> <span class="word-bit ev">(Evidence)</span></li>
                  <li><span class="word-bit sa">Speech-Act</span> <span class="word-bit aux">(Auxiliary)</span> <span class="word-bit verb">Verb</span> <span class="word-bit neg">(Negation)</span> <span class="word-bit cps">Case-Phrase-Subject</span> <span class="word-bit cp2">(Case-Phrase-Object)</span> <span class="word-bit cp3">Case-Phrase-Instrument</span> <span class="word-bit ev">(Evidence)</span></li>
                  <li><span class="word-bit sa">Speech-Act</span> <span class="word-bit aux">(Auxiliary)</span> <span class="word-bit verb">Verb</span> <span class="word-bit neg">(Negation)</span> <span class="word-bit cps">Case-Phrase-Subject</span> <span class="word-bit cp2">Case-Phrase-Associate</span> <span class="word-bit ev">(Evidence)</span></li>
                  <li><span class="word-bit sa">Speech-Act</span> <span class="word-bit aux">(Auxiliary)</span> <span class="word-bit verb">Verb</span> <span class="word-bit neg">(Negation)</span> <span class="word-bit cps">Case-Phrase-Subject</span> <span class="word-bit cp2">Case-Phrase-Beneficiary</span> <span class="word-bit ev">(Evidence)</span></li>
                  <li><span class="word-bit sa">Speech-Act</span> <span class="word-bit aux">(Auxiliary)</span> <span class="word-bit verb">Verb</span> <span class="word-bit neg">(Negation)</span> <span class="word-bit cps">Case-Phrase-Subject</span> <span class="word-bit cp2">Case-Phrase-Time</span> <span class="word-bit ev">(Evidence)</span></li>
                  <li><span class="word-bit sa">Speech-Act</span> <span class="word-bit aux">(Auxiliary)</span> <span class="word-bit verb">Verb</span> <span class="word-bit neg">(Negation)</span> <span class="word-bit cps">Case-Phrase-Subject</span> <span class="word-bit cp2">Case-Phrase-Place</span> <span class="word-bit ev">(Evidence)</span></li>
                  <li><span class="word-bit sa">Speech-Act</span> <span class="word-bit aux">(Auxiliary)</span> <span class="word-bit verb">Verb</span> <span class="word-bit neg">(Negation)</span> <span class="word-bit cps">Case-Phrase-Subject</span> <span class="word-bit cp2">Case-Phrase-Identifier</span> <span class="word-bit ev">(Evidence)</span></li>
                  <li><span class="word-bit sa">Speech-Act</span> <span class="word-bit aux">(Auxiliary)</span> <span class="word-bit verb">Verb</span> <span class="word-bit neg">(Negation)</span> <span class="word-bit cps">Case-Phrase-Subject</span> <span class="word-bit cp2">Case-Phrase-Manner</span> <span class="word-bit ev">(Evidence)</span></li>
                  <li><span class="word-bit sa">Speech-Act</span> <span class="word-bit aux">(Auxiliary)</span> <span class="word-bit verb">Verb</span> <span class="word-bit neg">(Negation)</span> <span class="word-bit cps">Case-Phrase-Subject</span> <span class="word-bit cp2">Case-Phrase-Cause</span> <span class="word-bit ev">(Evidence)</span></li>
                  <li><span class="word-bit sa">Speech-Act</span> <span class="word-bit aux">(Auxiliary)</span> <span class="word-bit verb">Verb</span> <span class="word-bit neg">(Negation)</span> <span class="word-bit cps">Case-Phrase-Subject</span> <span class="word-bit cp2">Case-Phrase-Possessive</span> <span class="word-bit ev">(Evidence)</span></li>

                  <li><span class="word-bit sa">Speech-Act</span> <span class="word-bit aux">(Auxiliary)</span> <span class="word-bit verb">Verb</span> <span class="word-bit neg">(Negation)</span> <span class="word-bit cps">Case-Phrase-Subject</span> <span class="word-bit embed">Embedded Declarative Sentence</span><span class="word-bit"><em>-hé</em></span> <span class="word-bit ev">(Evidence)</span></li>
                  <li><span class="word-bit sa">Speech-Act</span> <span class="word-bit aux">(Auxiliary)</span> <span class="word-bit verb">Verb</span> <span class="word-bit neg">(Negation)</span> <span class="word-bit cps">Case-Phrase-Subject</span> <span class="word-bit embed">Embedded Question Sentence</span><span class="word-bit"><em>-hée</em></span> </li>
                  <li><span class="word-bit sa">Speech-Act</span> <span class="word-bit aux">(Auxiliary)</span> <span class="word-bit verb">Verb</span> <span class="word-bit neg">(Negation)</span> <span class="word-bit cps">Case-Phrase-Subject</span> <span class="word-bit embed">Embedded Relative Clause</span><span class="word-bit"><em>-háa</em></span> <span class="word-bit ev">(Evidence)</span> </li>
              </ul>
          </div>
      </div>
      
      <hr>

      <footer class="footer">
        <p>Programmed by Rachel Wil Sha Singh (Rachel@Moosader.com)</p>
      </footer>

    </div> <!-- /container -->


  </body>
</html>
