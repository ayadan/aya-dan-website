<?

// Load the dictionary
// laadan-to-english.json

$laadan_speechact = file_get_contents( 'content/laadan-speechact.json' );
$laadan_speechact_json = json_decode( $laadan_speechact, true );

$laadan_evidence = file_get_contents( 'content/laadan-evidence.json' );
$laadan_evidence_json = json_decode( $laadan_evidence, true );

$laadan_tense = file_get_contents( 'content/laadan-tense.json' );
$laadan_tense_json = json_decode( $laadan_tense, true );

$laadan_inverbs = file_get_contents( 'content/laadan-intransitive-verbs.json' );
$laadan_inverbs_json = json_decode( $laadan_inverbs, true );

$laadan_transverbs = file_get_contents( 'content/laadan-transitive-verbs.json' );
$laadan_transverbs_json = json_decode( $laadan_transverbs, true );

$laadan_nouns = file_get_contents( 'content/laadan-nouns.json' );
$laadan_nouns_json = json_decode( $laadan_nouns, true );

$laadan_pronouns = file_get_contents( 'content/laadan-pronouns.json' );
$laadan_pronouns_json = json_decode( $laadan_pronouns, true );

?>
