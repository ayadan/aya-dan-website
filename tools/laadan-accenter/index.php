<?
$title = "Láadan Letter Accenter - By Rachel Singh";
?>
<? include_once( "../../content/layout/sub-sub-header.php" ); ?>
<? include_once( "../../content/php/functions.php" ); ?>
<? include_once( "backend.php" ); ?>

    <script src="converter.js"></script>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Láadan Letter Accenter</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-sm-12">
            <p>Put a ' after a vowel to auto-accent it: A', E', I', O', U'</p>
            <textarea id="type-box" class="form-control"></textarea>
        </div>
        <div class="col-md-6 col-sm-12">
            <p>Formatted:</p>
            <p id="result">&nbsp;</p>
            <textarea id="converted" class="form-control"></textarea>
        </div>
    </div>
</div>

<? include_once( "../../content/layout/sub-sub-footer.php" ); ?>
