$( function() {
	$( "#result" ).css( "display", "none" );
	
	$( "#type-box" ).keyup( function() {
		text = $( "#type-box" ).val();
		formatted = text;
		formatted = replaceAll( "a'", '&aacute;', formatted );
		formatted = replaceAll( "e'", '&eacute;', formatted );
		formatted = replaceAll( "i'", '&iacute;', formatted );
		formatted = replaceAll( "o'", '&oacute;', formatted );
		formatted = replaceAll( "u'", '&uacute;', formatted );
		formatted = replaceAll( "A'", '&Aacute;', formatted );
		formatted = replaceAll( "E'", '&Eacute;', formatted );
		formatted = replaceAll( "I'", '&Iacute;', formatted );
		formatted = replaceAll( "O'", '&Oacute;', formatted );
		formatted = replaceAll( "U'", '&Uacute;', formatted );
		$( "#result" ).html( formatted );
		$( "#converted" ).val( $("#result").html() );
	} );
	
	function replaceAll(find, replace, str) {
	  return str.replace(new RegExp(find, 'g'), replace);
	}

} );

