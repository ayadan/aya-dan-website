<!DOCTYPE html>
<head>
    <title>Láadan Dictionary</title>
    
    <link href="http://ayadanconlangs.com/content/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="http://ayadanconlangs.com/content/bootstrap/js/bootstrap.min.js"></script>

    <script src="http://ayadanconlangs.com/content/jquery/jquery-3.3.1.min.js"></script>
    <script src="http://ayadanconlangs.com/content/js/expander.js"></script>

    <link href="http://ayadanconlangs.com/content/css/style.css" rel="stylesheet">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Baloo+Bhaijaan|KoHo|Mali|PT+Sans+Narrow|Varela+Round" rel="stylesheet">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-22400179-13"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-22400179-13');
    </script>
</head>
<body>

<script>
$( function() {
    $( "#result" ).css( "display", "none" );

    $( "#type-box" ).keyup( function() {
        text = $( "#type-box" ).val();
        formatted = text;
        formatted = replaceAll( "a'", '&aacute;', formatted );
        formatted = replaceAll( "e'", '&eacute;', formatted );
        formatted = replaceAll( "i'", '&iacute;', formatted );
        formatted = replaceAll( "o'", '&oacute;', formatted );
        formatted = replaceAll( "u'", '&uacute;', formatted );
        formatted = replaceAll( "A'", '&Aacute;', formatted );
        formatted = replaceAll( "E'", '&Eacute;', formatted );
        formatted = replaceAll( "I'", '&Iacute;', formatted );
        formatted = replaceAll( "O'", '&Oacute;', formatted );
        formatted = replaceAll( "U'", '&Uacute;', formatted );
        $( "#result" ).html( formatted );
        $( "#converted" ).val( $("#result").html() );
    } );

    function replaceAll(find, replace, str) {
      return str.replace(new RegExp(find, 'g'), replace);
    }

    $( "#laadan-hide-results" ).click( function() {
            $( ".aya-laadan-search-results" ).fadeOut( "fast" );
            $( "#laadan-hide-results" ).fadeOut( "fast" );
        } );
        
        $( "#laadan-search-go" ).click( function() {
            var search = $( "#laadan-search" ).val();
            var lookat = "all";

            if ( $( "#search-english" ).is(':checked') )
            {
                lookat = "english";
            }
            else if ( $( "#search-laadan" ).is(':checked') )
            {
                lookat = "laadan"
            }

            var apiString = "api/laadan-dictionary.php?search=" + search + "&lookat=" + lookat;

            console.log( apiString );

            $.get( apiString, function( data ) {
                console.log( "Got result..." );
                console.log( data );
                $( ".aya-laadan-search-results" ).empty();

                jQuery.each( data["data"], function( key, value ) {
                    if ( value[0] != "credit" ) {
                        var entryString = "<div class='entry'>";
                            entryString += "<p class='laadan conlang'>" + value["láadan"] + "</p>";
                            entryString += "<p class='english'>" + value["english"] + "</p>";
                            entryString += "<p class='description'>" + value["description"] + "</p>";
                        entryString += "</div>";

                        $( ".aya-laadan-search-results" ).append( entryString );
                        console.log( value );
                    }
                } );
                
                $( ".aya-laadan-search-results" ).fadeIn( "fast" );
                $( "#laadan-hide-results" ).fadeIn( "fast" );

            } );
        } );

} );

</script>

<div class="container-fluid">
    <div class="laadan-letter-accenter">
        <div class="row">
            <div class="col-md-12">
                <h2>Láadan Letter Accenter</h2>
                <p>Put a ' after a vowel to auto-accent it: A', E', I', O', U'</p>

            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-sm-12">
                <p><textarea id="type-box" class="form-control" placeholder="Type here"></textarea></p>
            </div>
            <div class="col-md-6 col-sm-12">
                <p><textarea id="converted" class="form-control" placeholder="Formatted text appears here"></textarea></p>
            </div>
        </div>
    </div>

    <div class="row">
        <hr>
    </div>

    <div class="laadan-quick-search-dictionary">
        <div class="row">
            <div class="col-md-12">
                <h2>Láadan Quick-Search Dictionary</h2>
            </div>
        </div>
    </div>
</div>
<!--
        <div class="col-md-12">
            <h3>Láadan Letter Accenter</h3>
            
        </div>
        
        <div class="row">
            <div class="col-md-12 col-sm-12">
                
            </div>
        </div>

        <div class="row aya-spacer">&nbsp;</div>
        
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <p id="result">&nbsp;</p>
                
            </div>
        </div>
    </div>

    <div class="row quick-search laadan">
        <h3>Láadan Dictionary</h3>

        <div class="row">
            <div class="col-md-12">
                <input type="text" id="laadan-search" class="form-control" placeholder="Enter text">
            </div>
        </div>
        
        <div class="row aya-spacer">&nbsp;</div>
        
        <div class="row">
            <div class="col-md-4">
                <input type="radio" id="search-all" name="laadan-search-what" value="all" checked="checked"> <label class="aya-small-tool-text" for="laadan-search-all">All</label>
            </div>
            <div class="col-md-4">
                <input type="radio" id="search-english" name="laadan-search-what" value="english"> <label class="aya-small-tool-text" for="laadan-search-english">English</label>
            </div>
            <div class="col-md-4">
                <input type="radio" id="search-laadan" name="laadan-search-what" value="laadan"> <label class="aya-small-tool-text" for="laadan-search-laadan">Láadan</label>
            </div>
        </div>
        
        <div class="row aya-spacer">&nbsp;</div>
        
        <div class="row">
            <div class="col-md-12">
                <input type="button" id="laadan-search-go" value="Search" class="form-control btn-primary">
            </div>
        </div>

        <div class="row aya-spacer">&nbsp;</div>
        
        <div class="row">
            <div class="col-md-12">
                <input type="button" id="laadan-hide-results" value="Hide results" class="form-control btn-primary" style="display: none;">
            </div>
        </div>
        
        <div class="row aya-spacer">&nbsp;</div>
        
        <div class="row view-all-link">
            <div class="col-md-12">
                <p><a href="http://ayadanconlangs.com/tools/laadan-dictionary/">Go to full dictionary &gt;&gt;</a></p>
            </div>
        </div>

        <div class="aya-laadan-search-results aya-search-results" style="display:none;">
            <div class="entry">
                <p class="laadan conlang">qwerqwer</p>
                <p class="english">asdfasdf</p>
                <p class="description">zxcvzxcv</p>
            </div>
        </div> 
    </div>
</div>

-->

</body>
