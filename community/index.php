<?
    $title = "Community";
    include_once( "../content/php/functions.php" );
    include_once( "../content/themes/2019-01/header.php" );
?>

<?
function LinkCommunity( $url, $icon, $text ) {
?>

    <p class="preview-image"><a href="<?=$url?>"><img src="<?=$icon?>"></a></p>
    <p>
        <span class="title"><a href="<?=$url?>"><?=$text?></a></span>
    </p>
<?
}
?>

<section class="section link-preview preview-pane cf">
    <div class="row-container">
        <div class="header-tab col-20">
            <div class="contents">
                <h2>Áya Dan Communities</h2>
            </div>
        </div>
        <div class="row-container col-80 cf">
            <div class="col-33">
                <div class="contents">
                    <? LinkCommunity( "https://discord.gg/HUzh5EF", "../content/images/webpage/icon-discord.png", "The Moosader Discord server, with conlang channels" ); ?>
                </div>
            </div>
            <div class="col-33">
                <div class="contents">
                    <? LinkCommunity( "http://www.ayadanconlangs.com/forums/", "../content/images/webpage/icon-forums.png", "Multi-Conlang Message Board" ); ?>
                </div>
            </div>
            <div class="col-33">
                <div class="contents">
                    <? LinkCommunity( "https://www.facebook.com/ayadanconlangs/", "../content/images/webpage/icon-facebook.png", "Facebook Page" ); ?>
                </div>
            </div>
        </div>
    </div>
</section>

&nbsp;

<section class="section link-preview preview-pane cf">
    <div class="row-container">
        <div class="header-tab col-20">
            <div class="contents">
                <h2>Communities Elseware</h2>
            </div>
        </div>
        <div class="row-container col-80 cf">
            <div class="col-33">
                <div class="contents">
                    <? LinkCommunity( "https://telegramo.org/", "../content/images/webpage/icon-telegram.png", "Telegramo - Esperanto Telegram channels" ); ?>
                </div>
            </div>
        </div>
    </div>
</section>



<? include_once( "http://www.ayadanconlangs.com/content/themes/2019-01/footer.php" ); ?>
