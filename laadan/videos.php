<!-- ----------------------- -->
<!-- - VIDEOS -------------- -->
<!-- ----------------------- -->

<?
$videos = Storage::GetVideos();

function VideoPreview( $video, $language ) {
    $url = "http://www.ayadanconlangs.com/videos/?video_id=" . $video['YouTube ID'] . "&video_language=" . $language;
?>
    <div class="col-25">
        <div class="contents">
            <p class="preview-image"><a href="<?=$url?>"><img src="https://img.youtube.com/vi/<?=$video['YouTube ID']?>/default.jpg"></a></p>
            <p>
                <span class="title"><a href="<?=$url?>"><?=$video['name']?></a></span>
                <span class="author">by <?=$video['creator']?>,</span>
                <span class="date"><?=Storage::YMDToDate($video['date'] )?></span>
            </p>
        </div>            
    </div>
<?
}
?>

<section class="section preview-pane cf">
    <div class="row-container">
        <div class="header-tab col-20">
            <div class="contents">
                <a name="about"></a><h2>Videos</h2>
                <p><a href="http://www.ayadanconlangs.com/videos/?language=laadan">View all &gt;&gt;</a></p>
            </div>
        </div>
        <div class="row-container col-80 cf">
            <?
                $i = 0;
                foreach ( $videos as $key=>$language ) {
                    if ( $pageLanguage != "" && $key != $pageLanguage ) { continue; }
                    foreach ( $language["videos"] as $video ) {
                        VideoPreview( $video, "laadan" );
                        $i++;
                        if ( $i != 0 && $i % 4 == 0 ) { break; }
                    } // foreach ( $language["videos"] as $video )
                } // foreach ( $videos as $key=>$language )
            ?>
        </div>
    </div>
</section>
