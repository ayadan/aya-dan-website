<section class="section preview-pane cf">
    <div class="row-container">
        <div class="header-tab col-20">
            <div class="contents">
                <a name="about"></a><h2>Archive</h2>
            </div>
        </div>
        <div class="row-container col-80 cf">
            <div class="contents">
                <p>Webpages and resources for Láadan have been disappearing over the years,
                so recovered items are stored on the Áya Dan server and hosted here.</p>

                <table class="table">
                    <thead>
                        <tr>
                            <th>Name</th><th>Type</th><th>Description</th><th>Last available</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><a href="http://www.ayadanconlangs.com/archive/amberwind/index.html">Amberwind's Láadan Lessons (Web)</a></td>
                            <td>Website</td>
                            <td>Very thorough lessons on Láadan grammar and vocabulary.</td>
                            <td>2015</td>
                        </tr>

                        <tr>
                            <td><a href="http://www.ayadanconlangs.com/archive/downloads/amberwind-laadan-lessons-all.pdf">Amberwind's Láadan Lessons (PDF)</a></td>
                            <td>PDF</td>
                            <td>Very thorough lessons on Láadan grammar and vocabulary.</td>
                            <td>2015</td>
                        </tr>

                        <tr>
                            <td><a href="http://www.ayadanconlangs.com/archive/jackie-powers/index.html">Jackie Powers' Láadan Language Reference Page</a></td>
                            <td>Website</td>
                            <td>An affiliate website that hosted some information on Láadan.</td>
                            <td>2008</td>
                        </tr>

                        <tr>
                            <td><a href="http://www.ayadanconlangs.com/archive/laadan-language-2012/index.html">Original Láadan Language website</a></td>
                            <td>Website</td>
                            <td>The original Láadan website</td>
                            <td>2012</td>
                        </tr>
                        
                        <tr>
                            <td><a href="http://www.ayadanconlangs.com/archive/downloads/first-dictionary_partial.pdf">A First Dictionary and Grammar of Láadan</a></td>
                            <td>Book</td>
                            <td>The out-of-print book in PDF form (no dictionary)</td>
                            <td>1988</td>
                        </tr>

                        <tr>
                            <td><a href="http://www.ayadanconlangs.com/archive/downloads/wiscon6-talk.pdf">Why a Woman is Not Like a Physicist: WisCon 6, March 6 1982</a></td>
                            <td>Article</td>
                            <td>A talk by Suzette Haden Elgin, preceeding her creation of Láadan.</td>
                            <td>1982</td>
                        </tr>

                        <tr>
                            <td><a href="http://www.ayadanconlangs.com/archive/the-communipaths/index.html">The Communipaths (Web)</a></td>
                            <td>Book</td>
                            <td>The first book in the Coyote Jones series, originally made available free online.</td>
                            <td>2016</td>
                        </tr>

                        <tr>
                            <td><a href="http://www.ayadanconlangs.com/archive/downloads/the-communipaths_suzette-haden-elgin_maverynthia.epub">The Communipaths (EPUB)</a></td>
                            <td>Book</td>
                            <td>The first book in the Coyote Jones series, originally made available free online.</td>
                            <td>2016</td>
                        </tr>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

