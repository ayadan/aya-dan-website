<?
    $title = "The Native Tongue Series (1984 - 1993)";
    include_once( "../content/php/functions.php" );
    include_once( "../content/themes/2019-01/header.php" );
?>

<!-- ----------------------- -->
<!-- - ABOUT --------------- -->
<!-- ----------------------- -->

<section class="section video-preview preview-pane cf">
    <div class="row-container">
        <div class="header-tab col-20">
            <div class="contents">
                <h2>About the Series</h2>
            </div>
        </div>
        <div class="row-container col-80">
            <div class="contents">
                <p>
                    Work in progress; I've only read books 1 and 2.
                </p>
<!--
                <a href="../content/images/laadan/Ozark.jpg"><img style="margin: 10px; float:right; width: 300px;" src="../content/images/laadan/Ozark.jpg" title="A map of the world of Ozark" style="width:100%;"></a>
-->
                
            </div>
        </div>
    </div>
</section>

<!-- ----------------------- -->
<!-- - BOOKS --------------- -->
<!-- ----------------------- -->

<section class="section video-preview preview-pane cf">
    <div class="row-container">
        <div class="header-tab col-20">
            <div class="contents">
                <h2>Books in the Series</h2>
            </div>
        </div>
        <div class="row-container col-80">
            <div class="contents">

                <hr style="clear:both;">

                <h3>Native Tongue</h3>
                <img style="margin: 10px; width:200px; float:right;" src="../content/images/laadan/nt-nativetongue.jpg" title="Book cover of Native Tongue">
                <p>From the back cover...</p>
                <blockquote>
                    <p>With the defeat of the Equal Rights Amendment in 1982, the women's movement received its first serious setback.
                    In 1991, with the passage of the 25th Amendment, the women's movement received its death blow.
                    Because that incredible amendment rolled back women's rights two hundred years and assured the supremacy of males in every aspect of life.</p>

                    <p>This is a novel of the cold war between the sexes as it developed in the centuries that followed.
                    It is a highly controversial novel, sure to arouse charged emotions in readers of both sexes.
                    Suzette Haden Elgin, science fiction author and proessor of linguistics, combines her talents to tell a vivid story of people in a future society where interplanetary trade had made
                    language-study a necessity - and thereby handed the so-called &quot;weaker&quot; sex a weapon for liberation... if they dared use it.</p>
                </blockquote>

                <hr style="clear:both;">

                <h3>The Judas Rose</h3>
                <img style="margin: 10px; width:200px; float:right;" src="../content/images/laadan/nt-judasrose.jpg" title="Book cover of The Judas Rose">
                <p>From the back cover...</p>
                <blockquote>
                    <p>IN LANGUAGE LIES POWER!</p>

                    <p>And on a future Earth where genetically bred linguists hold the key to the planet's economic survival because
                    only they can serve as translators between human and alien traders,
                    language has, indeed, become the way to power.
                    But what this future Earth's male-dominated society does not yet realize is that ordinary
                    women as well as linguists can wield this weapons of the mind.</p>
                    <p>And while mankind vies to claim its place in an alien-ruled universe,
                    womankind strives in a unique battle back on Earth, spreading the knowledge
                    of Láadan, the secret language for women. For in Láadan lies women's one hope
                    for regaining their freedom - and for saving all humankind!</p>
                </blockquote>

                <hr style="clear:both;">

                <h3>Earthsong</h3>
                <img style="margin: 10px; width:200px; float:right;" src="../content/images/laadan/nt-earthsong.jpg" title="Book cover of Earthsong">
                <p>From the back cover...</p>
                <blockquote>
                    <p>THE WOMEN OF THE LINGUIST LINES</p>

                    <p>On a future Earth where economic survival depends on communication and trade
                    with alien species, linguistics has taken on a power and meaning unknown to us today.
                    In this world thirteen families of brilliant, genetically bred linguists,
                    trained from birth in nonhuman language, hold the key to Earth's economic survival
                    because only they can provide translations during alien trade summits.</p>

                    <p>Yet this is also a world where the 25th Amendment, which denies women equal rights,
                    has plunged civilization into a repressive dark age. Women are once again considered
                    property - useful only for procreation and menial chores. Only the women of the
                    Linguist Lines, whose talents are considered too valuable to waste, have ever
                    been allowed to do anything beyond basic domestic work.</p>

                    <p>But when the aliens suddenly abandon Earth, taking their technology with them,
                    and plunging the Earth into economic disaster, can the women of the Linguist Lines,
                    wo have long planned for the liberation of their sex, now seize the power to save their world?</p>
                </blockquote>

                <hr style="clear:both;">
                
            </div>
        </div>
    </div>
</section>


<!-- ----------------------- --> 
<!-- - CHARACTERS ---------- -->
<!-- ----------------------- -->

<!--
<section class="section video-preview preview-pane cf">
    <div class="row-container">
        <div class="header-tab col-20">
            <div class="contents">
                <h2>Characters</h2>
            </div>
        </div>
        <div class="row-container col-80">
            <div class="contents">
                <h3>Notable Characters</h3>

                <h4></h4>
            </div>
        </div>
    </div>
</section>
-->




<!-- ----------------------- -->
<!-- - CULTURE ------------- -->
<!-- ----------------------- -->

<!--
<section class="section video-preview preview-pane cf">
    <div class="row-container">
        <div class="header-tab col-20">
            <div class="contents">
                <h2>Culture</h2>
            </div>
        </div>
        <div class="row-container col-80">
            <div class="contents">
                <h3>The Twelve Families of Earth</h3>

                <p>
                    The Twelve Families of Earth are people from around the Ozarks area of the United States of America.
                    In 2012, they left the planet in search of a new home.
                </p>
            </div>
        </div>
    </div>
</section>
-->


<!-- ----------------------- -->
<!-- - GALLERY ------------- -->
<!-- ----------------------- -->

<?
function GalleryPreview( $image, $title, $description ) {
?>
    <div class="col-25">
        <div class="contents">
            <p class="preview-image"><a href="<?=$image?>"><img src="<?=$image?>"></a></p>
            <p>
                <span class="title"><a href="<?=$image?>"><?=$title?></a></span>
                <span class="description"><?=$description?></span>
            </p>
        </div>
    </div>
<?
}
?>

<!--
<section class="section video-preview preview-pane cf">
    <div class="row-container">
        <div class="header-tab col-20">
            <div class="contents">
                <h2>Gallery</h2>
            </div>
        </div>
        <div class="row-container col-80">
            <div class="contents">
                <h3>Art from Suzette's Page</h3>
                <div class="row-container cf">
                    <? // GalleryPreview( "../content/images/laadan/mule_sfwa.org.gif", "", "" ); ?>
                </div>
            </div>
        </div>
    </div>
</section>
-->


<!-- ----------------------- -->
<!-- - LINKS --------------- -->
<!-- ----------------------- -->

<section class="section video-preview preview-pane cf">
    <div class="row-container">
        <div class="header-tab col-20">
            <div class="contents">
                <h2>Links</h2>
            </div>
        </div>
        <div class="row-container col-80">
            <div class="contents">
                <ul>
                    <li>Goodreads Links
                        <ul>
                            <li><a href="https://www.goodreads.com/book/show/285563.Native_Tongue">Native Tongue (Book 1)</a></li>
                            <li><a href="https://www.goodreads.com/book/show/285565.The_Judas_Rose">The Judas Rose (Book 2)</a></li>
                            <li><a href="https://www.goodreads.com/book/show/886000.Earthsong">Earthsong (Book 3)</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>



<? include_once( "http://www.ayadanconlangs.com/content/themes/2019-01/footer.php" ); ?>
