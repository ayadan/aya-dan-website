<?
    $title = "Láadan";
    include_once( "../content/php/functions.php" );
    include_once( "../content/themes/2019-01/header.php" );
?>

<? 
$pageLanguage = "laadan";
?>

<div class="container">
    <div class="row">
        <div class="col-md-12">

<!--
            <div class="row">
                <div class="quick-nav"> Quick Nav:
                <a href="#videos">Videos</a>
                <a href="#blog">Blog</a>
                <a href="#tools">Tools</a>
                <a href="#music">Music</a> 
                
                <a href="#archive">Archive</a>
                <a href="#links">Links</a> 
                <a href="#books">Suzette's Books</a> 
                <a href="#about">About</a>
                </div>
            </div>
-->
            <? include_once( "videos.php" ); ?>
            <? include_once( "blog.php" ); ?>
            <? include_once( "learn.php" ); ?>
            <? include_once( "tools.php" ); ?>
            <? include_once( "entertainment.php" ); ?>
            <? include_once( "archive.php" ); ?>
            <? include_once( "links.php" ); ?>
            <? include_once( "books.php" ); ?>
            <? include_once( "about.php" ); ?>
        </div>
    </div>
</div>

<? include_once( "http://www.ayadanconlangs.com/content/themes/2019-01/footer.php" ); ?>
