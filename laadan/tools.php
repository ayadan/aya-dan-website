<!-- ----------------------- -->
<!-- - TOOLS --------------- -->
<!-- ----------------------- -->

<?
function ToolPreview( $url, $icon, $title, $description ) {
?>
    <div class="col-33">
        <div class="contents">
            <p class="preview-image"><a href="<?=$url?>"><img src="<?=$icon?>"></a></p>
            <p>
                <span class="title"><a href="<?=$url?>"><?=$title?></a></span>
                <span class="description"><?=$description?></span>
            </p>
        </div>
    </div>
<?
}
?>

<section class="section preview-pane cf">
    <div class="row-container">
        <div class="header-tab col-20">
            <div class="contents">
                <a name="about"></a><h2>Tools</h2>
            </div>
        </div>
        <div class="row-container col-80 cf">
            <? ToolPreview( "http://ayadanconlangs.com/tools/laadan-dictionary/", "../content/images/webpage/big-icon-laadan-dictionary.png", "Láadan Quick Search Dictionary", "Useful quick-search dictionary for Láadan." ); ?>
            <? ToolPreview( "http://ayadanconlangs.com/tools/laadan-accenter/", "../content/images/webpage/big-icon-laadan-accenter.png", "Láadan Letter Accenter", "Type text in with ' to auto-accent letters to work with Láadan" ); ?>
            <? ToolPreview( "http://ayadanconlangs.com/tools/laadan-translator/", "../content/images/webpage/big-icon-laadan-translator.png", "Láadan-to-English Translator", "Translates each word in a Láadan phrase to English" ); ?>
        <div class="row-container col-80 cf"></div>
            <? ToolPreview( "http://ayadanconlangs.com/tools/sentence-builder/", "../content/images/webpage/big-icon-laadan-sentence.png", "Láadan Sentence Builder (WIP)", "A tool to build basic, grammatically-correct Láadan sentences." ); ?>
            <? ToolPreview( "http://ayadanconlangs.com/tools/calendar.html", "../content/images/webpage/big-icon-laadan-calendar.png", "Láadan Calendar Generator", "A tool to generate a calendar in Láadan for any year." ); ?>
            <? ToolPreview( "http://ayadanconlangs.com/tools/poetry-generator.html", "../content/images/webpage/big-icon-laadan-poetry.png", "Láadan Poetry Generator", "Randomly generate silly Láadan poems." ); ?>
        </div>
    </div>
</section>

