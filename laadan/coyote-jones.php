<?
    $title = "The Coyote Jones Series (1970 - 1986)";
    include_once( "../content/php/functions.php" );
    include_once( "../content/themes/2019-01/header.php" );
?>

<!-- ----------------------- -->
<!-- - ABOUT --------------- -->
<!-- ----------------------- -->

<section class="section video-preview preview-pane cf">
    <div class="row-container">
        <div class="header-tab col-20">
            <div class="contents">
                <h2>About the Series</h2>
            </div>
        </div>
        <div class="row-container col-80">
            <div class="contents">
                <p>
                    So far, I've only finished reading the first book in the series. More will be updated as I finish the other books.
                </p>
<!--
                <a href="../content/images/laadan/Ozark.jpg"><img style="margin: 10px; float:right; width: 300px;" src="../content/images/laadan/Ozark.jpg" title="A map of the world of Ozark" style="width:100%;"></a>
-->
                
            </div>
        </div>
    </div>
</section>

<!-- ----------------------- -->
<!-- - BOOKS --------------- -->
<!-- ----------------------- -->

<section class="section video-preview preview-pane cf">
    <div class="row-container">
        <div class="header-tab col-20">
            <div class="contents">
                <h2>Books in the Series</h2>
            </div>
        </div>
        <div class="row-container col-80">
            <div class="contents">

                <hr style="clear:both;">

                <h3>The Communipaths</h3>
                <img style="margin: 10px; width:200px; float:right;" src="../content/images/laadan/cj-communipaths.jpg" title="Book cover of The Communipaths">
                <p>
                    From the inner-page blurb...
                </p>
                <blockquote>
                    <p>FROM THE DIARY OF TESSA, MEMBER OF MAKLUNITE CLUSTER &quot;CRYSANTHEMUM BRIDGE,&quot;
                    PLANET 34.922.107:</p>

                    <p>Gentle Thursday was not so gentle to Anne-Charlotte or her baby.
                    Four Fedrobots came and took the baby away. Later they charged Anne-Charlotte
                    with high treason against humankind because the baby was needed as a Communipath.</p>
                    <p>Anne-Charlotte screamed foul and dreadful things, and her mind projected
                    an obscene sticky blackness that tried to drown us. She flew over the ground
                    like a low-flying bird, and then teleported herself in fits, popping up
                    all over the landscape.</p>
                    <p>We don't know what to do about Anne-Charlotte. Patrick says she is insane
                    and not responsible. But what if her baby is insane too? Now we won't
                    know until the baby gets mad enough to rip apart the galaxy...</p>
                </blockquote>

                <p>
                    This first book was originally hosted on the <em>sfwa.org</em> website, though
                    the links have gone down. This webpage hosts an archive of the book.
                </p>

                <ul>
                    <li><a href="http://www.ayadanconlangs.com/archive/the-communipaths/index.html">The Communipaths (web)</a></li>
                    <li><a href="http://www.ayadanconlangs.com/archive/downloads/the-communipaths_suzette-haden-elgin_maverynthia.epub">The Communipaths (ebook, created by Maveyrinthia)</a></li>
                </ul>

                <hr style="clear:both;">

                <h3>Furthest</h3>
                <img style="margin: 10px; width:200px; float:right;" src="../content/images/laadan/cj-furthest.jpg" title="Book cover of Furthest">
                <p>From the back cover...</p>
                <blockquote>
                    <p>THE HIDDEN PLANET</p>

                    <p>Coyote Jones, agent for the Tri-Galactic Intelligence Service, had been sent to
                    a planet so unimaginably distant from the rest of the FEderation that it bore
                    the descriptive name Furthest. His mission: to find out why the total
                    body of data about Furthest showed the world's inhabitants to be absolutely
                    average down to the last decimal place. That data had to be false.</p>

                    <p>Jones was permitted to live on the planet, but the natives were so wary
                    of him that he could uncover nothing - until he chanced into a personal
                    crisis faced by his young Furthester assistant. The boy's sister had been
                    sentenced to Erasure, and he wanted Coyote Jones to take the fugitive
                    girl in and hide her.</p>

                    <p>Against his judgement, Jones agreed, and thereby became a criminal
                    on a word he didn't understand. But suddenly the answers began to come,
                    and he found that this planet named Furthest held more strangeness
                    than he could ever have imagined...</p>
                </blockquote>

                <hr style="clear:both;">

                <h3>At the Seventh Level</h3>
                <img style="margin: 10px; width:200px; float:right;" src="../content/images/laadan/cj-seventh.jpg" title="Book cover of At the Seventh Level">
                <p>From the back cover...</p>
                <blockquote>
                    <p>SEXUAL CHAUVINISM WAS THE FOUNDATIO OF THAT WORLD'S STRUCTURE</p>

                    <p>Coyote JOnes had never heard of Abba until he was assigned there. It was a
                    remotely beautiful world, but one which had been admitted to the society of
                    civilized planets only after it had made concessions on its degrading treatment
                    of women. Until then, women were considered as not human, as a sort of necessary beast,
                    but not more.</p>

                    <p>The concessions had been slight - but as a result one briliant female,
                    Jacinth, had risen to the very top of that strange society, to the Seventh Level.
                    Thereby she had become the spiteful target of male fury, female envy, and finally
                    of a deviously evil plot that might cost the world its status.</p>

                    <p>What Coyote Jones found on Abba, the sensuality of the surface, the
                    sexual horror beneath, and the meaning of human dignity, is a novel
                    worthy of the talents of the most gifted new SF writer since Samuel R. Delany
                    and Roger Zelazny.</p>
                </blockquote>

                <hr style="clear:both;">

                <h3>Star-Anchored, Star-Angered</h3>
                <img style="margin: 10px; width:200px; float:right;" src="../content/images/laadan/cj-staranchor.jpg" title="Book cover of Star-Anchored, Star-Angered">
                <p>From the back cover...</p>
                <blockquote>
                    <p>Coyote manfully restrained himself from making the obvious retort,
                    and smiled politely at his host. He wondered which of them was the most extraordinary
                    in appearance. There he himself stood in a dark blue loincloth
                    with a white pinstripe, his chest abloom with curly red hair and
                    tasteful pseudo-tattoos, his fingers heavy with rings, his ankles
                    clanking with bracelets. And there stood Agamen Horta Cady
                    in full green-and-white-striped trousers, bound at the ankle with green
                    ribbons, and a transparent white shirt with flowing sleeves and cuffs, open almost
                    to the waist, a dashing gold sash knotted about his middle, and his head a mass
                    of improbably golden curls topped by a slender gold circlet, a kind of coronet.
                    Coyote decided that they were both in the worst possible taste,
                    from an abstract aesthetic point of view, and that there was no choosing between them for abominableness.</p>
                </blockquote>


                <hr style="clear:both;">

                <h3>Yonder Comes the Other End of Time</h3>
                <img style="margin: 10px; width:200px; float:right;" src="../content/images/laadan/cj-yonder.jpg" title="Book cover of Yonder Comes the Other End of Time">
                <p>From the back cover...</p>
                <blockquote>
                    <p>ROGUE TELEPATH!</p>

                    <p>The Communipaths have traced a mind message of incredible strength
                    to a seemingly empty sector of space, and now Tri-Galactic Federation agent
                    Coyote Jones must find an invisible planet and bring back the
                    unknown telepath who threatens to disrupt the entire Communipath system.</p>

                    <p>Bursting through a Spell of Invisibility and straight into Brightwater Kingdom
                    on the planet Ozark, Coyote discovers a realm ruled by an iron-willed
                    young woman named Responsible - perhaps the very telepath he seeks. But on this
                    world where Magicians of Rank can call up a storm or cure a wounded and unwelcome
                    offworlder with equal ease, will Coyote's psience or Ozark's spells prove the stronger?</p>
                </blockquote>


                <hr style="clear:both;">
                
            </div>
        </div>
    </div>
</section>


<!-- ----------------------- -->
<!-- - CHARACTERS ---------- -->
<!-- ----------------------- -->

<section class="section video-preview preview-pane cf">
    <div class="row-container">
        <div class="header-tab col-20">
            <div class="contents">
                <h2>Characters</h2>
            </div>
        </div>
        <div class="row-container col-80">
            <div class="contents">
                <h3>Notable Characters</h3>

                <h4>Coyote Jones</h4>

                <p>Coyote Jones is an agent of the Tri-Galactic Federation and a telepath - though
                he cannot make out telepathic speech.</p>

                <h4>Tzana Kai</h4>

                <hr style="clear:both;">
                <h3>From the book The Communipaths</h3>

                <h4>Maklunites of Crysanthemum Bridge of planet Iris</h4>

                <p><strong>Tessa</strong> - A child who journals about the recent happenings in her cluster.</p>

                <p><strong>Anne-Charlotte</strong> - A strong telepathic woman who tried to hide her telepathic baby from the government.</p>
                <blockquote>Anne-Charlotte is lovely. She has black hair in two long braids almost to her hips and her skin is the color of the Tsai bushes after the rain (and in case you never saw a Tsai bush that's a sort of pale golden brown with a shine to it). Her eyes are big and black and she has a good wide mouth that used to be always smiling until the baby's father died. When she thinks, there is a sort of flicker all around her; and I could watch her move and listen to her talk and never be tired of it.</blockquote>
                
                <p><strong>Drijn</strong> - Anne-Charlotte's mate, who also was a strong telepath. Deceased.</p>

                <p><strong>Patrick</strong></p>
                <p><strong>Tomaso</strong> - A forest ranger on Iris; has a high Q-rating.</p>
                <p><strong>Ian</strong> - The teacher of the group.</p>
                
                <hr style="clear:both;">
                <h3>From the book Furthest</h3>

                <h4>Furthesters</h4>

                <p><strong>Arh Qu'e (aka &quot;RK&quot;)</strong> - Coyote Jones' helper on Furthest</p>
                <p><strong>Bess</strong> - Arh Qu'e's sister</p>
            </div>
        </div>
    </div>
</section>




<!-- ----------------------- -->
<!-- - CULTURE ------------- -->
<!-- ----------------------- -->

<!--
<section class="section video-preview preview-pane cf">
    <div class="row-container">
        <div class="header-tab col-20">
            <div class="contents">
                <h2>Culture</h2>
            </div>
        </div>
        <div class="row-container col-80">
            <div class="contents">
                <h3>The Twelve Families of Earth</h3>

                <p>
                    The Twelve Families of Earth are people from around the Ozarks area of the United States of America.
                    In 2012, they left the planet in search of a new home.
                </p>

            </div>
        </div>
    </div>
</section>
-->


<!-- ----------------------- -->
<!-- - GALLERY ------------- -->
<!-- ----------------------- -->

<?
function GalleryPreview( $image, $title, $description ) {
?>
    <div class="col-25">
        <div class="contents">
            <p class="preview-image"><a href="<?=$image?>"><img src="<?=$image?>"></a></p>
            <p>
                <span class="title"><a href="<?=$image?>"><?=$title?></a></span>
                <span class="description"><?=$description?></span>
            </p>
        </div>
    </div>
<?
}
?>


<section class="section video-preview preview-pane cf">
    <div class="row-container">
        <div class="header-tab col-20">
            <div class="contents">
                <h2>Gallery</h2>
            </div>
        </div>
        <div class="row-container col-80">
            <div class="contents">
                <h3>Rachel's Art</h3>
                <div class="row-container cf">
                    <? GalleryPreview( "../content/images/laadan/coyote-jones/anne-charlotte.jpg", "Anne-Charlotte", "A Maklunite at Crysanthemum Bridge, whose baby was taken by the government." ); ?>
                </div>
            </div>
        </div>
    </div>
</section>



<!-- ----------------------- -->
<!-- - LINKS --------------- -->
<!-- ----------------------- -->

<section class="section video-preview preview-pane cf">
    <div class="row-container">
        <div class="header-tab col-20">
            <div class="contents">
                <h2>Links</h2>
            </div>
        </div>
        <div class="row-container col-80">
            <div class="contents">
                <ul>
                    <li>Goodreads Links
                        <ul>
                            <li><a href="https://www.goodreads.com/book/show/2037133.Communipath_Worlds">Communipath Worlds (First 3 books)</a></li>
                            <li><a href="https://www.goodreads.com/book/show/16251701-communipaths">The Communipaths (Book 1)</a></li>
                            <li><a href="https://www.goodreads.com/book/show/2834663-furthest">Furthest (Book 2)</a></li>
                            <li><a href="https://www.goodreads.com/book/show/7324204-at-the-seventh-level">At the Seventh Level (Book 3)</a></li>
                            <li><a href="https://www.goodreads.com/book/show/919936.Star_Anchored_Star_Angered">Star-Anchored, Star-Angered (Book 4)</a></li>
                            <li><a href="https://www.goodreads.com/book/show/1055886.Yonder_Comes_The_Other_End_of_Time">Yonder Comes the Other End of Time (Book 5)</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>



<? include_once( "http://www.ayadanconlangs.com/content/themes/2019-01/footer.php" ); ?>
