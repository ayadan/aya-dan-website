<section class="section preview-pane cf">
    <div class="row-container">
        <div class="header-tab col-20">
            <div class="contents">
                <a name="about"></a><h2>Learning Láadan</h2>
            </div>
        </div>
        <div class="row-container col-80 cf">
            <div class="contents">
                <h3>What resources should I use to learn Láadan?</h3>
                
                <p>
					To learn Láadan, you will need to first learn the grammar.
					Memorizing vocabulary is not as important, as you probably
					won't come across any speakers in person, and will therefore
					have time to look through the dictionary as you're working
					with the language.
                </p>
                
                <h4>Grammar Lessons</h4>
                
                <ul>
					<li>
						<strong><a href="https://en.wikibooks.org/wiki/L%C3%A1adan">The Láadan WikiBook</a></strong>
						presents most of the Láadan grammar in easy-to-digest chunks. It covers everything in
						Suzette's original First Dictionary publishing, and some things from the Amberwind lessons.
						This is probably the best place to start.
					</li>
					<li>
						<strong><a href="http://www.ayadanconlangs.com/archive/amberwind/index.html">Amberwind's Láadan Lessons (Archived)</a></strong>
						also has many lessons you can go through. The original website is down, but this site hosts
						a mirror (archive) of that old website. The audio links no longer work, but it is a wealth
						of information - as well as practice - that you can use.
					</li>
					<li>
						<strong><a href="http://www.ayadanconlangs.com/archive/downloads/first-dictionary_partial.pdf">The First Dictionary and Grammar of Láadan Book (Archived)</a></strong>
						is the original book Suzette published that outlines the grammar of Láadan and some practice lessons.
						The original book has a dictionary, though I did not copy those pages for this backup.
					</li>
                </ul>
                
                <p>
					There are also several less-complete resources you can use for additional grammar lessons.
					This includes <a href="https://laadanlanguage.wordpress.com/lesson-books/laadan-lessons-for-beginners/">Láadan Lessons For Beginners</a>
					and
					<a href="http://www.sfwa.org/members/elgin/LaadanLessons/index.html">Láadan Made Easier</a>, both by Suzette.
                </p>
                
                <h4>Dictionaries</h4>
                
                <p>
					Now you'll need to be able to search for vocabulary to use. There are two main places you can access dictionaries...
                </p>
                
                <ul>
					<li>
						<strong><a href="http://ayadanconlangs.com/tools/laadan-dictionary/">The Láadan Quick-Search Dictionary</a></strong>
						is our webapp that makes searching the Láadan dictionary easier. Quickly filter based on different criteria to
						find what you're looking for.
					</li>
					<li>
						<strong><a href="https://laadanlanguage.wordpress.com/dictionaries/">The Dictionary on the official Láadan webpage</a></strong>
						is the original dictionary published on the official website, plus added vocabulary made and approved by pra-community members.
						The quick search dictionary is based on this one.
					</li>
                </ul>
                
                <p>
					I have also parsed out the official Láadan dictionry into machine-readable formats, such as CSV and JSON.
					You can use these for your own projects, or whatever you see fit. There is an
					<a href="https://bitbucket.org/ayadan/laadan-dictionary/src/master/full%20dictionary/english-to-laadan.csv">English to Láadan CSV</a>,
					a <a href="https://bitbucket.org/ayadan/laadan-dictionary/src/master/full%20dictionary/laadan-to-english.json">Láadan to English CSV</a>,
					and a <a href="https://bitbucket.org/ayadan/laadan-dictionary/src/master/full%20dictionary/laadan_to_spanish.csv">Láadan to Spanish CSV</a> (translated by Tea)
                </p>
                
                <h4>Where to Use Láadan</h4>
                
                <p>
					There are not many Láadan users, even online. But, there are a few places where you have the opportunity to talk about Láadan
					and practice speaking in Láadan...
                </p>
                
                <ul>
					<li><strong><a href="http://www.ayadanconlangs.com/forums/">The Áya Dan Message Board</a></strong> allows for asynchronous discussion of various conlangs, including Láadan.</li>
					<li><strong><a href="http://www.ayadanconlangs.com/community/#aya-dan">The Moosader Discord</a></strong> is a multi-topic community, based around programming and tech, but also conlangs, including a dedicated Láadan channel.</li>
                </ul>
                
                <p>
					You may also find Láadan blogs on sites like Tumblr, or posts about Láadan on Twitter, and there are a couple of Facebook pages for Láadan as well.
                </p>
                
            </div>
        </div>
    </div>
</section>
