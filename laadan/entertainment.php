<section class="section preview-pane cf">
    <div class="row-container">
        <div class="header-tab col-20">
            <div class="contents">
                <a name="about"></a><h2>Music</h2>
            </div>
        </div>
        <div class="row-container col-80 cf">
            <div class="col-50">
                <div class="contents">
                    <h3>Hathóol Lom - The Months Song</h3>
                    <p>By Rachel Singh</p>
                    <p>A song to learn about the names of the months in Láadan</p>
                    <ul>
                        <li><a href="laadan-library/original/songs/Month Song - Hathóol Lom/Hathóol.mp3">.mp3</a></li>
                        <li><a href="laadan-library/original/songs/Month Song - Hathóol Lom/Hathóol - Months Song.pdf">Sheet music (PDF)</a></li>
                        <li><a href="laadan-library/original/songs/Month Song - Hathóol Lom/Hathóol.mscz">Sheet music (MuseScore)</a></li>
                        <li><a href="https://www.youtube.com/watch?v=HqA-e1qJG-Q">Video</a></li>
                        <li><a href="songs/Hathóol - Months Song.pdf">Lyrics</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-50">
                <div class="contents">
                    <h3>Birthsong</h3>
                    <p>By Suzette Haden Elgin</p>
                    <ul>
                        <li><a href="laadan-library/original/songs/Birthsong/Birthsong.mp3">.mp3</a></li>
                        <li><a href="laadan-library/original/songs/Birthsong/Birthsong.pdf">Sheet music (PDF)</a></li>
                        <li><a href="laadan-library/original/songs/Birthsong/Birthsong.mscz">Sheet music (MuseScore)</a></li>
                        <li><a href="https://www.youtube.com/watch?v=BzSqqulYmxo">Video</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
