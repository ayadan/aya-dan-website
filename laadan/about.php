<section class="section preview-pane cf">
    <div class="row-container">
        <div class="header-tab col-20">
            <div class="contents">
                <a name="about"></a><h2>About Láadan</h2>
            </div>
        </div>
        <div class="row-container col-80 cf">
            <div class="contents">
                <h3>What is Láadan?</h3>
                <img style="float: right; margin: 10px;" src="../content/images/laadan/suzette-hotwire.jpg" title="An image of Suzette Haden Elgin signing a copy of her book, from the November 1985 Hot Wire magazine.">
                
                <p>Láadan is a language that was constructed in 1982 by Suzette Haden Elgin, a science fiction author, self-help author, feminist, and linguist. She built Láadan to explore the idea of an “undefinable other reality” that we have no vocabulary for. As a science fiction author, she knew of stories that were “Matriarchies” and “Androgynous” presented as an alternative to our traditionally Patriarchal society, but she wondered about some third alternative, where it wasn’t a fight between men vs. women for power, but something else completely.</p>

                <p>This as well as her own personal experiences with using language for human communication, her perceived difficulties that women experience trying to express themselves with certain limitations (how to more easily describe concepts that are common among women but have no terms, how men react to the language women use and write off concerns, etc.), this caused her to think about a language by and for women.</p>

                <p>Láadan went along hand-in-hand with her Native Tongue series of books. In the book series, a group of women who are sent to the “Barren House” once no longer useful to their society end up creating Láadan in secret. In reality, Suzette created Láadan as an experiment, curious whether women would become interested in it, or at least inspire women to build a better language.</p>

                <p>Klingon and Láadan were both created in the 1980s. After ten years passed, Láadan did not gain much popularity, and Suzette saw the Klingon language being more widely adopted as… well, it was something. She declared the Láadan experiment a failure.</p>
                
                <h3>Rachel's Thoughts</h3>

                <p>I think that the concept of Láadan is easier to portray to those who have grown up queer and sheltered like me. For people from my generation and before, if you’re not quite the same as others – whether sexuality-wise or gender-wise – it feels alienating. And, without that community, you aren’t aware of the words that exist to describe your experience (Dysphoria, Asexuality, etc.) In this case, we can see that there was clearly a lack of vocabulary to describe our experiences, but it has since been created and a community built up.</p>

                <p>Likewise, some words in Láadan help describe certain experiences that all people have, though its emphasis as a woman’s language is in how it tries to be more soft, perceptive, and in-tune with feelings. Of course, people of all genders can use and appreciate this language if they are interested; Suzette never meant for it to be “just for women” even if she created it for women. Similarly, so many movies are made with male leads but meant for everybody, while we still assume movies with a female lead are often movies “for women”. Something made for women can still be for everybody.</p>

                <p>In a comparison between Klingon vs. Láadan, I think you would have to compare how they were presented and backed – Klingon had a television show it belonged to, and with the TV show came merchandise to popularize Klingon characters and keep it alive long-term. Suzette had a series of three novels and purposely did not try to market it in such a way.</p>

                <p>On the downsides of Láadan, I have to mention the lack of queer vocabulary. This can be remedied, but it is not something that I am comfortable doing myself and I would like to have more people join my efforts in working with Láadan, so that we may expand the dictionary to cover more perceptions and experiences.</p>
            </div>
        </div>
    </div>
</section>
