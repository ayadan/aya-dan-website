<section class="section preview-pane cf">
    <div class="row-container">
        <div class="header-tab col-20">
            <div class="contents">
                <a name="about"></a><h2>About Láadan</h2>
            </div>
        </div>
        <div class="row-container col-80 cf">
            <div class="contents">
                <p>Links to other Láadan-related items around the internet.</p>

                <table class="table">
                    <thead>
                        <tr>
                            <th>Name</th><th>Type</th><th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><a href="https://laadanlanguage.wordpress.com/">
                                Official Láadan Website
                            </a></td>
                            <td>Website</td>
                            <td></td>
                        </tr>
                        
                        <tr>
                            <td><a href="http://conlangery.com/2011/07/11/conlangery-06-linguistic-typology/">
                                Conlangery Podcast 06
                            </a></td>
                            <td>Podcast</td>
                            <td>A podcast episode that mentions Láadan, but isn't for it.</td>
                        </tr>
                        <tr><td><a href="http://podcast.conlang.org/2009/04/interview-with-suzette-haden-elgin/">
                            Language Creation Society - Interview with Suzette
                            </a></td>
                            <td>Interview</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><a href="http://laadan.tumblr.com/">
                                Thámezhin i Ruth
                            </a></td>
                            <td>Blog</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><a href="https://laadana.tumblr.com/">
                                Láadaná
                            </a></td>
                            <td>Blog</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><a href="http://reshallaadan.tumblr.com/">
                                Reshal i Láadan
                            </a></td>
                            <td>Blog</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><a href="http://ozarque.livejournal.com/">
                                Suzette Haden Elgin’s blog
                            </a></td>
                            <td>Blog</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><a href="https://www.facebook.com/groups/1510434509169558/">
                                Láadan Facebook Group
                            </a></td>
                            <td>Social Media</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><a href="https://www.facebook.com/groups/1491100827835701/">
                                Esperantistaj Láadan-lernantoj
                            </a></td>
                            <td>Social Media</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><a href="http://www.reddit.com/r/Laadan">
                                /r/Laadan
                            </a></td>
                            <td>Social Media</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><a href="http://laadan.livejournal.com/">
                                LiveJournal community
                            </a></td>
                            <td>Social Media</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><a href="https://twitter.com/hashtag/L%C3%A1adan?src=hash">
                                Tweets tagged #Laadan
                            </a></td>
                            <td>Social Media</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><a href="https://www.tumblr.com/search/laadan">
                                Tumblr posts mentioning Láadan
                            </a></td>
                                <td>Social Media</td>
                                <td></td>
                        </tr>
                        <tr>
                            <td><a href="https://en.wikibooks.org/wiki/L%C3%A1adan">
                                Láadan Wikibook
                            </a></td>
                            <td>Grammar Lessons</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><a href="https://laadanlanguage.wordpress.com/lesson-books/laadan-lessons-for-beginners/">
                                Láadan Lessons for Beginners
                            </a></td>
                            <td>Grammar Lessons</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><a href="http://www.sfwa.org/members/elgin/LaadanLessons/index.html">
                                Láadan Made Easier
                            </a></td>
                            <td>Grammar Lessons</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><a href="http://www.ayadanconlangs.com/archive/amberwind/index.html">
                                Amberwind’s Láadan Lessons (Archived)
                            </a></td>
                            <td>Grammar Lessons</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><a href="http://laadan.club/wil-sha/asdf">
                                Memrise – Láadan basic vocabulary
                            </a></td>
                            <td>Vocabulary Lessons</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><a href="http://www.memrise.com/course/556115/laadan-lessons/">
                                Memrise – Amberwind’s vocabulary
                            </a></td>
                            <td>Vocabulary Lessons</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><a href="http://www.memrise.com/course/332370/laadan-vocabulary/">
                                Memrise – Full dictionary
                            </a></td>
                            <td>Vocabulary Lessons</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><a href="https://mw.lojban.org/extensions/ilmentufa/i/laadan/index.html#">
                                la sutysisku – fast dictionary
                            </a></td>
                            <td>Dictionary</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><a href="https://laadanlanguage.wordpress.com/english-to-laadan-dictionary/">
                                English to Láadan
                            </a></td>
                            <td>Dictionary</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><a href="https://laadanlanguage.wordpress.com/dictionaries/laadan-to-english-dictionary/">
                                Láadan to English
                            </a></td>
                            <td>Dictionary</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><a href="https://bitbucket.org/ayadan/laadan-dictionary/src/master/full%20dictionary/laadan_to_spanish.csv">
                                Láadan to Spanish
                            </a></td>
                            <td>Dictionary</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><a href="https://laadanlanguage.wordpress.com/laadan-reference/core-words/">
                                Láadan Core Words
                            </a></td>
                            <td>Dictionary</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><a href="https://bitbucket.org/ayadan/laadan-dictionary/src/master/full%20dictionary/english-to-laadan.csv">
                                English to Láadan (.csv)
                            </a></td>
                            <td>Dictionary</td>
                            <td>Plaintext, computer-parsable dictionary that can be used for software creation.</td>
                        </tr>
                        <tr>
                            <td><a href="https://bitbucket.org/ayadan/laadan-dictionary/src/master/full%20dictionary/laadan-to-english.csv">
                                Láadan to English (.csv)
                            </a></td>
                            <td>Dictionary</td>
                            <td>Plaintext, computer-parsable dictionary that can be used for software creation.</td>
                        </tr>
                        <tr>
                            <td><a href="https://glosbe.com/ldn/en/">
                                Glosbe
                            </a></td>
                            <td>Dictionary</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><a href="http://lingweenie.org/conlang/laadan.html">
                                Lingweenie – Láadan: Chapter Six Brief Reading
                            </a></td>
                            <td>Article</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><a href="https://esirc.emporia.edu/handle/123456789/986">
                                Bíi ril thad óotha demedi be (writing a window to the soul) : Suzette Haden Elgin and the path to a constructed language.
                            </a></td>
                            <td>Article</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><a href="https://play.google.com/store/apps/details?id=laadanhelper.moosader.com.ladanbedinahabe">
                                Láadan Helper – Android App
                            </a></td>
                            <td>Dictionary,Grammar Lessons</td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

