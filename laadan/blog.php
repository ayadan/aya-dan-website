<!-- ----------------------- -->
<!-- - BLOG ---------------- -->
<!-- ----------------------- -->

<?
$blogs = Storage::GetBlogPostsCategory( "laadan" );

function BlogPreview( $post ) {
?>
    <p><span class="title"><a href="<?=$post->link?>"><?=$post->title?></a></span>, <span class="date"><?= Storage::WordpressToDate( $post->pubDate ) ?></span></p>
<?
}
?>

<section class="section preview-pane cf">
    <div class="row-container">
        <div class="header-tab col-20">
            <div class="contents">
                <a name="about"></a><h2>Blog</h2>
                <p><a href="http://ayadanconlangs.com/blog/category/laadan/">View all &gt;&gt;</a></p>
            </div>
        </div>
        <div class="row-container col-80 cf">
            <div class="col-100">
                <div class="contents">
                    <?  $i = 0;
                        foreach( $blogs->channel->item as $key=>$post ) {
                            BlogPreview( $post );
                            $i++;
                            if ( $i % 4 == 0 && $i != 0 ) { break; }
                    } ?>
                </div>
            </div>
        </div>
    </div>
</section>
