<section class="section preview-pane cf">
    <div class="row-container">
        <div class="header-tab col-20">
            <div class="contents">
                <a name="about"></a><h2>Suzette's Books</h2>
            </div>
        </div>
        <div class="row-container col-80 cf">
            <div class="contents">
                <h4>Novels</h4>

                <table class="table">
                    <tr>
                        <th>Book</th>
                        <th>Series</th>
                        <th>Published Date</th>
                    </tr>

                    <tr>
                        <td> The Communipaths </td>
                        <td> <a href="coyote-jones.php">The Coyote Jones series</a> </td>
                        <td> 1970 </td>
                    </tr>

                    <tr>
                        <td> Furthest </td>
                        <td> <a href="coyote-jones.php">The Coyote Jones series</a> </td>
                        <td> 1971 </td>
                    </tr>

                    <tr>
                        <td> At the Seventh Level </td>
                        <td> <a href="coyote-jones.php">The Coyote Jones series</a> </td>
                        <td> 1972 </td>
                    </tr>

                    <tr>
                        <td> Star-Anchored, Star-Angered </td>
                        <td> <a href="coyote-jones.php">The Coyote Jones series</a> </td>
                        <td> 1979 </td>
                    </tr>

                    <tr>
                        <td> Yonder Comes the Other End of Time </td>
                        <td> <a href="coyote-jones.php">The Coyote Jones series</a> </td>
                        <td> 1986 </td>
                    </tr>

                    <tr>
                        <td> Twelve Fair Kingdoms </td>
                        <td> <a href="the-ozark-trilogy.php">The Ozark Trilogy</a> </td>
                        <td> 1981 </td>
                    </tr>

                    <tr>
                        <td> The Grand Jubilee </td>
                        <td> <a href="the-ozark-trilogy.php">The Ozark Trilogy</a> </td>
                        <td> 1981 </td>
                    </tr>

                    <tr>
                        <td> And Then There'll Be Fireworks </td>
                        <td> <a href="the-ozark-trilogy.php">The Ozark Trilogy</a> </td>
                        <td> 1981 </td>
                    </tr>

                    <tr>
                        <td> Native Tongue </td>
                        <td> <a href="native-tongue.php">The Native Tongue series</a> </td>
                        <td> 1984 </td>
                    </tr>

                    <tr>
                        <td> The Judas Rose </td>
                        <td> <a href="native-tongue.php">The Native Tongue series</a> </td>
                        <td> 1987 </td>
                    </tr>

                    <tr>
                        <td> Earthsong </td>
                        <td> <a href="native-tongue.php">The Native Tongue series</a> </td>
                        <td> 1993 </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</section>
