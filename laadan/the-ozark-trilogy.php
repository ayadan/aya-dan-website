<?
    $title = "The Ozark Trilogy (1981)";
    include_once( "../content/php/functions.php" );
    include_once( "../content/themes/2019-01/header.php" );
?>

<!-- ----------------------- -->
<!-- - ABOUT --------------- -->
<!-- ----------------------- -->

<section class="section video-preview preview-pane cf">
    <div class="row-container">
        <div class="header-tab col-20">
            <div class="contents">
                <h2>About the Series</h2>
            </div>
        </div>
        <div class="row-container col-80">
            <div class="contents">
                <a href="../content/images/laadan/Ozark.jpg"><img style="margin: 10px; float:right; width: 300px;" src="../content/images/laadan/Ozark.jpg" title="A map of the world of Ozark" style="width:100%;"></a>
                <p>
                    The Ozark Trilogy is a sci-fi/fantasy novel set in the future,
                    after humans (from the Ozarks region on Earth) have made a new home
                    on the planet they've named Ozark. As such, they've named the continents
                    they live on after Old Earth: Kintucky, Tinaseeh, Mizzurah, Arkansaw, Oklahomah,
                    and one more continent named Marktwain.
                </p>

                <p>
                    Upon settling on the planet, the families created a Confederation of Continents,
                    with there being twelve Families: Airy, Brightwater, Clark, Farson, Guthrie, Lewis, McDaniels, Motley, Purdy, Smith, Traveller, and Wommack.
                    The people of these continents make use of both Magic and of Technology, with Comsets (like networked television you can transmit over) being
                    commonplace, but also benefitting from magic to grow crops, control weather, and maintain health,
                    as well as magic-enchanted Mules for quick flight transportation.
                    People from each kingdom commonly marry people from other kingdoms and move between them,
                    and for the 1,000+ years they have been on the planet, they have had no war or violence.
                </p>

                <p>
                    The social makeup contains the castle <strong>Families</strong>, in charge of ruling their kingdom.
                    Within the castles include <strong>Grannys</strong> - old women who have a basic level of magic
                    and are known for being naggy, but really keep everything running -
                    <strong>Magicians</strong> - People with some magical ability -
                    <strong>Magicians of Rank</strong> - The most skilled and highest magicians.
                    Beyond that, the common person lives and works in the kingdom, benefitting from the magic
                    and healing of the Magicians and Grannys.
                </p>

                <h3>&quot;I should have know that something was very wrong when the Mules started flying erratically.&quot;</h3>

                <p>
                    Suzette Haden Elgin came up with this opening quote to Twelve Fair Kingdoms while teaching a class on writing science fiction novels.
                </p>
                <p><sub>(See <a href="https://www.sfwa.org/members/elgin/OzarkTrilogy/oztrilfaq.html">The FAQ</a>)</sub></p>
            </div>
        </div>
    </div>
</section>

<!-- ----------------------- -->
<!-- - BOOKS --------------- -->
<!-- ----------------------- -->

<section class="section video-preview preview-pane cf">
    <div class="row-container">
        <div class="header-tab col-20">
            <div class="contents">
                <h2>Books in the Trilogy</h2>
            </div>
        </div>
        <div class="row-container col-80">
            <div class="contents">
                <p>
                    Each book isn't really a standalone book - the trilogy needs to be read as one book, though it was split into three.
                </p>

                <p>
                    I wasn't sure what to expect of the series after finishing only the first book... From the titles of the second
                    two, I thought perhaps the rest of the series would be traditional fantasy fare with a party (the Grand Jubilee)
                    with tropey melodrama, and a triumphant closing (And Then There'll Be Fireworks) - <strong>but nope.</strong>
                </p>

                <p>
                    This series is more a <em>political drama</em> than a single character adventure book like many fantasy novels.
                    The first book focuses on Responsible's Quest and is told from her view only, but the second and third books
                    shift between characters and kingdoms to highlight the different gears working together in the story,
                    building towards the prophecy-foretold Trouble, and how the kingdoms adapt and try to survive.
                </p>

                <p>
                    Above all, the world building has a lot of thought put into it. Throughout the three books, you'll learn
                    about the cultures across different kingdoms of the planet, political structures, history of the planet and the people,
                    and so on. Different classes of people have different <em>ways</em> of speaking, with
                    Magicians speaking &quot;properly&quot; and Grannys speaking most &quot;Old-Earth Ozark&quot;-like.
                </p>

                <p>
                    And, some similar ideas can be found between this series and Native Tongue... The sudden shifting of
                    the status quo, creating chaos in the world, the burden that the menfolk create for everyone,
                    and <em>lots</em> of old grannies knitting.
                </p>

                <hr style="clear:both;">

                <h3>Twelve Fair Kingdoms</h3>
                <img style="margin: 10px; width:200px; float:right;" src="../content/images/laadan/twelve-fair-kingdoms.jpg" title="Book cover of Twelve Fair Kingdoms">
                <p>
                    Shenanigans are afoot - magic is misbehaving, mules aren't flying straight, and
                    somebody kidnapped a visiting family's baby and stranded it up in a tree
                    in an effort to seemingly disrupt the upcoming Grand Jubilee.
                </p>
                <p>
                    Responsible of Brightwater, the eldest daughter of Castle Brightwater,
                    decides to leave on a quest to meet with each Family and try to figure out
                    what is causing the problems, and which Family (or Families?) wants
                    to undermine the Confederation of Continents.
                </p>

                <p>
                    This first book is mostly an introduction to each kingdom and each royal family,
                    as well as an introduction to the world of Ozark. This is the only book in the trilogy told in
                    first-person, entirely from Responsible's point of view, as she travels around the world
                    on her Quest.
                </p>

                <hr style="clear:both;">
                <h3>The Grand Jubilee</h3>
                <img style="margin: 10px; width:200px; float:right;" src="../content/images/laadan/the-grand-jubilee.jpg" title="Book cover of The Grand Jubilee">

                <p>
                    The time has arrived for The Grand Jubilee, where each of the twelve kingdoms meet at
                    Castle Brightwater to discuss the state of the Confederation of Continents.
                    Responsible has spent the time since her Quest preparing for the Jubilee, carefully
                    making plans to try to account for any trouble, but a wrench can be thrown into even
                    the most carefully laid plans...
                </p>

                <hr style="clear:both;">
                <h3>And Then There'll Be Fireworks</h3>
                <img style="margin: 10px; width:200px; float:right;" src="../content/images/laadan/and-then-therell-be-fireworks.jpg" title="Book cover of And Then There'll Be Fireworks">

                <p>
                    It's hard to discuss the setting and events of this book without giving away spoilers for the previous books.
                    There is Trouble, as a prophecy had foretold only vaguely, and magic begins to disappear in the world of Ozark.
                    Additionally, there are hidden truths that the Grannys guard that become exposed and must be dealt with.
                </p>
                


                <hr style="clear:both;">

                <h3>Yonder Comes the Other End of Time</h3>
                <img style="margin: 10px; width:200px; float:right;" src="../content/images/laadan/cj-yonder.jpg" title="Book cover of Yonder Comes the Other End of Time">
                <p>This book is actually part of the Coyote Jones series, but it is a cross-over between the world of The Communipaths and the world of Ozark.</p>
                <p>From the back cover...</p>
                <blockquote>
                    <p>ROGUE TELEPATH!</p>

                    <p>The Communipaths have traced a mind message of incredible strength
                    to a seemingly empty sector of space, and now Tri-Galactic Federation agent
                    Coyote Jones must find an invisible planet and bring back the
                    unknown telepath who threatens to disrupt the entire Communipath system.</p>

                    <p>Bursting through a Spell of Invisibility and straight into Brightwater Kingdom
                    on the planet Ozark, Coyote discovers a realm ruled by an iron-willed
                    young woman named Responsible - perhaps the very telepath he seeks. But on this
                    world where Magicians of Rank can call up a storm or cure a wounded and unwelcome
                    offworlder with equal ease, will Coyote's psience or Ozark's spells prove the stronger?</p>
                </blockquote>
            </div>
        </div>
    </div>
</section>


<!-- ----------------------- -->
<!-- - LOCATIONS ----------- -->
<!-- ----------------------- -->

<section class="section video-preview preview-pane cf">
    <div class="row-container">
        <div class="header-tab col-20">
            <div class="contents">
                <h2>Locations</h2>
            </div>
        </div>
        <div class="row-container col-80">
            <div class="contents">
                <!-- ARKANSAW -->
                <h3>Continent of Arkansaw</h3>
                <img style="margin: 5px; width:300px; float:right;" src="../content/images/laadan/arkansaw.jpg" title="The Continent of Arkansaw">
                        
                <h4>The Kingdom of Farson</h4>
                <p><img src="../content/images/laadan/ozark-farson.jpg" title="The Banner of Farson" style="width:125px;"></p>

                <h4>The Kingdom of Guthrie</h4>
                <p><img src="../content/images/laadan/ozark-guthrie.jpg" title="The Banner of Guthrie" style="width:125px;"></p>
                
                <h4>The Kingdom of Purdy</h4>
                <p><img src="../content/images/laadan/ozark-purdy.jpg" title="The Banner of Purdy" style="width:125px;"></p>

                <hr style="clear:both;">

                <!-- KINTUCKY -->
                <h3>Continent of Kintucky</h3>
                <img style="margin: 5px; width:300px; float:right;" src="../content/images/laadan/kintucky.jpg" title="The Continent of Kintucky" style="width:100%;">
                
                <h4>The Kingdom of Wommack</h4>
                <p><img src="../content/images/laadan/ozark-wommack.jpg" title="The Banner of Wommack" style="width:125px;"></p>
                
                <hr style="clear:both;">

                <!-- MARKTWAIN -->
                <h3>Continent of Marktwain</h3>
                <img style="margin: 5px; width:300px; float:right;" src="../content/images/laadan/marktwain.jpg" title="The Continent of Marktwain" style="width:100%;">

                <h4>The Kingdom of Brightwater</h4>
                <p><img src="../content/images/laadan/ozark-brightwater.jpg" title="The Banner of Brightwater" style="width:125px;"></p>
                
                <h4>The Kingdom of McDaniels</h4>
                <p><img src="../content/images/laadan/ozark-mcdaniels.jpg" title="The Banner of McDaniels" style="width:125px;"></p>
                    
                <hr style="clear:both;">

                <!-- MIZZURAH -->
                <h3>Continent of Mizzurah</h3>
                <img style="margin: 5px; width:300px; float:right;" src="../content/images/laadan/mizzurah.jpg" title="The Continent of Mizzurah" style="width:100%;">

                <h4>The Kingdom of Lewis</h4>
                <p><img src="../content/images/laadan/ozark-lewis.jpg" title="The Banner of Lewis" style="width:125px;"></p>
                
                <h4>The Kingdom of Motley</h4>
                <p><img src="../content/images/laadan/ozark-motley.jpg" title="The Banner of Motley" style="width:125px;"></p>
                    
                <hr style="clear:both;">

                <!-- OKLAHOMAH -->
                <h3>Continent of Oklahomah</h3>
                <img style="margin: 5px; width:300px; float:right;" src="../content/images/laadan/oklahomah.jpg" title="The Continent of Oklahomah" style="width:100%;">
                
                <h4>The Kingdom of Airy</h4>
                <p><img src="../content/images/laadan/ozark-airy.jpg" title="The Banner of Airy" style="width:125px;"></p>
                
                <h4>The Kingdom of Clark</h4>
                <p><img src="../content/images/laadan/ozark-clark.jpg" title="The Banner of Clark" style="width:125px;"></p>
                
                <h4>The Kingdom of Smith</h4>
                <p><img src="../content/images/laadan/ozark-smith.jpg" style="width:125px;"></p>

                <hr style="clear:both;">

                <!-- TINASEEH -->
                <h3>Continent of Tinaseeh</h3>

                <img style="margin: 5px; width:300px; float:right;" src="../content/images/laadan/tinaseeh.jpg" title="The Continent of Tinaseeh" style="width:100%;">

                <h4>The Kingdom of Traveller</h4>
                <p><img src="../content/images/laadan/ozark-traveller.jpg" title="The Banner of Traveller" style="width:125px;"></p>

            </div>
        </div>
    </div>
</section>

<!-- ----------------------- -->
<!-- - CHARACTERS ---------- -->
<!-- ----------------------- -->

<section class="section video-preview preview-pane cf">
    <div class="row-container">
        <div class="header-tab col-20">
            <div class="contents">
                <h2>Characters</h2>
            </div>
        </div>
        <div class="row-container col-80">
            <div class="contents">
                <h3>Notable Characters</h3>

                <h4>Responsible of Brightwater</h4>
                <img style="margin: 5px; width: 200px; float:right;" src="../content/images/laadan/responsible.jpg" title="The image of Responsible and her Mule, Sterling, from the cover of Twelve Fair Kingdoms">

                <p>
                    Responsible is a 14-year-old girl (though she turns 15 during the book series).
                    She is heavily relied on in her kingdom to get things done, make decisions, and maintain order,
                    though many men from other kingdoms resent being ordered around by a child.
                </p>
                <p>
                    She begins the series going on a Quest to try to figure out what is happening with
                    the kingdom's magic going awry.
                </p>

                <hr style="clear:both;">

                <!-- Family Members -->
                <h3>List of Family Members</h3>

                <p>(I will fill this out more upon a re-read of the series; there are <em>a lot</em> of people)</p>

                <p>Hint: People with &quot;of&quot; in their name are women. People with a first, middle, and last name are men.</p>

                <table class="table">
                    <tr> <th>Name</th>                              <th>Castle</th>             <th>Description</th> </tr>
                    <tr> <td>Responsible of Brightwater</td>        <td>Brightwater</td>        <td></td> </tr>
                    <tr> <td>Troublesome of Brightwater</td>        <td>Brightwater</td>        <td>Responsible's sister</td> </tr>
                    <tr> <td>Thorn of Guthrie</td>                  <td>Brightwater</td>        <td>Responsible's mother</td> </tr>
                    <tr> <td>Donald Patrick Brightwater</td>        <td>Brightwater</td>        <td></td> </tr>
                    <tr> <td>Terrence Merryweather McDaniels</td>   <td>McDaniels</td>          <td>Baby kidnapped at beginning of Twelve Fair Kingdoms</td> </tr>
                    <tr> <td>Vine of Motley</td>                    <td>McDaniels</td>          <td>Terrence Merryweather McDaniels' mother</td> </tr>
                    <tr> <td>Halliday Joseph McDaniels</td>         <td>McDaniels</td>          <td>Terrence Merryweather McDaniels' father</td> </tr>
                    <tr> <td>Jewel of Wommack</td>                  <td>Wommack</td>            <td></td> </tr>
                    <tr> <td>Lewis Motley Wommack</td>              <td>Wommack</td>            <td></td> </tr>
                    <tr> <td>Gilead of Wommack</td>                 <td>Wommack</td>            <td></td> </tr>
                    <tr> <td>Thomas Lincoln Wommack</td>            <td>Wommack</td>            <td></td> </tr>
                    <tr> <td>Jacob Donahue Wommack</td>             <td>Wommack</td>            <td></td> </tr>
                    <tr> <td>Jeremiah Thomas Traveller</td>         <td>Traveller</td>          <td></td> </tr>
                    <tr> <td>Jacob Jeremiah Traveller</td>          <td>Traveller</td>          <td></td> </tr>
                    <tr> <td>Dellon Mallard Smith</td>              <td>Smith</td>              <td></td> </tr>
                    <tr> <td>Anne of Brightwater</td>               <td>McDaniels</td>          <td>Mother of Silverweb of McDaniels</td> </tr>
                    <tr> <td>Silverweb of McDaniels</td>            <td>McDaniels</td>          <td>Daughter of Anne of Brightwater</td> </tr>
                    <tr> <td>Charity of ?</td>                      <td>Airy</td>               <td></td> </tr>
                    <tr> <td>Patience of Clark</td>                 <td>Airy</td>               <td></td> </tr>
                    <tr> <td>Una of Clark</td>                      <td></td>                   <td></td> </tr>
                    <tr> <td>Myrrh of Guthrie</td>                  <td>Guthrie</td>            <td></td> </tr>
                </table>

                <hr style="clear:both;">

                <!-- Grannys -->
                <h3>List of Grannys</h3>
                
                <table class="table">
                    <tr> <th>Name</th> <th>Castle</th> <th>Description</th> </tr>

                    <tr><td>Granny Hazelbide</td><td>Brightwater</td><td></td></tr>
                    <tr><td>Granny Gableframe</td><td></td><td></td></tr>
                    <tr><td>Granny Frostfall</td><td></td><td></td></tr>
                    <tr><td>Granny Cobbledrayke</td><td>McDaniels</td><td></td></tr>
                    <tr><td>Granny Whiffletree</td><td></td><td></td></tr>
                    <tr><td>Granny Edging</td><td></td><td></td></tr>
                    <tr><td>Granny Leeward</td><td>Traveller</td><td></td></tr>
                    <tr><td>Granny Heatherknit</td><td>Airy</td><td></td></tr>
                    <tr><td>Granny Flyswift</td><td>Airy</td><td></td></tr>
                    <tr><td>Granny Copperdell</td><td>Wommack</td><td></td></tr>
                </table>

                <hr style="clear:both;">

                <!-- Magicians -->
                <h3>List of Magicians of Rank</h3>
                
                <table class="table">
                    <tr> <th>Name</th> <th>Castle</th> <th>Description</th> </tr>

                    <tr> <td>Lincoln Parradyne Smith</td> <td>Smith</td> <td></td> </tr>
                    <tr> <td>Veritas Truebreed Motley</td> <td>Brightwater</td> <td></td> </tr>
                    <tr> <td>Michael Stepforth Guthrie</td> <td>Guthrie</td> <td></td> </tr>
                    <tr> <td>Michael Desirard McDaniels</td> <td>Farson</td> <td></td> </tr>
                </table>
            </div>
        </div>
    </div>
</section>




<!-- ----------------------- -->
<!-- - CULTURE ------------- -->
<!-- ----------------------- -->

<section class="section video-preview preview-pane cf">
    <div class="row-container">
        <div class="header-tab col-20">
            <div class="contents">
                <h2>Culture</h2>
            </div>
        </div>
        <div class="row-container col-80">
            <div class="contents">
                <h3>The Twelve Families of Earth</h3>

                <p>
                    The Twelve Families of Earth are people from around the Ozarks area of the United States of America.
                    In 2012, they left the planet in search of a new home.
                </p>

                <h3>Proper Namings</h3>

                <p>
                    Grannys are tasked with the important job of naming all daughters with a Proper Name, according to some rules.
                    An improper naming can lead to disaster - The Wommack family believe that they have a family curse due to an
                    Improper Naming from long ago.
                </p>

                <p>From the Glossary...</p>

                <blockquote>
                    <p>PROPER NAMING - The system used by the Grannys of Ozark to ensure that a female
                    infant will have the name intended for her by destiny. The mechanism is simple: a name
                    is chosen, by use of the grid below, so that the sum of its letters will be one of the
                    numbers from one to nine. That is not complicated.
                    (For example: Joan = 1 + 6 + 1 + 5 = 13 = 1 + 3 = 4.)</p>

                    <p>What requires skill is knowing which of those numbers is the proper one for a particular
                    girlchild, since each has its own set of distinct characteristics. That knowledge is part
                    of Granny Magic, and is one of the few parts of magic known to them alone.</p>

                    <p>Here is the grid:</p>

                    <table class="table">
                        <tr>
                            <th> 1 </th> <th> 2 </th> <th> 3 </th> <th> 4 </th> <th> 5 </th> <th> 6 </th> <th> 7 </th> <th> 8 </th> <th> 9 </th> 
                        </tr>
                        
                        <tr><td> A </td> <td> B </td> <td> C </td> <td> D </td> <td> E </td> <td> F </td> <td> G </td> <td> H </td> <td> I </td></tr>
                        <tr><td> J </td> <td> K </td> <td> L </td> <td> M </td> <td> N </td> <td> O </td> <td> P </td> <td> Q </td> <td> R </td></tr>
                        <tr><td> S </td> <td> T </td> <td> U </td> <td> V </td> <td> W </td> <td> X </td> <td> Y </td> <td> Z </td> <td> ' </td></tr>
                    </table>

                    <p>(Where ' signifies a glottal stop)</p>
                </blockquote>
            </div>
        </div>
    </div>
</section>


<!-- ----------------------- -->
<!-- - GALLERY ------------- -->
<!-- ----------------------- -->

<?
function GalleryPreview( $image, $title, $description ) {
?>
    <div class="col-25">
        <div class="contents">
            <p class="preview-image"><a href="<?=$image?>"><img src="<?=$image?>"></a></p>
            <p>
                <span class="title"><a href="<?=$image?>"><?=$title?></a></span>
                <span class="description"><?=$description?></span>
            </p>
        </div>
    </div>
<?
}
?>

<section class="section video-preview preview-pane cf">
    <div class="row-container">
        <div class="header-tab col-20">
            <div class="contents">
                <h2>Gallery</h2>
            </div>
        </div>
        <div class="row-container col-80">
            <div class="contents">
                <h3>Art from Suzette's Page</h3>
                <p>These images are from <a href="http://www.sfwa.org/members/elgin/OzarkTrilogy/index.html">sfwa.org</a> where Suzette had a page about her books.</p>
                <p>The art is by Karen Jollie, who also did the map art for the book.</p>
                <div class="row-container cf">
                    <? GalleryPreview( "../content/images/laadan/mule_sfwa.org.gif", "Mule", "In Ozark there is a native species called Mules (for their similarity to Earth Mules). They have been enchanted to fly to provide transportation for the Ozarkers" ); ?>
                    <? GalleryPreview( "../content/images/laadan/ozark-paperdoll-1.gif", "Responsible Paper Doll - Set 1", "Responsible of Brightwater is one of the main characters of the trilogy." ); ?>
                    <? GalleryPreview( "../content/images/laadan/ozark-paperdoll-2.gif", "Responsible Paper Doll - Set 2", "More clothes for the doll" ); ?>
                    <? GalleryPreview( "../content/images/laadan/ozark-paperdoll-3.gif", "Responsible Paper Doll - Set 3", "More clothes for the doll" ); ?>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- ----------------------- -->
<!-- - LINKS --------------- -->
<!-- ----------------------- -->

<section class="section video-preview preview-pane cf">
    <div class="row-container">
        <div class="header-tab col-20">
            <div class="contents">
                <h2>Links</h2>
            </div>
        </div>
        <div class="row-container col-80">
            <div class="contents">
                <ul>
                    <li><a href="https://www.sfwa.org/members/elgin/OzarkTrilogy/index.html">Suzette's webpage on The Ozark Trilogy</a></li>
                    <li>Goodreads Links
                        <ul>
                            <li><a href="https://www.goodreads.com/book/show/1895803.The_Ozark_Trilogy">The Ozark Trilogy (all the books)</a></li>
                            <li><a href="https://www.goodreads.com/book/show/13739.Twelve_Fair_Kingdoms">Twelve Fair Kingdoms (Book 1)</a></li>
                            <li><a href="https://www.goodreads.com/book/show/1895800.The_Grand_Jubilee">The Grand Jubilee (Book 2)</a></li>
                            <li><a href="https://www.goodreads.com/book/show/1897205.And_Then_There_ll_Be_Fireworks">And Then There'll Be Fireworks (Book 3)</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>



<? include_once( "http://www.ayadanconlangs.com/content/themes/2019-01/footer.php" ); ?>
