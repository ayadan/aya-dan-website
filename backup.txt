
        <div class="left-sidebar">
            
            <div class="site-logo">
                <img src="<?=$path?>content/images/webpage/logo-small-text.png" title="The Áya Dan giraffe logo!">
                <header>
                    <h1>Áya Dan</h1>
                    <p><em>Language is Beautiful</em></p>
                    <p><a href="http://www.ayadanconlangs.com/">AyaDanConlangs.com</a></p>
                </header>
            </div>
            
            <nav>
                <h3>Media</h3>
                <ul>
                    <li><a href="http://www.ayadanconlangs.com/blog/">      <img src="<?=$path?>content/images/webpage/icon-blog.png"> Blog</a></li>
                    <li><a href="http://www.ayadanconlangs.com/videos/">    <img src="<?=$path?>content/images/webpage/icon-video.png"> Videos</a></li>
                    <li><a href="http://www.ayadanconlangs.com/comics/">    <img src="<?=$path?>content/images/webpage/icon-comic.png"> Comics</a></li>
                    <li><a href="http://www.ayadanconlangs.com/arcade/">    <img src="<?=$path?>content/images/webpage/icon-arcade.png"> Arcade</a></li>
                    <li><a href="http://www.ayadanconlangs.com/tools/">     <img src="<?=$path?>content/images/webpage/icon-tools.png"> Tools</a></li>
                    <li><a href="http://www.ayadanconlangs.com/resources/"> <img src="<?=$path?>content/images/webpage/icon-resources.png"> Resources</a></li>
                    <li><a href="http://www.ayadanconlangs.com/community/"> <img src="<?=$path?>content/images/webpage/icon-community.png"> Community</a></li>
                </ul>

                <hr>

                <h3>Language</h3>
                <ul>
                    <li><a href="http://www.ayadanconlangs.com/esperanto/"> <img src="<?=$path?>content/images/webpage/icon-esperanto.png"> Esperanto</a></li>
                    <li><a href="http://www.ayadanconlangs.com/ido/">       <img src="<?=$path?>content/images/webpage/icon-ido.png"> Ido</a></li>
                    <li><a href="http://www.ayadanconlangs.com/laadan/">    <img src="<?=$path?>content/images/webpage/icon-laadan.png"> Láadan</a></li>
                    <li><a href=""><img src="content/images/webpage/icon-solresol.png"> Solresol</a></li>
                    <li><a href=""><img src="content/images/webpage/icon-tokipona.png"> Toki Pona</a></li>
                </ul>

                <hr>

                <h3>Community</h3>
                <ul>
                    <li><a href="http://www.ayadanconlangs.com/community#discord">  <img src="<?=$path?>content/images/webpage/icon-discord.png"> Discord</a></li>
                    <li><a href="http://www.ayadanconlangs.com/community#telegram"> <img src="<?=$path?>content/images/webpage/icon-telegram.png"> Telegram</a></li>
                    <li><a href="http://www.ayadanconlangs.com/forums">             <img src="<?=$path?>content/images/webpage/icon-forums.png"> Forums</a></li>
                </ul>
            </nav>
            
        </div>

<section class="preview blog-preview">
    <h2>Blog</h2>

    <p class="view-more"><a href="">View all &raquo;</a></p>
</section>

<hr>

<section class="preview videos-preview">
    <h2>Videos</h2>

    <p class="view-more"><a href="">View all &raquo;</a></p>
</section>

<hr>

<section class="preview community-preview">
    <h2>Community</h2>

    <p class="view-more"><a href="">View all &raquo;</a></p>
</section>

<hr>

<section class="preview resources-preview">
    <h2>Resources</h2>

    <p class="view-more"><a href="">View all &raquo;</a></p>
</section>

<hr>

<section class="preview tools-preview">
    <h2>Tools</h2>

    <p class="view-more"><a href="">View all &raquo;</a></p>
</section>


<nav class="sub-links">
    <h3>Quick Links</h3>

    <ul>
        <li><a href="https://shop.spreadshirt.com/conlangers/">YouTube channel</a></li>
        <li><a href="https://www.youtube.com/ayadanconlangs">T-shirts</a></li>
    </ul>

    <hr>
    
    <h4>Esperanto</h4>
    <ul>
        <li><a href="">Online Dictionary</a></li>
    </ul>
    <h4>Ido</h4>
    <ul>
        <li><a href="">Online Dictionary</a></li>
    </ul>
    <h4>Láadan</h4>
    <ul>
        <li><a href="">Online Dictionary</a></li>
    </ul>
</nav>


                </div> <!-- main view -->
        </div> <!-- container -->
    </div> <!-- fluid container -->

    <footer>
        <div class="fluid-container">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-2">
                    <p><strong>Áya Dan</strong><br>Conlang Blog and Community</p>
                    <p>Created &amp; maintained by<br> Rachel Wil Sha Singh</p>
                    <p>Contact: Rachel@Moosader.com</p>
                    <p>Always looking for more contributors :)</p>
                </div>
                <div class="col-md-4">
                    <p><strong>Áya Dan Links</strong></p>
                    <div class="row">
                        <div class="col-md-4">
                            <ul>
                                <li><a href="http://www.ayadanconlangs.com/blog/">Blog</a></li>
                                <li><a href="http://www.ayadanconlangs.com/videos/">Videos</a></li>
                                <li><a href="http://www.ayadanconlangs.com/comics/">Comics</a></li>
                                <li><a href="http://www.ayadanconlangs.com/arcade/">Arcade</a></li>
                                <li><a href="http://www.ayadanconlangs.com/tools/">Tools</a></li>
                                <li><a href="http://www.ayadanconlangs.com/resources/">Resources</a></li>
                                <li><a href="http://www.ayadanconlangs.com/community/">Community</a></li>
                            </ul>
                        </div>
                        <div class="col-md-4">
                            <ul>
                                <li><a href="http://www.ayadanconlangs.com/esperanto/">Esperanto</a></li>
                                <li><a href="http://www.ayadanconlangs.com/ido/">Ido</a></li>
                                <li><a href="http://www.ayadanconlangs.com/laadan/">Láadan</a></li>
                                <li><a href="http://www.ayadanconlangs.com/community#discord">Discord</a></li>
                                <li><a href="http://www.ayadanconlangs.com/community#telegram">Telegram</a></li>
                                <li><a href="http://www.ayadanconlangs.com/forums/">Forums</a></li>
                                <li><a href="https://www.youtube.com/ayadanconlangs">Áya Dan YouTube channel</a></li>
                                <li><a href="https://shop.spreadshirt.com/conlangers/">Áya Dan T-shirt store</a></li>
                            </ul>
                        </div>
                        <div class="col-md-4">
                            <ul>
                                <li><a href="">Láadan Dictionary</a></li>
                                <li><a href="https://en.wikibooks.org/wiki/L%C3%A1adan">Láadan Wikibook</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <p><strong>Non-Áya Dan affiliated</strong></p>
                    <div class="row">
                        <div class="col-md-4">
                            <p><strong>Esperanto</strong></p>
                            <ul>
                                <li><a href="">asdf</a></li>
                            </ul>
                        </div>
                        <div class="col-md-4">
                            <p><strong>Ido</strong></p>
                            <ul>
                                <li><a href="">asdf</a></li>
                            </ul>
                        </div>
                        <div class="col-md-4">
                            <p><strong>Láadan</strong></p>
                            <ul>
                                <li><a href="https://laadanlanguage.wordpress.com/">Official Láadan Page</a></li>
                                <li><a href="">Amberwind's Lessons</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-1"></div>
            </div>
        </div>
    </footer>
