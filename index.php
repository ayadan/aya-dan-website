<?
$title = "Welcome!";
?>
<?
    include_once( "content/php/functions.php" );
    include_once( "content/themes/2019-01/header.php" );
?>

<?
$videos = Storage::GetVideosBase();
$blogs = Storage::GetBlogPosts();
$blogsRachel = Storage::GetBlogPostsAuthor( "rachel" );
$blogsBecca = Storage::GetBlogPostsAuthor( "beka" );
$forums = Storage::GetForum();
?>

<!-- ----------------------- -->
<!-- - VIDEOS -------------- -->
<!-- ----------------------- -->

<?
function VideoPreview( $video, $language ) {
    $url = "http://www.ayadanconlangs.com/videos/?video_id=" . $video['YouTube ID'] . "&video_language=" . $language;
?>
    <p class="preview-image"><a href="<?=$url?>"><img src="https://img.youtube.com/vi/<?=$video['YouTube ID']?>/default.jpg"></a></p>
    <p>
        <span class="title"><a href="<?=$url?>"><?=$video['name']?></a></span>
        <span class="author">by <?=$video['creator']?>,</span>
        <span class="date"><?=Storage::YMDToDate($video['date'] )?></span>
    </p>
<?
}
?>

<section class="section video-preview preview-pane cf">
    <div class="row-container">
        <div class="header-tab col-10">
            <div class="contents">
                <h2>Latest Videos</h2>
                <p><a href="http://www.ayadanconlangs.com/videos/">View all &gt;&gt;</a></p>
            </div>
        </div>
        <div class="row-container col-90 cf">
            <div class="col-25">
                <div class="contents">
                    <h3>Esperanto</h3>
                    <? VideoPreview( $videos["esperanto"]["videos"][0], "esperanto" ); ?>
                </div>            
            </div>
            <div class="col-25">
                <div class="contents">
                    <h3>Ido</h3>
                    <? VideoPreview( $videos["ido"]["videos"][0], "ido" ); ?>
                </div>
            </div>
            <div class="col-25">
                <div class="contents">
                    <h3>Láadan</h3>
                    <? VideoPreview( $videos["laadan"]["videos"][0], "laadan" ); ?>
                </div>
            </div>
            <div class="col-25">
                <div class="contents">
                    <h3>Other</h3>
                    <? VideoPreview( $videos["other"]["videos"][0], "other" ); ?>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- ----------------------- -->
<!-- - BLOG ---------------- -->
<!-- ----------------------- -->

<?
function BlogPreview( $post ) {
?>
    <p><span class="title"><a href="<?=$post->link?>"><?=$post->title?></a></span>, <span class="date"><?= Storage::WordpressToDate( $post->pubDate ) ?></span></p>
<?
}
?>

<section class="section blog-preview preview-pane cf">
    <div class="row-container">
        <div class="header-tab col-10">
            <div class="contents">
                <h2>Latest Blog Posts</h2>
                <p><a href="http://www.ayadanconlangs.com/blog/">View all &gt;&gt;</a></p>
            </div>
        </div>
        <div class="row-container col-90 cf">
            <div class="col-50">
                <div class="contents">
                    <h3>From BecciCat</h3>
                    <?  $i = 0;
                        foreach( $blogsBecca->channel->item as $key=>$post ) {
                            BlogPreview( $post );
                            $i++;
                            if ( $i % 4 == 0 && $i != 0 ) { break; }
                    } ?>
                </div>
            </div>
            <div class="col-50">
                <div class="contents">
                    <h3>From Rachel</h3>
                    <?  $i = 0;
                        foreach( $blogsRachel->channel->item as $key=>$post ) {
                            BlogPreview( $post );
                            $i++;
                            if ( $i % 4 == 0 && $i != 0 ) { break; }
                    } ?>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- ----------------------- -->
<!-- - FORUM --------------- -->
<!-- ----------------------- -->

<?
function ForumPreview( $post ) {
?>
    <div class="col-25">
        <div class="contents">
            <p>
                <span class="title"><a href="<?=$post->id?>"><?=$post->title?></a></span>
                <span class="author">by <?=$post->author->name?>,</span>
                <span class="date"><?= Storage::PhpbbToDate( $post->published ) ?></span>
            </p>
        </div>            
    </div>
<?
}
?>

<section class="section forum-preview preview-pane cf">
    <div class="row-container">
        <div class="header-tab col-10">
            <div class="contents">
                <h2>Latest Forum Posts</h2>
                <p><a href="http://www.ayadanconlangs.com/forums/">View all &gt;&gt;</a></p>
            </div>
        </div>
        <div class="row-container col-90 cf">
    
            <?
            $i = 0;
            foreach( $forums->entry as $key=>$post ) {
                ForumPreview( $post );
                $i++;
                if ( $i % 4 == 0 && $i != 0 ) { break; }
            }
            ?>
        </div>
    </div>
</section>



<? include_once( "content/themes/2019-01/footer.php" ); ?>
