<?
$title = "Welcome!";
$sidebar = "temp-bar";
?>
<? include_once( "content/layout/base-header.php" ); ?>
<? include_once( "content/php/functions.php" ); ?>

<?
$videos = Storage::GetVideosBase();
$blogs = Storage::GetBlogPosts();
$blogsRachel = Storage::GetBlogPostsAuthor( "rachel" );
$blogsBecca = Storage::GetBlogPostsAuthor( "beka" );
$forums = Storage::GetForum();
?>

<div class="container">
    <div class="row">
        <div class="col-md-12">

            <h1>Welcome to Áya Dan!</h1>

<!--
            <hr>

            <section class="preview">
                <h2>Quick Tools</h2>
                <?
                    $width = 600;
                    $height = 600; 
                    $esperanto = "http://www.ayadanconlangs.com/tools/portable-esperanto/";
                    $laadan = "http://www.ayadanconlangs.com/tools/portable-laadan/";
                    $ido = "http://www.ayadanconlangs.com/tools/portable-ido/";
                ?>
                
                <div class="row">
                    <div class="col-md-4 col-sm-12">
                        <a href="<?=$esperanto?>" target="popup" onclick="window.open('<?=$esperanto?>','name','width=<?=$width?>,height=<?=$height?>')">
                            <img src="content/images/webpage/icon-esperanto.png"> Esperanto Dictionary
                        </a>
                    </div>
                    
                    <div class="col-md-4 col-sm-12">
                        <a href="<?=$laadan?>" target="popup" onclick="window.open('<?=$laadan?>','name','width=<?=$width?>,height=<?=$height?>')">
                            <img src="content/images/webpage/icon-laadan.png"> Láadan Dictionary
                        </a> 
                    </div>
                    
                    <div class="col-md-4 col-sm-12">
                        <a href="<?=$ido?>" target="popup" onclick="window.open('<?=$ido?>','name','width=<?=$width?>,height=<?=$height?>')">
                            <img src="content/images/webpage/icon-ido.png"> Ido Dictionary
                        </a>
                    </div>
                </div>
            </section>
-->

            <hr>

            <section class="preview video-preview">
                <h2>Latest Videos</h2>
                    
                <div class="language-videos row">
                    <div class="video-item col-md-3">
                        <h3>Esperanto</h3>
                        <a href="https://www.youtube.com/watch?v=<?=$videos["esperanto"]["videos"][0]['YouTube ID']?>"><img src="https://img.youtube.com/vi/<?=$videos["esperanto"]["videos"][0]['YouTube ID']?>/default.jpg"></a>
                        <p class="title"><a href="https://www.youtube.com/watch?v=<?=$videos["esperanto"]["videos"][0]['YouTube ID']?>"><?=$videos["esperanto"]["videos"][0]['name']?></a></p>
                        <p class="author">By <?=$videos["esperanto"]["videos"][0]['creator']?></p>
                        <p class="date"><?=Storage::YMDToDate($videos["esperanto"]["videos"][0]['date'] )?></p>
                    </div>
                    
                    <div class="video-item col-md-3">
                        <h3>Ido</h3>
                        <a href="https://www.youtube.com/watch?v=<?=$videos["ido"]["videos"][0]['YouTube ID']?>"><img src="https://img.youtube.com/vi/<?=$videos["ido"]["videos"][0]['YouTube ID']?>/default.jpg"></a>
                        <p class="title"><a href="https://www.youtube.com/watch?v=<?=$videos["ido"]["videos"][0]['YouTube ID']?>"><?=$videos["ido"]["videos"][0]['name']?></a></p>
                        <p class="author">By <?=$videos["ido"]["videos"][0]['creator']?></p>
                        <p class="date"><?=Storage::YMDToDate($videos["ido"]["videos"][0]['date'] )?></p>
                    </div>
                    
                    <div class="video-item col-md-3">
                        <h3>Láadan</h3>
                        <a href="https://www.youtube.com/watch?v=<?=$videos["laadan"]["videos"][0]['YouTube ID']?>"><img src="https://img.youtube.com/vi/<?=$videos["laadan"]["videos"][0]['YouTube ID']?>/default.jpg"></a>
                        <p class="title"><a href="https://www.youtube.com/watch?v=<?=$videos["laadan"]["videos"][0]['YouTube ID']?>"><?=$videos["laadan"]["videos"][0]['name']?></a></p>
                        <p class="author">By <?=$videos["laadan"]["videos"][0]['creator']?></p>
                        <p class="date"><?=Storage::YMDToDate( $videos["laadan"]["videos"][0]['date'] )?></p>
                    </div>
                    
                    <div class="video-item col-md-3">
                        <h3>Others</h3>
                        <a href="https://www.youtube.com/watch?v=<?=$videos["other"]["videos"][0]['YouTube ID']?>"><img src="https://img.youtube.com/vi/<?=$videos["other"]["videos"][0]['YouTube ID']?>/default.jpg"></a>
                        <p class="title"><a href="https://www.youtube.com/watch?v=<?=$videos["other"]["videos"][0]['YouTube ID']?>"><?=$videos["other"]["videos"][0]['name']?></a></p>
                        <p class="author">By <?=$videos["other"]["videos"][0]['creator']?></p>
                        <p class="date"><?=Storage::YMDToDate( $videos["other"]["videos"][0]['date'] )?></p>
                    </div>
                </div>
                
                <div class="row view-all-link">
                    <div class="col-md-12">
                        <a href="http://www.ayadanconlangs.com/videos/">View all videos &gt;&gt;</a>
                    </div>
                </div>
            </section>

            <hr>

            <section class="preview blog-preview">
                <h2>Latest Blog Posts</h2>

                <div class="blog-feature grid-view row aya-blog-preview">
                    <h3>From BecciCat</h3>
                    
                    <?  $i = 0;
                        foreach( $blogsBecca->channel->item as $key=>$post ) { ?>
                        <div class="blog-item grid-item col-md-12">
                            <p class="title"><a href="<?=$post->link?>"><?=$post->title?></a></p>
                            <p class="date"><?= Storage::WordpressToDate( $post->pubDate ) ?></p>
                            <p class="blog-feed-raw"><?=$post->description?></p>
                        </div>
                    <?
                        $i++;
                        //if ( $i % 4 == 0 && $i != 0 ) { break; }
                        break;
                    } ?>
                </div>
                
                <div class="blog-feature grid-view row aya-blog-preview">
                    <h3>From Rachel</h3>
                    <?  $i = 0;
                        foreach( $blogsRachel->channel->item as $key=>$post ) { ?>
                        <div class="blog-item grid-item col-md-12">
                            <p class="title"><a href="<?=$post->link?>"><?=$post->title?></a></p>
                            <p class="date"><?= Storage::WordpressToDate( $post->pubDate ) ?></p>
                            <p class="blog-feed-raw"><?=$post->description?></p>
                        </div>
                    <?
                        $i++;
                        //if ( $i % 4 == 0 && $i != 0 ) { break; }
                        break;
                    } ?>
                </div>

                <p class="view-more"><a href="http://www.ayadanconlangs.com/blog/">View all posts &gt;&gt;</a></p>
            </section>

            <hr>

            <section class="preview forum-preview">
                <h2>Latest Forum Posts</h2>
                
                <div class="blog-feature grid-view row">
                    <?
                    $i = 0;
                    foreach( $forums->entry as $key=>$post ) {
                        ?>
                        <div class="blog-item grid-item col-md-3">
                            <a href="<?=$post->id?>"><img src="content/images/webpage/icon-forums-big.png"></a>
                            <p class="title"><a href="<?=$post->id?>"><?=$post->title?></a></p>
                            <p class="author">By <?=$post->author->name?></p>
                            <p class="date"><?= Storage::PhpbbToDate( $post->published ) ?></p>
                        </div>
                        <?
                        $i++;
                        if ( $i % 4 == 0 && $i != 0 ) { break; }
                    }
                    ?>
                </div>
            </section>

        </div>
    </div>
</div>


<? include_once( "content/layout/base-footer.php" ); ?>
