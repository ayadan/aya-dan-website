<?php

/* index_body.html */
class __TwigTemplate_c16e71b8df39431e11be7d218f29216c429c3982d50fb9c86f4f671ded50da8c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $location = "overall_header.html";
        $namespace = false;
        if (strpos($location, '@') === 0) {
            $namespace = substr($location, 1, strpos($location, '/') - 1);
            $previous_look_up_order = $this->env->getNamespaceLookUpOrder();
            $this->env->setNamespaceLookUpOrder(array($namespace, '__main__'));
        }
        $this->loadTemplate("overall_header.html", "index_body.html", 1)->display($context);
        if ($namespace) {
            $this->env->setNamespaceLookUpOrder($previous_look_up_order);
        }
        // line 2
        echo "
<p class=\"";
        // line 3
        echo ($context["S_CONTENT_FLOW_END"] ?? null);
        echo " responsive-center time";
        if (($context["S_USER_LOGGED_IN"] ?? null)) {
            echo " rightside";
        }
        echo "\">";
        if (($context["S_USER_LOGGED_IN"] ?? null)) {
            echo ($context["LAST_VISIT_DATE"] ?? null);
        } else {
            echo ($context["CURRENT_TIME"] ?? null);
        }
        echo "</p>
";
        // line 4
        if (($context["S_USER_LOGGED_IN"] ?? null)) {
            echo "<p class=\"responsive-center time\">";
            echo ($context["CURRENT_TIME"] ?? null);
            echo "</p>";
        }
        // line 5
        echo "
";
        // line 6
        // line 7
        echo "
";
        // line 8
        if (($context["U_MARK_FORUMS"] ?? null)) {
            // line 9
            echo "\t<div class=\"action-bar compact\">
\t\t<a href=\"";
            // line 10
            echo ($context["U_MARK_FORUMS"] ?? null);
            echo "\" class=\"mark-read rightside\" accesskey=\"m\" data-ajax=\"mark_forums_read\">";
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("MARK_FORUMS_READ");
            echo "</a>
\t</div>

";
        }
        // line 14
        echo "
";
        // line 15
        // line 16
        echo "<!-- topic-navi for phpBB 3.2 by Joyce&Luna http://phpBB-Style-Design.de -->
";
        // line 17
        if (($context["S_DISPLAY_SEARCH"] ?? null)) {
            // line 18
            echo "<ul class=\"topic-navi\">
\t";
            // line 19
            if (($context["S_REGISTERED_USER"] ?? null)) {
                // line 20
                echo "\t\t<li>
\t\t\t<a href=\"";
                // line 21
                echo ($context["U_SEARCH_SELF"] ?? null);
                echo "\">
\t\t\t\t<i class=\"icons fa fa-file-text-o\" aria-hidden=\"true\"></i><span>";
                // line 22
                echo $this->env->getExtension('phpbb\template\twig\extension')->lang("SEARCH_SELF");
                echo "</span>
\t\t\t</a>
\t\t</li>
\t";
            }
            // line 26
            echo "
\t";
            // line 27
            if (($context["S_USER_LOGGED_IN"] ?? null)) {
                // line 28
                echo "\t\t<li>
\t\t\t<a href=\"";
                // line 29
                echo ($context["U_SEARCH_NEW"] ?? null);
                echo "\">
\t\t\t\t<i class=\"icons fa fa-file-text-o\" aria-hidden=\"true\"></i><span>";
                // line 30
                echo $this->env->getExtension('phpbb\template\twig\extension')->lang("SEARCH_NEW");
                echo "</span>
\t\t\t</a>
\t\t</li>
\t";
            }
            // line 34
            echo "
\t";
            // line 35
            if (($context["S_LOAD_UNREADS"] ?? null)) {
                // line 36
                echo "\t\t<li>
\t\t\t<a href=\"";
                // line 37
                echo ($context["U_SEARCH_UNREAD"] ?? null);
                echo "\">
\t\t\t\t<i class=\"icons fa fa-file-text-o\" aria-hidden=\"true\"></i><span>";
                // line 38
                echo $this->env->getExtension('phpbb\template\twig\extension')->lang("SEARCH_UNREAD");
                echo "</span>
\t\t\t</a>
\t\t</li>
\t";
            }
            // line 42
            echo "
\t\t<li>
\t\t\t<a href=\"";
            // line 44
            echo ($context["U_SEARCH_UNANSWERED"] ?? null);
            echo "\">
\t\t\t\t<i class=\"icons fa fa-file-text-o\" aria-hidden=\"true\"></i><span>";
            // line 45
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("SEARCH_UNANSWERED");
            echo "</span>
\t\t\t</a>
\t\t</li>

\t\t<li>
\t\t\t<a href=\"";
            // line 50
            echo ($context["U_SEARCH_ACTIVE_TOPICS"] ?? null);
            echo "\">
\t\t\t\t<i class=\"icons fa fa-file-text-o\" aria-hidden=\"true\"></i><span>";
            // line 51
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("SEARCH_ACTIVE_TOPICS");
            echo "</span>
\t\t\t</a>
\t\t</li>
</ul>
";
        }
        // line 56
        $location = "forumlist_body.html";
        $namespace = false;
        if (strpos($location, '@') === 0) {
            $namespace = substr($location, 1, strpos($location, '/') - 1);
            $previous_look_up_order = $this->env->getNamespaceLookUpOrder();
            $this->env->setNamespaceLookUpOrder(array($namespace, '__main__'));
        }
        $this->loadTemplate("forumlist_body.html", "index_body.html", 56)->display($context);
        if ($namespace) {
            $this->env->setNamespaceLookUpOrder($previous_look_up_order);
        }
        // line 57
        echo "
";
        // line 58
        // line 59
        echo "
";
        // line 60
        if (( !($context["S_USER_LOGGED_IN"] ?? null) &&  !($context["S_IS_BOT"] ?? null))) {
            // line 61
            echo "\t<form method=\"post\" action=\"";
            echo ($context["S_LOGIN_ACTION"] ?? null);
            echo "\" class=\"headerspace\">
\t<h3><a href=\"";
            // line 62
            echo ($context["U_LOGIN_LOGOUT"] ?? null);
            echo "\">";
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("LOGIN_LOGOUT");
            echo "</a>";
            if (($context["S_REGISTER_ENABLED"] ?? null)) {
                echo "&nbsp; &bull; &nbsp;<a href=\"";
                echo ($context["U_REGISTER"] ?? null);
                echo "\">";
                echo $this->env->getExtension('phpbb\template\twig\extension')->lang("REGISTER");
                echo "</a>";
            }
            echo "</h3>
\t\t<fieldset class=\"quick-login\">
\t\t\t<label for=\"username\"><span>";
            // line 64
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("USERNAME");
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("COLON");
            echo "</span> <input type=\"text\" tabindex=\"1\" name=\"username\" id=\"username\" size=\"10\" class=\"inputbox\" title=\"";
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("USERNAME");
            echo "\" /></label>
\t\t\t<label for=\"password\"><span>";
            // line 65
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("PASSWORD");
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("COLON");
            echo "</span> <input type=\"password\" tabindex=\"2\" name=\"password\" id=\"password\" size=\"10\" class=\"inputbox\" title=\"";
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("PASSWORD");
            echo "\" autocomplete=\"off\" /></label>
\t\t\t";
            // line 66
            if (($context["U_SEND_PASSWORD"] ?? null)) {
                // line 67
                echo "\t\t\t\t<a href=\"";
                echo ($context["U_SEND_PASSWORD"] ?? null);
                echo "\">";
                echo $this->env->getExtension('phpbb\template\twig\extension')->lang("FORGOT_PASS");
                echo "</a>
\t\t\t";
            }
            // line 69
            echo "\t\t\t";
            if (($context["S_AUTOLOGIN_ENABLED"] ?? null)) {
                // line 70
                echo "\t\t\t\t<span class=\"responsive-hide\">|</span> <label for=\"autologin\">";
                echo $this->env->getExtension('phpbb\template\twig\extension')->lang("LOG_ME_IN");
                echo " <input type=\"checkbox\" tabindex=\"4\" name=\"autologin\" id=\"autologin\" /></label>
\t\t\t";
            }
            // line 72
            echo "\t\t\t<input type=\"submit\" tabindex=\"5\" name=\"login\" value=\"";
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("LOGIN");
            echo "\" class=\"button2\" />
\t\t\t";
            // line 73
            echo ($context["S_LOGIN_REDIRECT"] ?? null);
            echo "
\t\t</fieldset>
\t</form>
";
        }
        // line 77
        echo "
";
        // line 78
        // line 79
        echo "
";
        // line 80
        if (($context["S_DISPLAY_ONLINE_LIST"] ?? null)) {
            // line 81
            echo "\t<div class=\"stat-block online-list\">
\t\t";
            // line 82
            if (($context["U_VIEWONLINE"] ?? null)) {
                echo "<h3><a href=\"";
                echo ($context["U_VIEWONLINE"] ?? null);
                echo "\">";
                echo $this->env->getExtension('phpbb\template\twig\extension')->lang("WHO_IS_ONLINE");
                echo "</a></h3>";
            } else {
                echo "<h3>";
                echo $this->env->getExtension('phpbb\template\twig\extension')->lang("WHO_IS_ONLINE");
                echo "</h3>";
            }
            // line 83
            echo "\t\t<p>
\t\t\t";
            // line 84
            // line 85
            echo "\t\t\t";
            echo ($context["TOTAL_USERS_ONLINE"] ?? null);
            echo " (";
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("ONLINE_EXPLAIN");
            echo ")<br />";
            echo ($context["RECORD_USERS"] ?? null);
            echo "<br /> 
\t\t\t";
            // line 86
            if (($context["U_VIEWONLINE"] ?? null)) {
                // line 87
                echo "\t\t\t\t<br />";
                echo ($context["LOGGED_IN_USER_LIST"] ?? null);
                echo "
\t\t\t\t";
                // line 88
                if (($context["LEGEND"] ?? null)) {
                    echo "<br /><em>";
                    echo $this->env->getExtension('phpbb\template\twig\extension')->lang("LEGEND");
                    echo $this->env->getExtension('phpbb\template\twig\extension')->lang("COLON");
                    echo " ";
                    echo ($context["LEGEND"] ?? null);
                    echo "</em>";
                }
                // line 89
                echo "\t\t\t";
            }
            // line 90
            echo "\t\t\t";
            // line 91
            echo "\t\t</p>
\t</div>
";
        }
        // line 94
        echo "
";
        // line 95
        // line 96
        echo "
";
        // line 97
        if (($context["S_DISPLAY_BIRTHDAY_LIST"] ?? null)) {
            // line 98
            echo "\t<div class=\"stat-block birthday-list\">
\t\t<h3>";
            // line 99
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("BIRTHDAYS");
            echo "</h3>
\t\t<p>
\t\t\t";
            // line 101
            // line 102
            echo "\t\t\t";
            if (twig_length_filter($this->env, $this->getAttribute(($context["loops"] ?? null), "birthdays", array()))) {
                echo $this->env->getExtension('phpbb\template\twig\extension')->lang("CONGRATULATIONS");
                echo $this->env->getExtension('phpbb\template\twig\extension')->lang("COLON");
                echo " <strong>";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["loops"] ?? null), "birthdays", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["birthdays"]) {
                    echo $this->getAttribute($context["birthdays"], "USERNAME", array());
                    if (($this->getAttribute($context["birthdays"], "AGE", array()) !== "")) {
                        echo " (";
                        echo $this->getAttribute($context["birthdays"], "AGE", array());
                        echo ")";
                    }
                    if ( !$this->getAttribute($context["birthdays"], "S_LAST_ROW", array())) {
                        echo ", ";
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['birthdays'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                echo "</strong>";
            } else {
                echo $this->env->getExtension('phpbb\template\twig\extension')->lang("NO_BIRTHDAYS");
            }
            // line 103
            echo "\t\t\t";
            // line 104
            echo "\t\t</p>
\t</div>
";
        }
        // line 107
        echo "
";
        // line 108
        if (($context["NEWEST_USER"] ?? null)) {
            // line 109
            echo "\t<div class=\"stat-block statistics\">
\t\t<h3>";
            // line 110
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("STATISTICS");
            echo "</h3>
\t\t<p>
\t\t\t";
            // line 112
            // line 113
            echo "\t\t\t";
            echo ($context["TOTAL_POSTS"] ?? null);
            echo " &bull; ";
            echo ($context["TOTAL_TOPICS"] ?? null);
            echo " &bull; ";
            echo ($context["TOTAL_USERS"] ?? null);
            echo " &bull; ";
            echo ($context["NEWEST_USER"] ?? null);
            echo "
\t\t\t";
            // line 114
            // line 115
            echo "\t\t</p>
\t</div>
";
        }
        // line 118
        echo "
";
        // line 119
        // line 120
        echo "
";
        // line 121
        $location = "overall_footer.html";
        $namespace = false;
        if (strpos($location, '@') === 0) {
            $namespace = substr($location, 1, strpos($location, '/') - 1);
            $previous_look_up_order = $this->env->getNamespaceLookUpOrder();
            $this->env->setNamespaceLookUpOrder(array($namespace, '__main__'));
        }
        $this->loadTemplate("overall_footer.html", "index_body.html", 121)->display($context);
        if ($namespace) {
            $this->env->setNamespaceLookUpOrder($previous_look_up_order);
        }
    }

    public function getTemplateName()
    {
        return "index_body.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  407 => 121,  404 => 120,  403 => 119,  400 => 118,  395 => 115,  394 => 114,  383 => 113,  382 => 112,  377 => 110,  374 => 109,  372 => 108,  369 => 107,  364 => 104,  362 => 103,  336 => 102,  335 => 101,  330 => 99,  327 => 98,  325 => 97,  322 => 96,  321 => 95,  318 => 94,  313 => 91,  311 => 90,  308 => 89,  299 => 88,  294 => 87,  292 => 86,  283 => 85,  282 => 84,  279 => 83,  267 => 82,  264 => 81,  262 => 80,  259 => 79,  258 => 78,  255 => 77,  248 => 73,  243 => 72,  237 => 70,  234 => 69,  226 => 67,  224 => 66,  217 => 65,  210 => 64,  195 => 62,  190 => 61,  188 => 60,  185 => 59,  184 => 58,  181 => 57,  169 => 56,  161 => 51,  157 => 50,  149 => 45,  145 => 44,  141 => 42,  134 => 38,  130 => 37,  127 => 36,  125 => 35,  122 => 34,  115 => 30,  111 => 29,  108 => 28,  106 => 27,  103 => 26,  96 => 22,  92 => 21,  89 => 20,  87 => 19,  84 => 18,  82 => 17,  79 => 16,  78 => 15,  75 => 14,  66 => 10,  63 => 9,  61 => 8,  58 => 7,  57 => 6,  54 => 5,  48 => 4,  34 => 3,  31 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "index_body.html", "");
    }
}
