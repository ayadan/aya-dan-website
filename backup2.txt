
/* Layout */
header { text-align: center; padding-top: 10px; }
.site-logo img { width: 95%; margin: 5px auto; display: block; }

nav ul li,
nav h2,
nav h3,
nav p,
nav h4 { padding-left: 10px; }


nav { padding: 0; width: 100%; margin: 0; }


.left-sidebar { padding-right: 0; padding-bottom: 100px; position: absolute; }
.right-sidebar { padding-left: 0; padding-bottom: 100px; }
nav h3 { margin-top: 30px; }
nav.sub-links { padding-left: 0; }
nav ul { padding: 0; margin: 0; }
nav ul li {  margin-top: 5px; margin-bottom: 5px; }
nav h4 { margin-top: 15px; }
footer { padding-top: 40px; }
nav hr { height: 5px; border: none; background: #ffc000; width: 90%; margin-top: 30px; margin-bottom: 0px; margin-left: auto; margin-right: auto; }
.view-more { text-align: right; }
nav li img { height: 30px; }

footer ul li ul { margin-left: 20px; }

/* Colors and style */
body { background: #000; }
.left-sidebar   { background: #67d4ff;  color: #042283; }
.right-sidebar  { background: #67d4ff;  color: #042283; }
.main-view      { background: #fff;     color: #333; }

nav ul { list-style-type: none; }
nav ul li:hover { background: #ffc000; width: 100%; }
nav { font-size: 1.5em; font-weight: bold; }
nav.sub-links { font-size: 1em; }
nav ul a { color: #042283; }
nav ul a:hover { color: #042283; }

nav h4 { font-size: 1.2em; }

footer { background: #042283; color: #fff; font-size: 9pt; border-top: solid 5px #ffc000; padding-bottom: 50px; }
footer a,
footer a:hover { color: #fff; }

footer ul { list-style-type: square; padding: 0; margin: 0; }

/* Widgets */

.image-float-right { float: right; margin: 10px; }

