<? $title = "Comics"; ?>
<? include_once( "../content/layout/sub-header.php" ); ?>
<? include_once( "../content/php/functions.php" ); ?>

<? $comics = Storage::GetComics();
?>

<?
$pageLanguage = "";
if ( isset( $_GET["language"] ) ) {
    $pageLanguage = $_GET["language"];
}
?>

<div class="container">
    <div class="row"> <h1>Comics</h1> </div>

    <div class="row">
        
<? if ( $pageLanguage == "" ) { ?>  <div class="quick-nav"> Quick Nav:  <? foreach ( $comics as $key=>$language ) { ?> <a href="#<?=$key?>"><?=$language["name"]?></a> <? } ?> </div>
<? } else { ?> <p><a href="http://www.ayadanconlangs.com/videos/">&lt;&lt; View videos for all languages</a></p> <? } ?>

    </div>
    <div class="row">
        <div class="col-md-12">

            <? foreach ( $comics as $key=>$language ) {
                if ( $pageLanguage != "" && $key != $pageLanguage ) { continue; }
                ?>
                <a name="<?=$key?>"></a><h2><?=$language["name"] ?></h2>
                <? $i = 0; ?>
                <? foreach ( $language["serieses"] as $series ) { ?>
                    <div class="language-videos row">
                        <div class="col-md-12">
                            <h3><?=$series["name"] ?></h3>
                            <p class="author">By <?=$series["creator"]?></p>
                        </div>
                        <? foreach( $series["comics"] as $comickey=>$comic ) { ?>
                        <div class="series-item grid-item  col-md-3">
                            <div class="comics-box">
                                <p class="title">Part <?=$comickey+1 ?></p>
                                <p><a href="<?=$series["key"]?>/<?= $comic["image"] ?>"><img src="<?=$series["key"]?>/<?= $comic["image"] ?>"></a></p>
                            </div>
                        </div>
                        <? } ?>
                    </div>
                    <hr>
                <? } ?>
                <hr>
            <? } ?>

        </div>
    </div>
</div>

<? include_once( "../content/layout/sub-footer.php" ); ?>
