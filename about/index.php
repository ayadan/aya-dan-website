<? $title = "About"; ?>
<? include_once( "../content/layout/sub-header.php" ); ?>

<h1>About Áya Dan</h1>

<p>Áya Dan is a blog and website focused on conlangs (constructed languages). Blog posts are written by various authors, about topics from language construction to entertainment to LGBTQIA+ issues.</p>

<p>Also, read about the Áya Dan YouTube channel here.</p>

<p>Each conlang page of the website contains links to resources that can be handy for learning or using the conlang, such as lessons, reading material, entertainment, dictionaries, and tools.</p>

<p>Anyone can contribute to Áya Dan, so long as the content is respectful of others!</p>

<p>Áya Dan is the aggregation of several conlang blogs, formerly: Nia Ido, Lolehoth, Esperanimeo, Pipi Suwi, and La Aliuloj.</p>

<p>The creator of Áyadan is Rachel J Morris, a programmer and game developer interested in conlangs and entertainment.</p>

<p>Contact Rachel about the blog at: Rachel@moosader.com</p>

<h3>&quot;Áya Dan&quot;?</h3>

<p>Áya = Beautiful, Dan = Language</p>

<p>In Láadan, one might express, “Bíi áya dan wa.” – This would roughly translate to, “I declare that language is beautiful, I know it because I have perceived it.”</p>

<p>Shortened down to exclude the speech-act morpheme and evidence morphemes, we just get “áya dan” :)</p>

<h2>Authors</h2>

<p>Authors to Áya Dan post whenever they have time or something to talk about! We’re not very organized. :) (Psst, interested in being an author?)</p>

<p><strong>Active contributors</strong></p>

<ul>
    <li>Rachel Wil Sha Singh</li>
    <li>Becca</li>
</ul>

<p><strong>Past contributors</strong></p>

<ul>
    <li>Aubrenjo</li>
    <li>Sencerbulino</li>
</ul>

<h2>Contribute to Áya Dan!</h2>

<p>If you would like to contribute to Áyadan, we can set up an account for you to post with. Generally, your posts will need to be about natural languages or constructed languages, but otherwise any interesting topics or submissions are welcome!</p>

<h3>Guest Posts and Blog Authors</h3>

<p>You can request that we make a one-time guest post about your project or article, or you can become an author and post to Áyadan whenever you have something to say!</p>

<p>Bonus if you are an author, artist, musician, or other type of creative person who likes to create original content in conlangs! :)</p>

<p>Email Rachel at: Rachel@Moosader.com to discuss more!</p>

<? include_once( "../content/layout/sub-footer.php" ); ?>
