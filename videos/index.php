<?
    $title = "Videos";
    include_once( "../content/php/functions.php" );
    include_once( "../content/themes/2019-01/header.php" );
?>

<? $videos = Storage::GetVideos(); ?>

<?
$pageLanguage = "";
if ( isset( $_GET["language"] ) ) {
    $pageLanguage = $_GET["language"];
}
if ( isset( $_GET["video_id"] ) ) {
    $videoId = $_GET["video_id"];
    $language = $_GET["video_language"];
    $video = Storage::GetVideo( $videoId, $language );
}
?>

<?
if ( isset( $_GET["video_id"] ) ) {
?>
<!-- ----------------------- -->
<!-- - VIDEO PLAYER -------- -->
<!-- ----------------------- -->
<?
// https://www.youtube.com/watch?v=<?=$video['YouTube ID']
?>
<script>
    $( document ).ready( function() {
        // Get window width
        var width = parseInt( $( ".video-player" ).css( "width" ), 10 );
        console.log( width );
        var ratio = 1280/720;
        var height = width / ratio;
        $( "#video-frame" ).attr( "width", width );
        $( "#video-frame" ).attr( "height", height );
    } );
</script>

<section class="section video-preview preview-pane cf">
    <div class="row-container">
        <div class="header-tab col-25">
            <div class="contents">
                <p>
                    <span class="title"><a href="https://www.youtube.com/watch?v=<?=$video['YouTube ID']?>"><?=$video['name']?></a></span>
                    <span class="author">by <?=$video['creator']?>,</span>
                    <span class="date"><?=Storage::YMDToDate($video['date'] )?></span>
                </p>
                <p><a href="http://www.ayadanconlangs.com/videos/">&lt;&lt; Back</a></p>
            </div>
        </div>
        
        <div class="row-container col-75">
            <div class="contents video-player">
                <iframe id="video-frame" width="560" height="315" src="https://www.youtube.com/embed/<?=$videoId?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>
<?
} else {
?>
<!-- ----------------------- -->
<!-- - VIDEO GALLERY ------- -->
<!-- ----------------------- -->



<?
function VideoPreview( $video, $language ) {
    $url = "?video_id=" . $video['YouTube ID'] . "&video_language=" . $language;
?>
    <div class="col-25">
        <p class="preview-image">
            <a href="<?=$url?>">
                <img src="https://img.youtube.com/vi/<?=$video['YouTube ID']?>/default.jpg">
            </a>
        </p>
        <p>
            <span class="title"><a href="<?=$url?>"><?=$video['name']?></a></span>
            <span class="author">by <?=$video['creator']?>,</span>
            <span class="date"><?=Storage::YMDToDate($video['date'] )?></span>
        </p>
    </div>
<?
}
?>

<? foreach ( $videos as $key=>$language ) {
    if ( $pageLanguage != "" && $key != $pageLanguage ) { continue; }
    ?>
<section class="section video-preview preview-pane cf">
    <div class="row-container">
        <div class="header-tab col-20">
            <div class="contents">
                <h2><?=$language["name"] ?></h2>
                <? if ( $pageLanguage == "" ) { ?>
                <p><a href="?language=<?=$key?>">View all &gt;&gt;</a></p>
                <? } else { ?>
                <p><a href="http://www.ayadanconlangs.com/videos/">&lt;&lt; Back</a></p>
                <? } ?>
            </div>
        </div>
        <div class="row-container col-80 cf">

            <?
            $i = 0;
            foreach ( $language["videos"] as $video ) {
                VideoPreview( $video, $key );
                
                $i++;
                if ( $i != 0 && $i % 4 == 0 ) {
                    if ( $pageLanguage == "" ) {
                        break;
                    } else {
                        // Break row
                        ?>
        </div>
        <div class="clear-tab col-20">
            &nbsp;
        </div>
        <div class="row-container col-80 cf">
                        <?
                    }
                }
                
            } // foreach ( $language["videos"] as $video )
            ?>
        </div>
    </div>
</section>
    <?
    } // foreach ( $videos as $key=>$language ) {
?>
<?
}
?>





<? include_once( "http://www.ayadanconlangs.com/content/themes/2019-01/footer.php" ); ?>
