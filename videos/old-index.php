<? $title = "Videos"; ?>
<? include_once( "../content/themes/2019-01/header.php" ); ?>
<? //include_once( "../content/layout/sub-header.php" ); ?>
<? include_once( "../content/php/functions.php" ); ?>

<? $videos = Storage::GetVideos(); ?>

<?
$pageLanguage = "";
if ( isset( $_GET["language"] ) ) {
    $pageLanguage = $_GET["language"];
}
?>

<div class="container">
    <div class="row"> <h1>Videos</h1> </div>
    <div class="row">
        
        <? if ( $pageLanguage == "" ) { ?>  <div class="quick-nav"> Quick Nav:  <? foreach ( $videos as $key=>$language ) { ?> <a href="#<?=$key?>"><?=$language["name"]?></a> <? } ?> </div>
        <? } else { ?> <p><a href="http://www.ayadanconlangs.com/videos/">&lt;&lt; View videos for all languages</a></p> <? } ?>

    </div>
    <div class="row">
          
        <? foreach ( $videos as $key=>$language ) {
            if ( $pageLanguage != "" && $key != $pageLanguage ) { continue; }
            ?>
            <div class="col-md-12">
                <a name="<?=$key?>"></a><h2><?=$language["name"] ?></h2>
                <div class="language-videos row">
                <? $i = 0; ?>
                <? foreach ( $language["videos"] as $video ) { ?>
                    <div class="video-item col-md-3">
                            <a href="https://www.youtube.com/watch?v=<?=$video['YouTube ID']?>"><img src="https://img.youtube.com/vi/<?=$video['YouTube ID']?>/default.jpg"></a>
                            <p class="title"><a href="https://www.youtube.com/watch?v=<?=$video['YouTube ID']?>"><?=$video['name']?></a></p>
                            <p class="author">By <?=$video['creator']?></p>
                            <p class="date"><?=$video['date']?></p>
                        </div>
                    <?
                    $i++;
                    if ( $i != 0 && $i % 4 == 0 ) {
                        if ( $pageLanguage == "" ) {
                            ?>
                            </div>
                            <div class="row view-all-link">
                                <div class="col-md-12">
                                    <a href="?language=<?=$key?>">View all <?=sizeof( $language["videos"] ) ?> <?=$language["name"]?> videos &gt;&gt;</a>
                                </div>
                            
                            <?
                            break;
                        } else {
                            ?> </div><div class="language-videos row"> <?
                        }
                    }
                    ?>
                    
                <? } ?>
            </div>
        </div>
        <hr>
    <? } ?>
    </div>
</div>

<? include_once( "../content/themes/2019-01/footer.php" ); ?>
<? //include_once( "../content/layout/sub-footer.php" ); ?>
