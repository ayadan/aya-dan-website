LUDANTO_ILO = {
    ludantoj: [],
    klavoj: {
        l1_supren: false,
        l1_malsupren: false,
        l2_supren: false,
        l2_malsupren: false,
        },
    klavokodo: {
        supren: 38, malsupren: 40,
        w: 87,      s: 83
        },
    
    VakigiLudantoj: function() {
        while ( LUDANTO_ILO.ludantoj.length > 0 ) { LUDANTO_ILO.ludantoj.pop(); }
    },

    Komenco: function() {
        // Ni bezonas du ludantojn
        var ludanto1 = { tipo: "ludanto",
            x: 25, y: ( ludostato.agordoj.alto / 2 ) - ( 75 / 2 ),
            largxo: 56, alto: 75, rapidoH: 0, rapidoV: 0, maksimumrapido: 5 };
        ludanto1.image = new Image();
        ludanto1.image.src = "assets/maldekstra_ludanto.png";
        LUDANTO_ILO.ludantoj.push( ludanto1 );

        var ludanto2 = { tipo: "ludanto",
            x: 559, y: ( ludostato.agordoj.alto / 2 ) - ( 75 / 2 ),
            largxo: 56, alto: 75, rapidoH: 0, rapidoV: 0, maksimumrapido: 5 };
        ludanto2.image = new Image();
        ludanto2.image.src = "assets/dekstra_ludanto.png";
        LUDANTO_ILO.ludantoj.push( ludanto2 );

        // terpomo
        var terpomo = { tipo: "ero",
            x: ( ludostato.agordoj.largxo / 2 ) - ( 22 / 2 ),
            y: ( ludostato.agordoj.alto / 2 ) - ( 28 / 2 ),
            largxo: 22, alto: 28, rapidoH: 1, rapidoV: 0, maksimumrapido: 5 };
        terpomo.image = new Image();
        terpomo.image.src = "assets/terpomo.png";
        LUDANTO_ILO.ludantoj.push( terpomo );
    },

    Gxisdatigi: function() {
        // Rapidiĝi
        if ( LUDANTO_ILO.klavoj.l1_supren ) {
            LUDANTO_ILO.SxaltiDirekton( 0, 0, -1 );
        }
        else if ( LUDANTO_ILO.klavoj.l1_malsupren ) {
            LUDANTO_ILO.SxaltiDirekton( 0, 0, 1 );
        }
        else {
            LUDANTO_ILO.SxaltiDirekton( 0, 0, 0 );
        }

        if ( LUDANTO_ILO.klavoj.l2_supren ) {
            LUDANTO_ILO.SxaltiDirekton( 1, 0, -1 );
        }
        else if ( LUDANTO_ILO.klavoj.l2_malsupren ) {
            LUDANTO_ILO.SxaltiDirekton( 1, 0, 1 );
        }
        else {
            LUDANTO_ILO.SxaltiDirekton( 1, 0, 0 );
        }

        for ( var i = 0; i < LUDANTO_ILO.ludantoj.length; i++ ) {
            // Translokigi
            if ( LUDANTO_ILO.ludantoj[i].rapidoV != 0 ) {
                LUDANTO_ILO.ludantoj[ i ].y += LUDANTO_ILO.ludantoj[i].rapidoV;
            }
            if ( LUDANTO_ILO.ludantoj[i].rapidoH != 0 ) {
                LUDANTO_ILO.ludantoj[ i ].x += LUDANTO_ILO.ludantoj[i].rapidoH;
            }

            if ( LUDANTO_ILO.ludantoj[i].y < 0 ) {
                if ( LUDANTO_ILO.ludantoj[i].tipo == "ludanto" ) {
                    LUDANTO_ILO.ludantoj[i].y = 0;
                    LUDANTO_ILO.ludantoj[i].rapidoV = 0;
                }
                else {
                    LUDANTO_ILO.ludantoj[i].rapidoV = -LUDANTO_ILO.ludantoj[i].rapidoV;
                }
            }
            else if ( LUDANTO_ILO.ludantoj[i].y > 360 - LUDANTO_ILO.ludantoj[i].alto ) {
                if ( LUDANTO_ILO.ludantoj[i].tipo == "ludanto" ) {
                    LUDANTO_ILO.ludantoj[i].y = 360 - LUDANTO_ILO.ludantoj[i].alto;
                    LUDANTO_ILO.ludantoj[i].rapidoV = 0;
                }
                else {
                    LUDANTO_ILO.ludantoj[i].rapidoV = -LUDANTO_ILO.ludantoj[i].rapidoV;
                }
            }            
                
            // kolizo
            if ( LUDANTO_ILO.ludantoj[ i ].tipo == "ero" ) {
                // Terpomo forlasis la ekranon
                if ( LUDANTO_ILO.ludantoj[ i ].x < -132 ) {
                    LUDANTO_ILO.ludantoj[ i ].x = ( ludostato.agordoj.largxo / 2 ) - ( 22 / 2 );
                    LUDANTO_ILO.ludantoj[ i ].y = ( ludostato.agordoj.alto / 2 ) - ( 28 / 2 );
                    
                    if ( LUDANTO_ILO.ludantoj[i].rapidoH > 0 ) {
                        LUDANTO_ILO.ludantoj[i].rapidoH = -1; 
                    } else { 
                        LUDANTO_ILO.ludantoj[i].rapidoH = 1;
                    }
                    LUDANTO_ILO.ludantoj[i].rapidoV = 0;
                    ludostato.AldoniPoentojn( 1 );
                }
                else if ( LUDANTO_ILO.ludantoj[ i ].x > ludostato.agordoj.largxo + 100 ) {
                    LUDANTO_ILO.ludantoj[ i ].x = ( ludostato.agordoj.largxo / 2 ) - ( 22 / 2 );
                    LUDANTO_ILO.ludantoj[ i ].y = ( ludostato.agordoj.alto / 2 ) - ( 28 / 2 );
                    
                    if ( LUDANTO_ILO.ludantoj[i].rapidoH > 0 ) {
                        LUDANTO_ILO.ludantoj[i].rapidoH = -1; 
                    } else { 
                        LUDANTO_ILO.ludantoj[i].rapidoH = 1;
                    }
                    LUDANTO_ILO.ludantoj[i].rapidoV = 0;
                    ludostato.AldoniPoentojn( 0 );
                }
                
                // Ludanto batis terpomon
                for ( var j = 0; j < LUDANTO_ILO.ludantoj.length; j++ ) { 
                    var t = LUDANTO_ILO.ludantoj[ i ];
                    var l = LUDANTO_ILO.ludantoj[ j ];
                    if ( l.tipo == "ludanto" &&
                         t.x            < l.x + l.largxo &&
                         t.x + t.largxo > l.x &&
                         t.y            < l.y + l.alto &&
                         t.y + t.alto   > l.y 
                         ) {
                             
                        t.rapidoH = -t.rapidoH;
                        var h = 0;
                        var v = 0;
                        if ( t.rapidoH < 0 ) {
                            h = -1;
                        } else if ( t.rapidoH > 0 ) { 
                            h = 1;
                        }
                        
                        var mezY = t.y + ( t.alto / 2 );
                        var triona = l.alto / 3;
                        // Triona...
                        if ( l.y < mezY && mezY < l.y + triona ) {
                            // 1/3
                            v = -1;
                        }
                        else if ( l.y + ( triona * 2 )  < mezY && mezY < l.y + ( triona * 3 ) ) {
                            // 3/3
                            v = 1;
                        }
                        else {
                            // 2/3
                            v = 0;
                        }
                        
                        LUDANTO_ILO.SxaltiDirekton( 2, h, v );
                        SONO_ILO.LudiSonon( "bati" );
                    }
                }
            }
        }
    },

    SxaltiKlavon: function( ev ) {
        if ( ev.keyCode == LUDANTO_ILO.klavokodo.supren ) {
            LUDANTO_ILO.klavoj.l2_supren = true;
        }
        else if ( ev.keyCode == LUDANTO_ILO.klavokodo.malsupren ) {
            LUDANTO_ILO.klavoj.l2_malsupren = true;
        }

        if ( ev.keyCode == LUDANTO_ILO.klavokodo.w ) {
            LUDANTO_ILO.klavoj.l1_supren = true;
        }
        else if ( ev.keyCode == LUDANTO_ILO.klavokodo.s ) {
            LUDANTO_ILO.klavoj.l1_malsupren = true;
        }
    },

    MalsxaltiKlavon: function( ev ) {
        if ( ev.keyCode == LUDANTO_ILO.klavokodo.supren ) {
            LUDANTO_ILO.klavoj.l2_supren = false;
        }
        else if ( ev.keyCode == LUDANTO_ILO.klavokodo.malsupren ) {
            LUDANTO_ILO.klavoj.l2_malsupren = false;
        }

        if ( ev.keyCode == LUDANTO_ILO.klavokodo.w ) {
            LUDANTO_ILO.klavoj.l1_supren = false;
        }
        else if ( ev.keyCode == LUDANTO_ILO.klavokodo.s ) {
            LUDANTO_ILO.klavoj.l1_malsupren = false;
        }
    },

    SxaltiDirekton: function( i, rapidoH, rapidoV ) {
        if ( rapidoV == 1 ) {
            //plirapidi malsupren
            LUDANTO_ILO.ludantoj[ i ].rapidoV += 1;
        }
        else if ( rapidoV == -1 ) {
            //plirapidi supren
            LUDANTO_ILO.ludantoj[ i ].rapidoV -= 1;
        }
        else {
            //malplirapidi
            if ( LUDANTO_ILO.ludantoj[ i ].rapidoV > 0 ) {
                LUDANTO_ILO.ludantoj[ i ].rapidoV -= 0.5;
            }
            else if (LUDANTO_ILO.ludantoj[ i ].rapidoV < 0 ) {
                LUDANTO_ILO.ludantoj[ i ].rapidoV += 0.5;
            }
        }

        if ( rapidoH == 1 ) {
            //plirapidi malsupren
            LUDANTO_ILO.ludantoj[ i ].rapidoH += 1;
        }
        else if ( rapidoH == -1 ) {
            //plirapidi supren
            LUDANTO_ILO.ludantoj[ i ].rapidoH -= 1;
        }

        if ( LUDANTO_ILO.ludantoj[ i ].rapidoH < -(LUDANTO_ILO.ludantoj[ i ].maksimumrapido) ) {
            LUDANTO_ILO.ludantoj[ i ].rapidoH = -(LUDANTO_ILO.ludantoj[ i ].maksimumrapido);
        }
        else if ( LUDANTO_ILO.ludantoj[ i ].rapidoH > LUDANTO_ILO.ludantoj[ i ].maksimumrapido ) {
            LUDANTO_ILO.ludantoj[ i ].rapidoH = LUDANTO_ILO.ludantoj[ i ].maksimumrapido;
        }

        if ( LUDANTO_ILO.ludantoj[ i ].rapidoV < -(LUDANTO_ILO.ludantoj[ i ].maksimumrapido) ) {
            LUDANTO_ILO.ludantoj[ i ].rapidoV = -(LUDANTO_ILO.ludantoj[ i ].maksimumrapido);
        }
        else if ( LUDANTO_ILO.ludantoj[ i ].rapidoV > LUDANTO_ILO.ludantoj[ i ].maksimumrapido ) {
            LUDANTO_ILO.ludantoj[ i ].rapidoV = LUDANTO_ILO.ludantoj[ i ].maksimumrapido;
        }
    },

    Desegni: function( kanvaso ) {
        for ( var i = 0; i < LUDANTO_ILO.ludantoj.length; i++ ) {
            kanvaso.drawImage(
                LUDANTO_ILO.ludantoj[i].image,
                0, 0, LUDANTO_ILO.ludantoj[i].largxo, LUDANTO_ILO.ludantoj[i].alto,
                LUDANTO_ILO.ludantoj[i].x, LUDANTO_ILO.ludantoj[i].y,
                LUDANTO_ILO.ludantoj[i].largxo, LUDANTO_ILO.ludantoj[i].alto );
        }
    },
};
