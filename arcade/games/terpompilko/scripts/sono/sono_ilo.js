SONO_ILO = {
    sonoj: {},
    nunKanto: null,
    
    Komenco: function() {
        SONO_ILO.sonoj.bati = new Audio( "assets/bfxr_hit.wav" );
        SONO_ILO.sonoj.bati.volume = 0.5;
        SONO_ILO.sonoj.bati.estasMuziko = false;
        
        SONO_ILO.sonoj.pasi = new Audio( "assets/bfxr_powerup.wav" );
        SONO_ILO.sonoj.pasi.volume = 0.5;
        SONO_ILO.sonoj.bati.estasMuziko = false;
        
        SONO_ILO.sonoj.butono = new Audio( "assets/bfxr_button.wav" );
        SONO_ILO.sonoj.butono.volume = 0.5;
        SONO_ILO.sonoj.butono.estasMuziko = false;
        
        SONO_ILO.sonoj.ludoMuziko = new Audio( "assets/Forest_tgfcoder.mp3" );
        SONO_ILO.sonoj.ludoMuziko.volume = 0.5;
        SONO_ILO.sonoj.ludoMuziko.loop = true;
        SONO_ILO.sonoj.ludoMuziko.estasMuziko = true;
        
        SONO_ILO.sonoj.menuoMuziko = new Audio( "assets/HappyMenu_tgfcoder.mp3" );
        SONO_ILO.sonoj.menuoMuziko.volume = 0.5;
        SONO_ILO.sonoj.menuoMuziko.loop = true;
        SONO_ILO.sonoj.menuoMuziko.estasMuziko = true;
    },
    
    LudiSonon: function( titolo ) {
        if ( SONO_ILO.sonoj[ titolo ] != null ) {
            if ( SONO_ILO.sonoj[ titolo ].estasMuziko == false ) {
                SONO_ILO.sonoj[ titolo ].play();
            }
        }
    },
    
    LudiMuzikon: function( titolo ) {
        if ( SONO_ILO.sonoj[ titolo ] != null ) {
            if ( SONO_ILO.sonoj[ titolo ].estasMuziko
                 && SONO_ILO.nunKanto != titolo ) {
                SONO_ILO.nunKanto = titolo;
                SONO_ILO.HaltuMuzikon();
                SONO_ILO.sonoj[ titolo ].play();
            }
        }
    },
    
    HaltuMuzikon: function() {
        for ( var i in SONO_ILO.sonoj ) {
            // Stop it if this is an Audio item
            if ( SONO_ILO.sonoj[ i ].estasMuziko ) {
                SONO_ILO.sonoj[ i ].pause();
                SONO_ILO.nunKanto = null;
            }
        }
    }
};
