<? $title = "Conlang Arcade"; ?>
<? include_once( "../content/layout/sub-header.php" ); ?>

<!-- jQuery Modal -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />

<?
$game = "";
if ( isset( $_GET["game"] ) ) {
    $game = $_GET["game"];
    $width = $_GET["width"];
    $height = $_GET["height"];

    if ( $game == "asteroidoj" )
    {
        $gameName = "Asteroidoj";
        $author = "Tea Blossom";
    }
    else if ( $game == "invadantoj" )
    {
        $gameName = "Invadantoj el Krokodilujo";
        $author = "Rachel Singh";
    }
    else if ( $game == "punch-a-nazi" )
    {
        $gameName = "Pugnu Naziojn";
        $author = "Rachel Singh";
    }
    else if ( $game == "terpompilko" )
    {
        $gameName = "Terpompilko";
        $author = "Rachel Singh";
    }
    else if ( $game == "thedehoth" )
    {
        $gameName = "Thedeyo";
        $author = "Rachel Singh";
    }
}
?>

<div class="container">
    <div class="row">
        <h1>Conlang Arcade</h1>
    </div>

        <div class="row">
<? if ( $game == "" ) { ?> <div class="quick-nav"> Quick Nav:  <a href="#esperanto">Esperanto</a> <a href="#laadan">Láadan</a> <a href="#links">Links</a> </div>
<? } else { ?> <p><a href="http://www.ayadanconlangs.com/arcade/">&lt;&lt; View all games</a></p> <? } ?>
        </div>
</div>

<?
if ( $game == "" )
{
?>

<div class="container">
    <a name="esperanto"></a><h2>Esperantaj Ludoj</h2>
    
    <div class="grid-view row">
        <div class="grid-item col-md-3">
            <p><a href="?game=asteroidoj&width=800&height=600"><img src="arcade-content/thumbnails/asteroidoj.png"><br>
            Asteroidoj</a></p>
            <p class="author">de Tea Blossom</p>
        </div>
        
        <div class="grid-item col-md-3">
            <p><a href="?game=invadantoj&width=800&height=400"><img src="arcade-content/thumbnails/inva.png"><br>
            Invadantoj el Krokodilujo</a></p>
            <p class="author">de Rachel Singh</p>
        </div>
        
        <div class="grid-item col-md-3">
            <p><a href="?game=punch-a-nazi&width=850&height=600"><img src="arcade-content/thumbnails/pugnu.png"><br>Pugnu Naziojn</a></p>
            <p class="author">de Rachel Singh</p>
        </div>
        
        <div class="grid-item col-md-3">
            <p><a href="?game=terpompilko&width=800&height=400"><img src="arcade-content/thumbnails/terpom.png"><br>
            Terpompilko</a></p>
            <p class="author">de Rachel Singh</p>
        </div>
    </div>

    <hr>
    
    <a name="laadan"></a><h2>Láadaneshida</h2>
    
    <div class="grid-view row">
        <div class="grid-item col-md-3">
            <p><a href="?game=thedehoth&width=800&height=500"><img src="arcade-content/thumbnails/thedeyo.png"><br>
            Thedeyo</a></p>
            <p class="author">by Rachel Singh</p>
        </div>
    </div>

    <hr>

    <section class="game-list links">
        <a name="links"></a><h2>Links</h2>
        <p>Conlang games in other places...</p>

        <div class="grid-view row">
                
            <div class="grid-item col-md-2"><p><a href="http://www.romhacking.net/translations/2663/"><img src="arcade-content/thumbnails-links/batman.png"></p><p class="title">Batman</a></p></div>
            <div class="grid-item col-md-2"><p><a href="https://www.wesnoth.org/"><img src="arcade-content/thumbnails-links/wesnoth.jpg"></p><p class="title">Battle for Wesnoth</a></p></div>
            <div class="grid-item col-md-2"><p><a href="http://www.nexusmods.com/fallout4/mods/10742"><img src="arcade-content/thumbnails-links/fallout.jpg"></p><p class="title">Fallout 4</a></p></div>
            <div class="grid-item col-md-2"><p><a href="http://www.freeciv.org/"><img src="arcade-content/thumbnails-links/freeciv.png"></p><p class="title">FreeCiv</a></p></div>
            <div class="grid-item col-md-2"><p><a href="https://www.gnu.org/software/freedink/"><img src="arcade-content/thumbnails-links/dink.png"></p><p class="title">FreeDink</a></p></div>
            <div class="grid-item col-md-2"><p><a href="https://vanege.itch.io/frenezurbeto"><img src="arcade-content/thumbnails-links/frenezurbeto.png"></p><p class="title">Frenezurbeto</a></p></div>
            <div class="grid-item col-md-2"><p><a href="https://scratch.mit.edu/projects/1399979/"><img src="arcade-content/thumbnails-links/fruitcraft.png"></p><p class="title">Fruitcraft RPG</a></p></div>
            <div class="grid-item col-md-2"><p><a href="https://vanege.itch.io/fungokolekt"><img src="arcade-content/thumbnails-links/fungo.png"></p><p class="title">Fungokolekt'</a></p></div>
            <div class="grid-item col-md-2"><p><a href="http://www.romhacking.net/translations/2202/"><img src="arcade-content/thumbnails-links/zelda2.png"></p><p class="title">Zelda II</a></p></div>
            <div class="grid-item col-md-2"><p><a href="http://www.adventuregamestudio.co.uk/site/games/game/401/"><img src="arcade-content/thumbnails-links/mmd.jpg"></p><p class="title small-text">Maniac Mansion Deluxe</a></p></div>
            <div class="grid-item col-md-2"><p><a href="https://minecraft.net/en/"><img src="arcade-content/thumbnails-links/minecraft.jpg"></p><p class="title">Minecraft</a></p></div>
            <div class="grid-item col-md-2"><p><a href="http://www.openttd.org/en/"><img src="arcade-content/thumbnails-links/openttd.png"></p><p class="title">OpenTTD</a></p></div>
            <div class="grid-item col-md-2"><p><a href="https://osu.ppy.sh/"><img src="arcade-content/thumbnails-links/osu.png"></p><p class="title">Osu!</a></p></div>
            <div class="grid-item col-md-2"><p><a href="http://ri-li.sourceforge.net/"><img src="arcade-content/thumbnails-links/rili.jpg"></p><p class="title">Ri-Li</a></p></div>
            <div class="grid-item col-md-2"><p><a href="https://play.google.com/store/apps/details?id=com.shatteredpixel.shatteredpixeldungeon&hl=en&rdid=com.shatteredpixel.shatteredpixeldungeon"><img src="arcade-content/thumbnails-links/spd.png"></p><p class="title small-text">Shattered Pixel Dungeon</a></p></div>
            <div class="grid-item col-md-2"><p><a href="http://skrablo.ikso.net/"><img src="arcade-content/thumbnails-links/skrablo.gif"></p><p class="title">Skrablo</a></p></div>
            <div class="grid-item col-md-2"><p><a href="https://gamescarab.itch.io/super-jetpack-lizard"><img src="arcade-content/thumbnails-links/jetpack.png"></p><p class="title small-text">Super Jetpack Lizard</a></p></div>
            <div class="grid-item col-md-2"><p><a href="https://www.teeworlds.com/"><img src="arcade-content/thumbnails-links/teeworlds.png"></p><p class="title">Teeworlds</a></p></div>
            <div class="grid-item col-md-2"><p><a href="https://ludisto.com/app/what-the-shell/"><img src="arcade-content/thumbnails-links/wts.png"></p><p class="title">What the Shell</a></p></div>
        
        </div>
        
    </section>
</div>
<?
}
else
{
?>

<div class="container arcade">
    <div class="row">
        <div class="col-md-12">
            <h2><?=$gameName?></h2>
            <p>by <?=$author?></p>
            <iframe src="http://www.ayadanconlangs.com/arcade/games/<?=$game?>" style="width:<?=$width?>px; height:<?=$height?>px;"/>
        </div>
    </div>
</div>
<?
}
?>
        
<? include_once( "../content/layout/sub-footer.php" ); ?>
