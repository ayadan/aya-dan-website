$( document ).ready( function() { 
       
    function setIFrame( path ) {
        $( "#game-frame" ).attr( "src", path );
    };
    
    $( ".ca a" ).click( function() {
        var link = $( this ).attr( "path" );
        setIFrame( link );
        $( "#game-modal" ).modal(
            { escapeClose: false,
            clickClose: false,
            showClose: false }
        );
    } );
    
    $( "#close-game" ).click( function() {
        $( "#game-frame" ).attr( "src", "" );
    } );
} );
