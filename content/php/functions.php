<?
class Storage {
    private static $pagePath = "page-config/page.json";
    private static $coursePath = "course-content/";
    
    
    private static function ReadJson( $path )
    {
        $raw = file_get_contents( $path );
        $array = json_decode( $raw, true );
        return $array;
    }

    private static function WriteJson( $path, $array )
    {
        $raw = json_encode( $array, JSON_PRETTY_PRINT );
        if ( $raw === FALSE ) { echo( "<p>Error: couldn't load " . $path . "!</p>" ); }

        if ( file_put_contents( $path, $raw ) )
        {
            // Success
        }
        else
        {
            // Fail
            DisplayError( "Failed to save grocery list!" );
        }
    }

    private static function GetRss( $path )
    {
        if ( @simplexml_load_file( $path ) )
        {
            $feed = simplexml_load_file( $path );
            return $feed;
        }
        return null;
    }

    public static function DayMonthDate( $date )
    {
        return date( "D, M j", $date );
    }

    public static function MonthDate( $date )
    {
        return date( "M j", $date );
    }

    public static function WordpressToDate( $string )
    {
        
        $year = substr( $string, 11, 5 );
        $month = substr( $string, 8, 3 );
        $day = substr( $string, 5, 2 );
        
        $date = date_parse( $month );
        
        return $year . "-" . $date["month"] . "-" . $day;
    }

    public static function PhpbbToDate( $string )
    {
        // 2018-12-12T17:52:50
        $year = substr( $string, 0, 4 );
        $month = substr( $string, 5, 2 );
        $day = substr( $string, 8, 2 );

        return $year . "-" . $month . "-" . $day;
    }

    public static function YMDToDate( $string )
    {
        $year = substr( $string, 0, 4 );
        $month = substr( $string, 5, 2 );
        $day = substr( $string, 9, 2 );
        return $string;
    }

    public static function GetVideos()
    {
        $data = self::ReadJson( "../content/data/videos.json" );
        return $data;
    }

    public static function GetVideo( $videoId, $language )
    {
        $data = self::GetVideos();

        foreach ( $data[ $language ][ "videos" ] as $key=>$video )
        {
            if ( $video["YouTube ID"] == $videoId )
            {
                return $video;
            }
        }
        
        return null;
    }

    public static function GetVideosBase()
    {
        $data = self::ReadJson( "content/data/videos.json" );
        return $data;
    }

    public static function GetGamesBase()
    {
        $data = self::ReadJson( "content/data/games.json" );
        return $data;
    }

    public static function GetComics()
    {
        $data = self::ReadJson( "../content/data/comics.json" );
        return $data;
    }

    public static function GetNews()
    {
        $data = self::ReadJson( "http://www.ayadanconlangs.com/content/data/site-news.json" );
        return $data;
    }

    public static function GetBlogPostsCategory( $category )
    {
        // http://ayadanconlangs.com/blog/category/laadan/feed/
        // http://ayadanconlangs.com/blog/feed/
        $feed = self::GetRss( "http://ayadanconlangs.com/blog/category/" . $category . "/feed/" );
        return $feed;
    }

    public static function GetBlogPostsAuthor( $author )
    {
        // http://ayadanconlangs.com/blog/author/beka/feed/
        $feed = self::GetRss( "http://ayadanconlangs.com/blog/author/" . $author . "/feed/" );
        return $feed;
    }

    public static function GetBlogPosts()
    {
        $feed = self::GetRss( "http://ayadanconlangs.com/blog/feed/" );
        return $feed;
    }

    public static function GetForum()
    {
        $feed = self::GetRss( "http://www.ayadanconlangs.com/forums/app.php/feed?sid=dcd825950af15fc3b54d4fd1d902e4b4" );
        return $feed;
    }
};
?>
