                <div class="aya-footer container">
                    <div class="row">
                        <div class="col-md-4">
                            <p><strong>Áya Dan</strong><br>Conlang Blog &amp; Website</p>
                            <p>Build and maintained by Rachel Wil Sha Singh</p>
                            <p>Rachel [at] Moosader.com</p>
                            <p>Want to contribute? We could always use more help. :)</p>
                        </div>
                        <div class="col-md-4">&nbsp;</div>
                        <div class="col-md-4">
                            <p>Áya Dan Elseware:</p>
                            <ul class="aya-social-media cf">
                                <li><a href="https://www.facebook.com/ayadanconlangs/"><img src="<?=$path?>content/images/webpage/icon-facebook.png"></a></li>
                                <li><a href="https://www.youtube.com/ayadanconlangs/"><img src="<?=$path?>content/images/webpage/icon-youtube.png"></a></li>
                                <li><a href="https://bitbucket.org/ayadan/"><img src="<?=$path?>content/images/webpage/icon-bitbucket.png"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
    
            </div>
            <div class="aya-left-third">
                <div class="aya-centered">                    
                    <a href="http://www.ayadanconlangs.com/"><img src="<?=$path?>content/images/webpage/logo-small-text.png" title="The Áya Dan giraffe logo!" class="aya-logo"></a>
            
                    <h1 class="aya-title">Áya Dan</h1>
                    <p class="aya-motto"><em>Language is Beautiful</em></p>
                    <p class="aya-link"><a href="http://www.ayadanconlangs.com/">AyaDanConlangs.com</a></p>
                </div>
                
                <nav>
                    <h3>Media</h3>
                    <ul>
                        <li><a href="<?=$base?>blog/">      <img src="<?=$path?>content/images/webpage/icon-blog.png"> Blog</a></li>
                        <li><a href="<?=$base?>videos/">    <img src="<?=$path?>content/images/webpage/icon-video.png"> Videos</a></li>
                        <li><a href="<?=$base?>arcade/">    <img src="<?=$path?>content/images/webpage/icon-arcade.png"> Arcade</a></li>
                        <li><a href="<?=$base?>comics/">    <img src="<?=$path?>content/images/webpage/icon-comic.png"> Comics</a></li>
                        <li><a href="<?=$base?>community/"> <img src="<?=$path?>content/images/webpage/icon-community.png"> Community</a></li>
                        <li><a href="<?=$base?>tools/">     <img src="<?=$path?>content/images/webpage/icon-tools.png"> Tools</a></li>
                        <li><a href="https://shop.spreadshirt.com/conlangers/"> <img src="<?=$path?>content/images/webpage/icon-shirts.png"> Apparel</a></li>
    <!--
                        
                        <li><a href="<?=$base?>resources/"> <img src="<?=$path?>content/images/webpage/icon-resources.png"> Resources</a></li>
    -->
                    </ul>
                    <p>Coming soon:</p>
                    <ul>
                        <li><img src="<?=$path?>content/images/webpage/icon-resources.png"> Resources</li>
                    </ul>

                    <h3>Language</h3>
                    <ul>
                        <li><a href="<?=$base?>laadan/">    <img src="<?=$path?>content/images/webpage/icon-laadan.png"> Láadan</a></li>
                    </ul>
                    
                    <p>Coming soon:</p>
                    <ul>
    <!--
                        <li><a href="<?=$base?>esperanto/"> <img src="<?=$path?>content/images/webpage/icon-esperanto.png"> Esperanto</a></li>
                        <li><a href="<?=$base?>ido/">       <img src="<?=$path?>content/images/webpage/icon-ido.png"> Ido</a></li>
                        
    -->
                        <li><img src="<?=$path?>content/images/webpage/icon-esperanto.png"> Esperanto</li>
                        <li><img src="<?=$path?>content/images/webpage/icon-ido.png"> Ido</li>
                    </ul>

                    <h3>Community</h3>
                    <ul>
                        <li><a href="<?=$base?>community#aya-dan">  <img src="<?=$path?>content/images/webpage/icon-discord.png"> Discord</a></li>
                        <li><a href="<?=$base?>community#links"> <img src="<?=$path?>content/images/webpage/icon-telegram.png"> Telegram</a></li>
                        <li><a href="<?=$base?>forums/">            <img src="<?=$path?>content/images/webpage/icon-forums.png"> Forums</a></li>
                    </ul>
                </nav>
                
            </div>
            <div class="aya-right-third">
                <!-- to add -->
                <? if ( isset( $sidebar ) ) {
                    include_once( $path . "content/sidebars/" . $sidebar . ".php" );
                } ?>
            </div>
        </div>

    </body>
</html>
