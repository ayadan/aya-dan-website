<!DOCTYPE html> <? $path = "../../"; ?>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="<?=$path?>content/images/webpage/favicon.png">

        <title>Dashboard Template for Bootstrap</title>

        <!-- JS and CSS -->
        <link href="<?=$path?>content/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <script src="<?=$path?>content/bootstrap/js/bootstrap.min.js"></script>

        <script src="<?=$path?>content/jquery/jquery-3.3.1.min.js"></script>
        <script src="<?=$path?>content/js/expander.js"></script>

        <link href="<?=$path?>content/css/style.css" rel="stylesheet">
        <link href="<?=$path?>content/css/dashboard.css" rel="stylesheet">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Baloo+Bhaijaan|KoHo|Mali|PT+Sans+Narrow|Varela+Round" rel="stylesheet">

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-22400179-13"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-22400179-13');
        </script>
    </head>

    <body class="aya-body">

        <div class="aya-mobile-nav cf">
            <a href="<?=$base?>blog/">      <img src="<?=$path?>content/images/webpage/icon-blog.png" title="Blog"></a>
            <a href="<?=$base?>videos/">    <img src="<?=$path?>content/images/webpage/icon-video.png" title="Videos"></a>
            <a href="<?=$base?>arcade/">    <img src="<?=$path?>content/images/webpage/icon-arcade.png" title="Arcade"></a>
            <a href="<?=$base?>comics/">    <img src="<?=$path?>content/images/webpage/icon-comic.png" title="Comics"></a>
            <a href="<?=$base?>community/"> <img src="<?=$path?>content/images/webpage/icon-community.png" title="Community"></a>
            <a href="<?=$base?>tools/">     <img src="<?=$path?>content/images/webpage/icon-tools.png" title="Tools"></a>
            <a href="https://shop.spreadshirt.com/conlangers/"> <img src="<?=$path?>content/images/webpage/icon-shirts.png" title="T-Shirts"></a><br>
            <a href="<?=$base?>esperanto/"> <img src="<?=$path?>content/images/webpage/icon-esperanto.png" title="Esperanto"></a>
            <a href="<?=$base?>ido/">       <img src="<?=$path?>content/images/webpage/icon-ido.png" title="Ido"></a>
            <a href="<?=$base?>laadan/">    <img src="<?=$path?>content/images/webpage/icon-laadan.png" title="Láadan"></a>
            <a href="<?=$base?>community#aya-dan">  <img src="<?=$path?>content/images/webpage/icon-discord.png" title="Discord"></a>
            <a href="<?=$base?>community#links"> <img src="<?=$path?>content/images/webpage/icon-telegram.png" title="Telegram"></a>
            <a href="<?=$base?>forums/">            <img src="<?=$path?>content/images/webpage/icon-forums.png" title="Forums"></a>
        </div>

        <div class="aya-thirds-container cf">
            <div class="aya-main-view">
                asdf

                <div class="aya-footer">
                </div>
            </div>
            <div class="aya-left-third">
                <div class="aya-centered">                    
                    <a href="http://www.ayadanconlangs.com/"><img src="<?=$path?>content/images/webpage/logo-small-text.png" title="The Áya Dan giraffe logo!" class="aya-logo"></a>
            
                    <h1 class="aya-title">Áya Dan</h1>
                    <p class="aya-motto"><em>Language is Beautiful</em></p>
                    <p class="aya-link"><a href="http://www.ayadanconlangs.com/">AyaDanConlangs.com</a></p>
                </div>
                
                <nav>
                    <h3>Media</h3>
                    <ul>
                        <li><a href="<?=$base?>blog/">      <img src="<?=$path?>content/images/webpage/icon-blog.png"> Blog</a></li>
                        <li><a href="<?=$base?>videos/">    <img src="<?=$path?>content/images/webpage/icon-video.png"> Videos</a></li>
                        <li><a href="<?=$base?>arcade/">    <img src="<?=$path?>content/images/webpage/icon-arcade.png"> Arcade</a></li>
                        <li><a href="<?=$base?>comics/">    <img src="<?=$path?>content/images/webpage/icon-comic.png"> Comics</a></li>
                        <li><a href="<?=$base?>community/"> <img src="<?=$path?>content/images/webpage/icon-community.png"> Community</a></li>
                        <li><a href="<?=$base?>tools/">     <img src="<?=$path?>content/images/webpage/icon-tools.png"> Tools</a></li>
                        <li><a href="https://shop.spreadshirt.com/conlangers/"> <img src="<?=$path?>content/images/webpage/icon-shirts.png"> Apparel</a></li>
    <!--
                        
                        <li><a href="<?=$base?>resources/"> <img src="<?=$path?>content/images/webpage/icon-resources.png"> Resources</a></li>
    -->
                    </ul>
                    <p>Coming soon:</p>
                    <ul>
                        <li><img src="<?=$path?>content/images/webpage/icon-resources.png"> Resources</li>
                    </ul>

                    <h3>Language</h3>
                    <p>Coming soon:</p>
                    <ul>
    <!--
                        <li><a href="<?=$base?>esperanto/"> <img src="<?=$path?>content/images/webpage/icon-esperanto.png"> Esperanto</a></li>
                        <li><a href="<?=$base?>ido/">       <img src="<?=$path?>content/images/webpage/icon-ido.png"> Ido</a></li>
                        <li><a href="<?=$base?>laadan/">    <img src="<?=$path?>content/images/webpage/icon-laadan.png"> Láadan</a></li>
    -->
                        <li><img src="<?=$path?>content/images/webpage/icon-esperanto.png"> Esperanto</li>
                        <li><img src="<?=$path?>content/images/webpage/icon-ido.png"> Ido</li>
                        <li><img src="<?=$path?>content/images/webpage/icon-laadan.png"> Láadan</li>
                    </ul>

                    <h3>Community</h3>
                    <ul>
                        <li><a href="<?=$base?>community#aya-dan">  <img src="<?=$path?>content/images/webpage/icon-discord.png"> Discord</a></li>
                        <li><a href="<?=$base?>community#links"> <img src="<?=$path?>content/images/webpage/icon-telegram.png"> Telegram</a></li>
                        <li><a href="<?=$base?>forums/">            <img src="<?=$path?>content/images/webpage/icon-forums.png"> Forums</a></li>
                    </ul>
                </nav>
                
            </div>
            <div class="aya-right-third">
                <!-- to add -->
            </div>
        </div>

    </body>
</html>
