<!DOCTYPE html> <? $base = "http://www.ayadanconlangs.com/"; ?>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="<?=$path?>content/images/webpage/favicon.png">

        <title>Áya Dan Conlang Blog - Language is Beautiful</title>

        <!-- JS and CSS -->
        <link href="<?=$path?>content/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <script src="<?=$path?>content/bootstrap/js/bootstrap.min.js"></script>

        <script src="<?=$path?>content/jquery/jquery-3.3.1.min.js"></script>
        <script src="<?=$path?>content/js/expander.js"></script>

        <link href="<?=$path?>content/css/style.css" rel="stylesheet">
        <link href="<?=$path?>content/css/dashboard.css" rel="stylesheet">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Baloo+Bhaijaan|KoHo|Mali|PT+Sans+Narrow|Varela+Round" rel="stylesheet">

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-22400179-13"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-22400179-13');
        </script>
    </head>

    <body class="aya-body">

        <div class="aya-mobile-nav cf">
            <a href="<?=$base?>blog/">      <img src="<?=$path?>content/images/webpage/icon-blog.png" title="Blog"></a>
            <a href="<?=$base?>videos/">    <img src="<?=$path?>content/images/webpage/icon-video.png" title="Videos"></a>
            <a href="<?=$base?>arcade/">    <img src="<?=$path?>content/images/webpage/icon-arcade.png" title="Arcade"></a>
            <a href="<?=$base?>comics/">    <img src="<?=$path?>content/images/webpage/icon-comic.png" title="Comics"></a>
            <a href="<?=$base?>community/"> <img src="<?=$path?>content/images/webpage/icon-community.png" title="Community"></a>
            <a href="<?=$base?>tools/">     <img src="<?=$path?>content/images/webpage/icon-tools.png" title="Tools"></a>
            <a href="https://shop.spreadshirt.com/conlangers/"> <img src="<?=$path?>content/images/webpage/icon-shirts.png" title="T-Shirts"></a><br>
            <a href="<?=$base?>esperanto/"> <img src="<?=$path?>content/images/webpage/icon-esperanto.png" title="Esperanto"></a>
            <a href="<?=$base?>ido/">       <img src="<?=$path?>content/images/webpage/icon-ido.png" title="Ido"></a>
            <a href="<?=$base?>laadan/">    <img src="<?=$path?>content/images/webpage/icon-laadan.png" title="Láadan"></a>
            <a href="<?=$base?>community#aya-dan">  <img src="<?=$path?>content/images/webpage/icon-discord.png" title="Discord"></a>
            <a href="<?=$base?>community#links"> <img src="<?=$path?>content/images/webpage/icon-telegram.png" title="Telegram"></a>
            <a href="<?=$base?>forums/">            <img src="<?=$path?>content/images/webpage/icon-forums.png" title="Forums"></a>
        </div>

        <div class="aya-thirds-container cf">
            <div class="aya-main-view">
