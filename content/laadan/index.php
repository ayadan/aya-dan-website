<?
$title = "Láadan";
$sidebar = "laadan-sidebar";
?>
<? include_once( "../content/layout/sub-header.php" ); ?>
<? include_once( "../content/php/functions.php" ); ?>

<? 
$pageLanguage = "laadan";
$videos = Storage::GetVideos();
$blogs = Storage::GetBlogPostsCategory( "laadan" );
?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Láadan</h1>
            <p>The Perception Language</p>

            <hr>

            <section class="preview blog-preview">
                <h2>Blog</h2>
                
                <div class="blog-feature grid-view row">
                    <?  $i = 0;
                        foreach( $blogs->channel->item as $key=>$post ) { ?>
                        <div class="blog-item grid-item col-md-3">
                            <p class="title"><a href="<?=$post->link?>"><?=$post->title?></a></p>
                            <p class="date"><?= Storage::WordpressToDate( $post->pubDate ) ?></p>
                        </div>
                    <?
                    $i++;
                    if ( $i % 4 == 0 && $i != 0 ) { break; }
                    } ?>
                </div>

                <p class="view-more"><a href="http://ayadanconlangs.com/blog/category/laadan/">View all <?=sizeof( $blogs->channel->item ) ?> posts &gt;&gt;</a></p>
            </section>
            
            <hr>

            <section class="preview videos-preview">
                <h2>Videos</h2>

                <? foreach ( $videos as $key=>$language ) {
                    if ( $pageLanguage != "" && $key != $pageLanguage ) { continue; }
                    ?>
                <div class="language-videos row">
                <? $i = 0; ?>
                <? foreach ( $language["videos"] as $video ) { ?>
                    <div class="video-item col-md-3">
                            <a href="https://www.youtube.com/watch?v=<?=$video['YouTube ID']?>"><img src="https://img.youtube.com/vi/<?=$video['YouTube ID']?>/default.jpg"></a>
                            <p class="title"><a href="https://www.youtube.com/watch?v=<?=$video['YouTube ID']?>"><?=$video['name']?></a></p>
                            <p class="author">By <?=$video['creator']?></p>
                            <p class="date"><?=$video['date']?></p>
                        </div>
                    <?
                    $i++;
                    if ( $i != 0 && $i % 4 == 0 ) {
                        ?>
                        </div>
                        <div class="row view-all-link">
                            <div class="col-md-12">
                                <a href="http://www.ayadanconlangs.com/videos/?language=<?=$key?>">View all <?=sizeof( $language["videos"] ) ?> <?=$language["name"]?> videos &gt;&gt;</a>
                            </div>
                        
                        <?
                        break;
                    }
                    ?>
                    
                <? } ?>
                </div>
            <? } ?>
            </section>

            <hr>

            <section class="preview community-preview">
                <h2>Community</h2>

                <p class="view-more"><a href="">View all &raquo;</a></p>
            </section>

            <hr>

            <section class="preview resources-preview">
                <h2>Resources</h2>
            </section>

            <hr>

            <section class="preview tools-preview">
                <h2>Tools</h2>

                <div class="link-feature">
                    <h3>Láadan Quick Search Dictionary</h3>
                    <p>ABOUT</p>
                    <p><a href="">Go to &raquo;</a></p>
                </div>

                <div class="link-feature">
                    <h3>Láadan Letter Accenter</h3>
                    <p>ABOUT</p>
                    <p><a href="">Go to &raquo;</a></p>
                </div>
            </section>

            <hr>

            <section class="preview links-preview">
                <h2>Links</h2>
                <p>Links to other Láadan-related items around the internet.</p>

                <table class="table">
                    <thead>
                        <tr>
                            <th>Name</th><th>Type</th><th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><a href="">
                                Official Láadan Website
                            </a></td>
                            <td>Website</td>
                            <td>DESCRIPTION</td>
                        </tr>
                        
                        <tr>
                            <td><a href="">
                                Conlangery Podcast 06
                            </a></td>
                            <td>Podcast</td>
                            <td>A podcast episode that mentions Láadan, but isn't for it.</td>
                        </tr>
                        <tr><td><a href="">
                            Language Creation Society - Interview with Suzette
                            </a></td>
                            <td>Interview</td>
                            <td>DESCRIPTION</td>
                        </tr>
                        <tr>
                            <td><a href="">
                                Thámezhin i Ruth
                            </a></td>
                            <td>Blog</td>
                            <td>DESCRIPTION</td>
                        </tr>
                        <tr>
                            <td><a href="">
                                Láadaná
                            </a></td>
                            <td>Blog</td>
                            <td>DESCRIPTION</td>
                        </tr>
                        <tr>
                            <td><a href="">
                                Reshal i Láadan
                            </a></td>
                            <td>Blog</td>
                            <td>DESCRIPTION</td>
                        </tr>
                        <tr>
                            <td><a href="http://ozarque.livejournal.com/">
                                Suzette Haden Elgin’s blog
                            </a></td>
                            <td>Blog</td>
                            <td>DESCRIPTION</td>
                        </tr>
                        <tr>
                            <td><a href="https://www.facebook.com/groups/1510434509169558/">
                                Láadan Facebook Group
                            </a></td>
                            <td>Social Media</td>
                            <td>DESCRIPTION</td>
                        </tr>
                        <tr>
                            <td><a href="https://www.facebook.com/groups/1491100827835701/">
                                Esperantistaj Láadan-lernantoj
                            </a></td>
                            <td>Social Media</td>
                            <td>DESCRIPTION</td>
                        </tr>
                        <tr>
                            <td><a href="http://www.reddit.com/r/Laadan">
                                /r/Laadan
                            </a></td>
                            <td>Social Media</td>
                            <td>DESCRIPTION</td>
                        </tr>
                        <tr>
                            <td><a href="http://laadan.livejournal.com/">
                                LiveJournal community
                            </a></td>
                            <td>Social Media</td>
                            <td>DESCRIPTION</td>
                        </tr>
                        <tr>
                            <td><a href="https://twitter.com/hashtag/L%C3%A1adan?src=hash">
                                Tweets tagged #Laadan
                            </a></td>
                            <td>Social Media</td>
                            <td>DESCRIPTION</td>
                        </tr>
                        <tr>
                            <td><a href="https://www.tumblr.com/search/laadan">
                                Tumblr posts mentioning Láadan
                            </a></td>
                                <td>Social Media</td>
                                <td>DESCRIPTION</td>
                        </tr>
                        <tr>
                            <td><a href="">
                                Láadan Wikibook
                            </a></td>
                            <td>Grammar Lessons</td>
                            <td>DESCRIPTION</td>
                        </tr>
                        <tr>
                            <td><a href="">
                                Láadan Lessons for Beginners
                            </a></td>
                            <td>Grammar Lessons</td>
                            <td>DESCRIPTION</td>
                        </tr>
                        <tr>
                            <td><a href="">
                                Láadan Made Easier
                            </a></td>
                            <td>Grammar Lessons</td>
                            <td>DESCRIPTION</td>
                        </tr>
                        <tr>
                            <td><a href="">
                                Amberwind’s Láadan Lessons (Archived)
                            </a></td>
                            <td>Grammar Lessons</td>
                            <td>DESCRIPTION</td>
                        </tr>
                        <tr>
                            <td><a href="http://laadan.club/wil-sha/asdf">
                                Memrise – Láadan basic vocabulary
                            </a></td>
                            <td>Vocabulary Lessons</td>
                            <td>DESCRIPTION</td>
                        </tr>
                        <tr>
                            <td><a href="http://www.memrise.com/course/556115/laadan-lessons/">
                                Memrise – Amberwind’s vocabulary
                            </a></td>
                            <td>Vocabulary Lessons</td>
                            <td>DESCRIPTION</td>
                        </tr>
                        <tr>
                            <td><a href="http://www.memrise.com/course/332370/laadan-vocabulary/">
                                Memrise – Full dictionary
                            </a></td>
                            <td>Vocabulary Lessons</td>
                            <td>DESCRIPTION</td>
                        </tr>
                        <tr>
                            <td><a href="https://mw.lojban.org/extensions/ilmentufa/i/laadan/index.html#">
                                la sutysisku – fast dictionary
                            </a></td>
                            <td>Dictionary</td>
                            <td>DESCRIPTION</td>
                        </tr>
                        <tr>
                            <td><a href="">
                                English to Láadan
                            </a></td>
                            <td>Dictionary</td>
                            <td>DESCRIPTION</td>
                        </tr>
                        <tr>
                            <td><a href="">
                                Láadan to English
                            </a></td>
                            <td>Dictionary</td>
                            <td>DESCRIPTION</td>
                        </tr>
                        <tr>
                            <td><a href="">
                                Láadan to Spanish
                            </a></td>
                            <td>Dictionary</td>
                            <td>DESCRIPTION</td>
                        </tr>
                        <tr>
                            <td><a href="">
                                Láadan Core Words
                            </a></td>
                            <td>Dictionary</td>
                            <td>DESCRIPTION</td>
                        </tr>
                        <tr>
                            <td><a href="">
                                English to Láadan (.csv)
                            </a></td>
                            <td>Dictionary</td>
                            <td>Plaintext, computer-parsable dictionary that can be used for software creation.</td>
                        </tr>
                        <tr>
                            <td><a href="">
                                Láadan to English (.csv)
                            </a></td>
                            <td>Dictionary</td>
                            <td>Plaintext, computer-parsable dictionary that can be used for software creation.</td>
                        </tr>
                        <tr>
                            <td><a href="https://glosbe.com/ldn/en/">
                                Glosbe
                            </a></td>
                            <td>Dictionary</td>
                            <td>DESCRIPTION</td>
                        </tr>
                        <tr>
                            <td><a href="http://lingweenie.org/conlang/laadan.html">
                                Lingweenie – Láadan: Chapter Six Brief Reading
                            </a></td>
                            <td>Article</td>
                            <td>DESCRIPTION</td>
                        </tr>
                        <tr>
                            <td><a href="https://esirc.emporia.edu/handle/123456789/986">
                                Bíi ril thad óotha demedi be (writing a window to the soul) : Suzette Haden Elgin and the path to a constructed language.
                            </a></td>
                            <td>Article</td>
                            <td>DESCRIPTION</td>
                        </tr>
                        <tr>
                            <td><a href="">Láadan Letter Accenter</a></td>
                            <td>Tool</td>
                            <td>DESCRIPTION</td>
                        </tr>
                        <tr>
                            <td><a href="">
                                Láadan Quick Search Dictionary
                            </a></td>
                            <td>Dictionary</td>
                            <td>DESCRIPTION</td>
                        </tr>
                        <tr>
                            <td><a href="https://play.google.com/store/apps/details?id=laadanhelper.moosader.com.ladanbedinahabe">
                                Láadan Helper – Android App
                            </a></td>
                            <td>Dictionary,Grammar Lessons</td>
                            <td>DESCRIPTION</td>
                        </tr>
                    </tbody>
                </table>
            </section>

            <hr>

            <section class="preview archive-preview">
                <h2>Archive</h2>

                <p>Webpages and resources for Láadan have been disappearing over the years,
                so recovered items are stored on the Áya Dan server and hosted here.</p>

                <table class="table">
                    <thead>
                        <tr>
                            <th>Name</th><th>Type</th><th>Description</th><th>Last available</th><th>Archive</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr> <td>Amberwind's Láadan Lessons</td><td>Website</td><td>Very thorough lessons on Láadan grammar and vocabulary.</td><td>2015</td><td><a href="http://www.ayadanconlangs.com/archive/amberwind">Link</a></td> </tr>
                        <tr> <td>Jackie Powers' Láadan Language Reference Page</td><td>Website</td><td>An affiliate website that hosted some information on Láadan.</td><td>2008</td><td><a href="http://www.ayadanconlangs.com/archive/jackie-powers">Link</a></td> </tr>
                        <tr> <td>Original Láadan Language website</td><td>Website</td><td>The original Láadan website</td><td>2012</td><td><a href="http://www.ayadanconlangs.com/archive/laadan-language">Link</a></td> </tr>
                        <tr> <td>A First Dictionary and Grammar of Láadan</td><td>Book</td><td>The out-of-print book in PDF form (no dictionary)</td><td>1988</td><td><a href="http://www.ayadanconlangs.com/archive/downloads/first-dictionary.pdf">Link</a></td> </tr>
                        <tr> <td>Why a Woman is Not Like a Physicist: WisCon 6, March 6 1982</td><td>Article</td><td>A talk by Suzette Haden Elgin, preceeding her creation of Láadan.</td><td>1982</td><td><a href="http://www.ayadanconlangs.com/archive/downloads/wiscon-talk.pdf">Link</a></td> </tr>
                        
                    </tbody>
                </table>
            </section>

            <hr>

            <section class="preview about-preview">
                <h2>About Láadan</h2>

                

                <h3>What is Láadan?</h3>
                <img style="float: right; margin: 10px;" src="../content/images/laadan/suzette-hotwire.jpg" title="An image of Suzette Haden Elgin signing a copy of her book, from the November 1985 Hot Wire magazine.">
                
                <p>Láadan is a language that was constructed in 1982 by Suzette Haden Elgin, a science fiction author, self-help author, feminist, and linguist. She built Láadan to explore the idea of an “undefinable other reality” that we have no vocabulary for. As a science fiction author, she knew of stories that were “Matriarchies” and “Androgynous” presented as an alternative to our traditionally Patriarchal society, but she wondered about some third alternative, where it wasn’t a fight between men vs. women for power, but something else completely.</p>

                <p>This as well as her own personal experiences with using language for human communication, her perceived difficulties that women experience trying to express themselves with certain limitations (how to more easily describe concepts that are common among women but have no terms, how men react to the language women use and write off concerns, etc.), this caused her to think about a language by and for women.</p>

                <p>Láadan went along hand-in-hand with her Native Tongue series of books. In the book series, a group of women who are sent to the “Barren House” once no longer useful to their society end up creating Láadan in secret. In reality, Suzette created Láadan as an experiment, curious whether women would become interested in it, or at least inspire women to build a better language.</p>

                <p>Klingon and Láadan were both created in the 1980s. After ten years passed, Láadan did not gain much popularity, and Suzette saw the Klingon language being more widely adopted as… well, it was something. She declared the Láadan experiment a failure.</p>
                
                <h3>Rachel's Thoughts</h3>

                <p>I think that the concept of Láadan is easier to portray to those who have grown up queer and sheltered like me. For people from my generation and before, if you’re not quite the same as others – whether sexuality-wise or gender-wise – it feels alienating. And, without that community, you aren’t aware of the words that exist to describe your experience (Dysphoria, Asexuality, etc.) In this case, we can see that there was clearly a lack of vocabulary to describe our experiences, but it has since been created and a community built up.</p>

                <p>Likewise, some words in Láadan help describe certain experiences that all people have, though its emphasis as a woman’s language is in how it tries to be more soft, perceptive, and in-tune with feelings. Of course, people of all genders can use and appreciate this language if they are interested; Suzette never meant for it to be “just for women” even if she created it for women. Similarly, so many movies are made with male leads but meant for everybody, while we still assume movies with a female lead are often movies “for women”. Something made for women can still be for everybody.</p>

                <p>In a comparison between Klingon vs. Láadan, I think you would have to compare how they were presented and backed – Klingon had a television show it belonged to, and with the TV show came merchandise to popularize Klingon characters and keep it alive long-term. Suzette had a series of three novels and purposely did not try to market it in such a way.</p>

                <p>On the downsides of Láadan, I have to mention the lack of queer vocabulary. This can be remedied, but it is not something that I am comfortable doing myself and I would like to have more people join my efforts in working with Láadan, so that we may expand the dictionary to cover more perceptions and experiences.</p>
            </section>
        </div>
    </div>
</div>

<? include_once( "../content/layout/sub-footer.php" ); ?>
