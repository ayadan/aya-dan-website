$( document ).ready( function() {
    $( ".x-icon" ).click( function() {
        $( this ).closest( ".header" ).siblings( ".contents" ).slideUp( "fast" );
        $( this ).siblings( ".bar-icon" ).css( "display", "block" );
        $( this ).css( "display", "none" );
    } );
    $( ".bar-icon" ).click( function() {
        $( this ).closest( ".header" ).siblings( ".contents" ).slideDown( "fast" );
        $( this ).siblings( ".x-icon" ).css( "display", "block" );
        $( this ).css( "display", "none" );
    } );
} );
