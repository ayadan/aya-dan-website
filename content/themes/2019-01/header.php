<!DOCTYPE html>
<html lang="en">

<head>
    <? 
    $path = "../../../";
    $base = "http://www.ayadanconlangs.com/";
    ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="Rachel Wil Sha Singh">
    <link rel="icon" href="<?=$path?>content/images/webpage/favicon.png">

    <title> <?=$title?> - Áya Dan ~ Language is Beautiful ~ Conlang Blog, Resources, and Entertainment </title>
    
    <script src="<?=$path?>content/jquery/jquery-3.3.1.min.js"></script>
    <script src="<?=$path?>content/themes/2019-01/window-manager.js"></script>
    <link href="<?=$path?>content/themes/2019-01/style.css" rel="stylesheet">
    
    <?
    include_once( "<?=$path?>content/php/functions.php" );
    $news = Storage::GetNews();
    ?>
</head>

<body>
    <div class="container cf">
        <div class="static-nav">
            <nav>
                <div class="window">
                    <div class="header">Welcome to... <div class="x-icon"><img src="<?=$base?>content/images/webpage/x-icon.png"></div> <div class="bar-icon"><img src="<?=$base?>content/images/webpage/bar-icon.png"></div></div>

                    <div class="contents">                   
                        <header class="cf">
                            <div class="logo">
                                <a href="http://www.ayadanconlangs.com/"><img src="<?=$base?>content/images/webpage/logo-small-text.png" title="The Áya Dan giraffe logo!" class="logo"></a>
                            </div>
                            <div class="text">
                                <span class="site-title">Áya Dan</span><br>
                                <span class="site-motto">Language is Beautiful</span><br>
                                <span class="site-link"><a href="http://www.ayadanconlangs.com/">AyaDanConlangs.com</a></span>
                            </div>
                        </header>
                    </div>
                </div>
                
                <div class="window">
                    <div class="header">Media <div class="x-icon"><img src="<?=$base?>content/images/webpage/x-icon.png"></div> <div class="bar-icon"><img src="<?=$base?>content/images/webpage/bar-icon.png"></div></div>
                    <div class="contents">
                        <ul class="icons cf">
                            <li><a href="<?=$base?>blog/"                           class="tooltip" title="Blog">       <img src="<?=$base?>content/images/webpage/icon-blog.png">        <span class="tooltiptext">Blog</span></a></li>
                            <li><a href="<?=$base?>videos/"                         class="tooltip" title="Videos">     <img src="<?=$base?>content/images/webpage/icon-video.png">       <span class="tooltiptext">Video</span></a></li>
                            <li><a href="<?=$base?>arcade/"                         class="tooltip" title="Arcade">     <img src="<?=$base?>content/images/webpage/icon-arcade.png">      <span class="tooltiptext">Arcade</span></a></li>
                            <li><a href="<?=$base?>comics/"                         class="tooltip" title="Comics">     <img src="<?=$base?>content/images/webpage/icon-comic.png">       <span class="tooltiptext">Comic</span></a></li>
                            <li><a href="<?=$base?>community/"                      class="tooltip" title="Community">  <img src="<?=$base?>content/images/webpage/icon-community.png">   <span class="tooltiptext">Community</span></a></li>
                            <li><a href="<?=$base?>tools/"                          class="tooltip" title="Tools">      <img src="<?=$base?>content/images/webpage/icon-tools.png">       <span class="tooltiptext">Tools</span></a></li>                     
                            <li><a href="<?=$base?>resources/"                      class="tooltip" title="Resources">  <img src="<?=$base?>content/images/webpage/icon-resources.png">   <span class="tooltiptext">Resources</span></a></li>
                            <li><a href="https://shop.spreadshirt.com/conlangers/"  class="tooltip" title="Apparel">    <img src="<?=$base?>content/images/webpage/icon-shirts.png" title="Apparel"><span class="tooltiptext">Apparel</span></a></li>
                        </ul>
                    </div>
                </div>
                

                <div class="window">
                    <div class="header">Languages <div class="x-icon"><img src="<?=$base?>content/images/webpage/x-icon.png"></div> <div class="bar-icon"><img src="<?=$base?>content/images/webpage/bar-icon.png"></div></div>
                    <div class="contents">
                        <ul class="icons cf">
                            <li><a href="<?=$base?>laadan/" class="tooltip" title="Láadan"> <img src="<?=$base?>content/images/webpage/icon-laadan.png">      <span class="tooltiptext">Láadan</span></a></li>
                            <li><a href="#" class="tooltip" title="Esperanto">              <img src="<?=$base?>content/images/webpage/icon-esperanto.png">   <span class="tooltiptext">Esperanto (Coming Soon)</span></a></li>
                            <li><a href="#" class="tooltip" title="Ido">                    <img src="<?=$base?>content/images/webpage/icon-ido.png">         <span class="tooltiptext">Ido (Coming Soon)</span></a></li>
                        </ul>
                    </div>
                </div>
                
                <div class="window">
                    <div class="header">Community <div class="x-icon"><img src="<?=$base?>content/images/webpage/x-icon.png"></div> <div class="bar-icon"><img src="<?=$base?>content/images/webpage/bar-icon.png"></div></div>
                    <div class="contents">
                        <ul class="icons cf">
                            <li><a href="<?=$base?>community#aya-dan"   class="tooltip" title="Discord">    <img src="<?=$base?>content/images/webpage/icon-discord.png">     <span class="tooltiptext">Discord</span></a></li>
                            <li><a href="<?=$base?>community#links"     class="tooltip" title="Telegram">   <img src="<?=$base?>content/images/webpage/icon-telegram.png">    <span class="tooltiptext">Telegram</span></a></li>
                            <li><a href="<?=$base?>forums/"             class="tooltip" title="Forums">     <img src="<?=$base?>content/images/webpage/icon-forums.png">      <span class="tooltiptext">Forums</span></a></li>
                        </ul>
                    </div>
                </div>
                
                <div class="window news">
                    <div class="header">Site News <div class="x-icon"><img src="<?=$base?>content/images/webpage/x-icon.png"></div> <div class="bar-icon"><img src="<?=$base?>content/images/webpage/bar-icon.png"></div></div>
                    <div class="contents">
                        <p><?= $news["news"][0]["content"] ?></p>
                        <p class="date"><?= $news["news"][0]["author"] ?>,<br> <?= $news["news"][0]["date"] ?></p>
                    </div>
                </div>
                
                <div class="window">
                    <div class="header">Áya Dan <div class="x-icon"><img src="<?=$base?>content/images/webpage/x-icon.png"></div> <div class="bar-icon"><img src="<?=$base?>content/images/webpage/bar-icon.png"></div></div>
                    <div class="contents">
                        <footer>
                            <p>Conlang Blog and Website</p>
                            <hr>

                            <p>Built and maintained by <strong>Rachel Wil Sha Singh</strong> - Rachel[at]Moosader.com</p>

                            <p>Want to contribute? We could always use more help. :)</p>

                            <hr>

                            <p>
                                <strong>Did you know?</strong> This website is open source! Since conlang resources come and go,
                                it can be important to back up and archive information. I've made this site open source
                                in case anything ever happens to me.
                                <a href="https://bitbucket.org/ayadan/aya-dan-website/src">The repository is on BitBucket</a>
                            </p>

                            <hr>

                            <p><strong>Social Media:</strong></p>
                            <ul class="icons cf">
                                <li><a href="https://www.facebook.com/ayadanconlangs/"  class="tooltip" title="Facebook">   <img src="<?=$base?>content/images/webpage/icon-facebook.png">      <span class="tooltiptext">Facebook</span></a></li>
                                <li><a href="https://www.youtube.com/ayadanconlangs/"   class="tooltip" title="YouTube">    <img src="<?=$base?>content/images/webpage/icon-youtube.png">       <span class="tooltiptext">YouTube</span></a></li>
                                <li><a href="https://bitbucket.org/ayadan/"             class="tooltip" title="BitBucket">  <img src="<?=$base?>content/images/webpage/icon-bitbucket.png">     <span class="tooltiptext">BitBucket</span></a></li>
                            </ul>

                            <hr>

                            <p><strong>My other things...</strong></p>
                            <ul>
                                <li><a href="http://www.moosader.com/">Moosader Games</a></li>
                                <li><a href="http://rejcx.moosader.com/">Rachel's blog</a></li>
                            </ul>
                        </footer>
                    </div>
                </div>
            </nav>
        </div>

        <div class="resizable-main">
            <main class="window">
                <div class="header"><?=$title?> <div class="x-icon"><img src="content/images/webpage/x-icon.png"></div> <div class="bar-icon"><img src="content/images/webpage/bar-icon.png"></div></div>
                <div class="contents">
