                </div>

            </main>
        </div>

        <div class="resizable-main">
            <main class="window">
                <div class="header">Quick Tools <div class="x-icon"><img src="content/images/webpage/x-icon.png"></div> <div class="bar-icon"><img src="content/images/webpage/bar-icon.png"></div></div>

                <div class="contents">
                    <div class="row-container cf">
                        <!-- -- -->
                        <div class="col-33">
                            <div class="contents">
                                <? include_once( "content/sidebars/esperanto-dictionary.php" ); ?>
                            </div>
                        </div>

                        <!-- -- -->
                        <div class="col-33">
                            <div class="contents">
                                <? include_once( "content/sidebars/ido-dictionary.php" ); ?>
                            </div>
                        </div>

                        <!-- -- -->
                        <div class="col-33">
                            <div class="contents">
                                <? include_once( "content/sidebars/laadan-dictionary.php" ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </div>

    <footer>
    </footer>

</body>

</html>
