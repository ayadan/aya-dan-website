<script>
$( function() {
    $( "#result" ).css( "display", "none" );

    $( "#type-box" ).keyup( function() {
        text = $( "#type-box" ).val();
        formatted = text;
        formatted = replaceAll( "a'", '&aacute;', formatted );
        formatted = replaceAll( "e'", '&eacute;', formatted );
        formatted = replaceAll( "i'", '&iacute;', formatted );
        formatted = replaceAll( "o'", '&oacute;', formatted );
        formatted = replaceAll( "u'", '&uacute;', formatted );
        formatted = replaceAll( "A'", '&Aacute;', formatted );
        formatted = replaceAll( "E'", '&Eacute;', formatted );
        formatted = replaceAll( "I'", '&Iacute;', formatted );
        formatted = replaceAll( "O'", '&Oacute;', formatted );
        formatted = replaceAll( "U'", '&Uacute;', formatted );
        $( "#result" ).html( formatted );
        $( "#converted" ).val( $("#result").html() );
    } );

    function replaceAll(find, replace, str) {
      return str.replace(new RegExp(find, 'g'), replace);
    }

} );

</script>

<div class="accenter-laadan container">
    <h3>Láadan Letter Accenter</h3>
    <div class="row">
        <div class="col-md-12">
            <p>Put a ' after a vowel to auto-accent it: A', E', I', O', U'</p>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <textarea id="type-box" class="form-control" placeholder="Type here"></textarea>
        </div>
    </div>

    <div class="row aya-spacer">&nbsp;</div>
    
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <p id="result">&nbsp;</p>
            <textarea id="converted" class="form-control" placeholder="Formatted text appears here"></textarea>
        </div>
    </div>
</div>
