    <script>
        /* IDO */
        $( document ).ready( function() {
            $( "#search-button-IDO" ).click( function() {
                console.log( "Clicked search" );
                var search = $( "#search-term-IDO" ).val();
                var apiString = "http://www.ayadanconlangs.com/api/ido-dictionary.php?search=" + search + "&lookat=all";

                console.log( apiString );

                $.get( apiString, function( data ) {
                    console.log( "Got result..." );
                    console.log( data );
                    
                    $( "#search-results-IDO" ).empty();

                    jQuery.each( data["data"], function( key, value ) {
                        if ( value[0] != "credit" ) {
                            var entryString = "<div class='entry'>";
                                entryString += "<p class='ido conlang'>" + value["ido"] + "</p>";
                                entryString += "<p class='english'>" + value["english"] + "</p>";
                            entryString += "</div>";

                            $( "#search-results-IDO" ).append( entryString );
                            console.log( value );
                        }
                    } );
                    
                    $( "search-results-IDO" ).slideDown( "fast" );

                } );
            } );
        } );
    </script>

    <h3>Ido Dictionary</h3>

    <p><a href="http://ayadanconlangs.com/tools/ido-vortolibro/">Go to Ido dictionary &gt;&gt;</a></p>

    <div class="quick-searcher quick-searcher-ido">
        <div class="search-term">
            <input type="text" id="search-term-IDO" placeholder="English/Ido search term">
        </div>
        <div class="search-button">
            <input type="button" value="Search Ido" id="search-button-IDO">
        </div>
        <div class="search-results" id="search-results-IDO">
            <div class="entry">
                <p class="conlang"></p>
                <p class="english"></p>
                <p class="description"></p>
            </div>
        </div>
    </div>
