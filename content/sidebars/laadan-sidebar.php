<h2>Quick links</h2>

<ul>
    <li><a href="http://ayadanconlangs.com/tools/laadan-dictionary/">Láadan Dictionary</a></li>
    <li><a href="http://ayadanconlangs.com/tools/laadan-accenter/">Láadan Letter Accenter</a></li>
    <li><a href="https://en.wikibooks.org/wiki/L%C3%A1adan">Láadan Wikibook</a></li>
    <li><a href="http://www.ayadanconlangs.com/archive/downloads/amberwind-laadan-lessons-all.pdf">Abmerwind's Láadan Lessons (PDF)</a></li>
</ul>

<hr>

<? include_once( "laadan-dictionary.php" ); ?>

<hr>

<? include_once( "laadan-accenter.php" ); ?>

<hr>

<? include_once( "laadan-wiki-links.php" ); ?>
