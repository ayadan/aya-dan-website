
    <script>
        /* ESPERANTO */
        $( document ).ready( function() {
            $( "#search-button-EO" ).click( function() {
                console.log( "Clicked search" );
                var search = $( "#search-term-EO" ).val();
                var apiString = "http://www.ayadanconlangs.com/api/esperanto-dictionary.php?search=" + search + "&lookat=all";

                console.log( apiString );

                $.get( apiString, function( data ) {
                    console.log( "Got result..." );
                    console.log( data );
                    
                    $( "#search-results-EO" ).empty();

                    jQuery.each( data["data"], function( key, value ) {
                        if ( value[0] != "credit" ) {
                            var entryString = "<div class='entry'>";
                                entryString += "<p class='esperanto conlang'>" + value["esperanto"] + "</p>";
                                entryString += "<p class='english'>" + value["english"] + "</p>";
                            entryString += "</div>";

                            $( "#search-results-EO" ).append( entryString );
                            console.log( value );
                        }
                    } );
                    
                    $( "search-results-EO" ).slideDown( "fast" );

                } );
            } );
        } );
    </script>

    <h3>Esperanto Dictionary</h3>
    <p><a href="http://www.denisowski.org/Esperanto/ESPDIC/espdic_readme.html">Go to ESPDIC &gt;&gt;</a></p>

    <div class="quick-searcher quick-searcher-esperanto">
        <div class="search-term">
            <input type="text" id="search-term-EO" placeholder="English/Esperanto search term">
        </div>
        <div class="search-button">
            <input type="button" value="Search Esperanto" id="search-button-EO">
        </div>
        <div class="search-results" id="search-results-EO">
            <div class="entry">
                <p class="conlang"></p>
                <p class="english"></p>
                <p class="description"></p>
            </div>
        </div>
    </div>
