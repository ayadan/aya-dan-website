
<div class="container">
    <h3>Learning Láadan</h3>
    <div class="row">
        <ul>
            <li>Part 1: Basic Sentences
            <ul>
                <li><a href="https://en.wikibooks.org/wiki/L%C3%A1adan/Lessons/1">Sounds, tones, and euphony</a></li>
                <li><a href="https://en.wikibooks.org/wiki/L%C3%A1adan/Lessons/2">What is it? What is it doing?</a></li>
                <li><a href="https://en.wikibooks.org/wiki/L%C3%A1adan/Lessons/3">Speech Act, Evidence, and negation</a></li>
                <li><a href="https://en.wikibooks.org/wiki/L%C3%A1adan/Lessons/4">Time, adjectives, plurals</a></li>
                <li><a href="https://en.wikibooks.org/wiki/L%C3%A1adan/Lessons/5">Pronouns, objects, multiple verbs, to try to</a></li>
            </ul>
            </li>
            
            <li>Part 2: Relations
            <ul>
                <li><a href="https://en.wikibooks.org/wiki/L%C3%A1adan/Lessons/6">Goal, Source</a></li>
                <li><a href="https://en.wikibooks.org/wiki/L%C3%A1adan/Lessons/7">Association, Beneficiary</a></li>
                <li><a href="https://en.wikibooks.org/wiki/L%C3%A1adan/Lessons/8">Instrument, Location</a></li>
                <li><a href="https://en.wikibooks.org/wiki/L%C3%A1adan/Lessons/9">Manner, Reason, Purpose</a></li>
                <li><a href="https://en.wikibooks.org/wiki/L%C3%A1adan/Lessons/9b">Path</a></li>
            </ul>
            </li>
            
            <li>Part 3: Perception
            <ul>
                <li><a href="https://en.wikibooks.org/wiki/L%C3%A1adan/Lessons/10">Ways to perceive</a></li>
                <li><a href="https://en.wikibooks.org/wiki/L%C3%A1adan/Lessons/11">Possession</a></li>
                <li><a href="https://en.wikibooks.org/wiki/L%C3%A1adan/Lessons/12">Relationships</a></li>
                <li><a href="https://en.wikibooks.org/wiki/L%C3%A1adan/Lessons/13">Degree</a></li>
                <li><a href="https://en.wikibooks.org/wiki/L%C3%A1adan/Lessons/14">Duration and Repetition</a></li>
                <li><a href="https://en.wikibooks.org/wiki/L%C3%A1adan/Lessons/15">State of Consciousness</a></li>
                <li><a href="https://en.wikibooks.org/wiki/L%C3%A1adan/Lessons/16">Adding to the Speech Act Morpheme</a></li>
                <li><a href="https://en.wikibooks.org/wiki/L%C3%A1adan/Lessons/17">Noun declensions</a></li>
                <li><a href="https://en.wikibooks.org/wiki/L%C3%A1adan/Lessons/22">Comparisons</a></li>
            </ul>
            </li>
            
            <li>Part 4: Additional
            <ul>
                <li><a href="https://en.wikibooks.org/wiki/L%C3%A1adan/Lessons/18">Who, what, when, where, why?</a></li>
                <li><a href="https://en.wikibooks.org/wiki/L%C3%A1adan/Lessons/19">Embedding sentences</a></li>
                <li><a href="https://en.wikibooks.org/wiki/L%C3%A1adan/Lessons/20">Passive voice</a></li>
                <li><a href="https://en.wikibooks.org/wiki/L%C3%A1adan/Lessons/21">Numbers and quantity</a></li>
                <li><a href="https://en.wikibooks.org/wiki/L%C3%A1adan/Lessons/23">If... then...</a></li>
            </ul>
            </li>
        </ul>
    </div>
</div>
