<script>
$( function() {
    $( "#eo-result" ).css( "display", "none" );

    $( "#eo-type-box" ).keyup( function() {
        text = $( "#eo-type-box" ).val();
        formatted = text;
        formatted = replaceAll( "c'", '&#265;', formatted );
        formatted = replaceAll( "g'", '&#285;', formatted );
        formatted = replaceAll( "h'", '&#293;', formatted );
        formatted = replaceAll( "j'", '&#309;', formatted );
        formatted = replaceAll( "s'", '&scirc;', formatted );
        formatted = replaceAll( "u'", '&#365;', formatted );
        formatted = replaceAll( "C'", '&#264;', formatted );
        formatted = replaceAll( "G'", '&#284;', formatted );
        formatted = replaceAll( "H'", '&#292;', formatted );
        formatted = replaceAll( "J'", '&#308;', formatted );
        formatted = replaceAll( "S'", '&Scirc;', formatted );
        formatted = replaceAll( "U'", '&#364;', formatted );
        $( "#eo-result" ).html( formatted );
        $( "#eo-converted" ).val( $("#eo-result").html() );
    } );

    function replaceAll(find, replace, str) {
      return str.replace(new RegExp(find, 'g'), replace);
    }

} );

</script>

<div class="accenter-laadan container">
    <h3>Esperanto Letter Accenter</h3>
    <div class="row">
        <div class="col-md-12">
            <p>Put a ' after a vowel to auto-accent it: c', g', h', j', s', u'</p>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <textarea id="eo-type-box" class="form-control" placeholder="Type here"></textarea>
        </div>
    </div>

    <div class="row aya-spacer">&nbsp;</div>
    
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <p id="eo-result">&nbsp;</p>
            <textarea id="eo-converted" class="form-control" placeholder="Formatted text appears here"></textarea>
        </div>
    </div>
</div>
