
<!--<link href="../css/style.css" rel="stylesheet">-->
        
<div class="container">
    <h2>Widget bar</h2>
    <hr>
</div>

<? include_once( "laadan-dictionary.php" ); ?>

<hr>

<? include_once( "esperanto-dictionary.php" ); ?>

<hr>

<? include_once( "ido-dictionary.php" ); ?>

<hr>

<? include_once( "laadan-accenter.php" ); ?>

<hr>

<? include_once( "esperanto-accenter.php" ); ?>

<hr>

<div class="row aya-spacer">&nbsp;</div>
    
<div class="under-construction-message">
    <p><strong>Under Construction :P</strong></p>

    <p>Hey there I'm currently working on updating the website.
    I know it's not great to be doing this live, but I wanted
    to just get the new page up and start adding on additional pages. :P
    Thanks.</p>

    <p>Webpage last updated: December 19th, 2018</p>

    <p>Done:</p>
    
    <ul class="checklist">
        <li>Moved the Wordpress blog</li>
        <li>Videos page</li>
        <li>Move Tools page and tools</li>
        <li>Move Community page</li>
        <li>Move Arcade page</li>
        <li>Implement Comics page</li>
        <li>Add feed of blog/videos/comics/etc. to home page</li>
        <li>Styled the message board</li>
        <li>Created Láadan dictionary API </li>
        <li>Created Láadan translator </li>
        <li>Created Láadan reference sidebars </li>
    </ul>

    <p>To Do:</p>

    <ul>
        <li>Make the Comics page prettier</li>
        <li>Style blog to fit with website</li>
        <li>Move Resources page</li>
        <li>Implement Esperanto homepage</li>
        <li>Implement Ido homepage</li>
        <li>Fix up the forums, de-spam, style, etc.</li>
        <li>Fix the footer</li>
        <li>Restyle the tools pages to fit the website better</li>
        <li>Implement some sort of API for the dictionaries</li>
        <li>Style the video / blog feeds nicer.</li>
        <li>Finish the Laadan page</li>
        <li>Make Dictionary API for Ido</li>
        <li>Make quick-dictionary widgets for the front page</li>
    </ul>
</div>
