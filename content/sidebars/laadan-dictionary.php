    <script>
        /* LAADAN */
        $( document ).ready( function() {
            $( "#search-button-LAADAN" ).click( function() {
                console.log( "Clicked search" );
                var search = $( "#search-term-LAADAN" ).val();
                var apiString = "http://www.ayadanconlangs.com/api/laadan-dictionary.php?search=" + search + "&lookat=all";

                console.log( apiString );

                $.get( apiString, function( data ) {
                    console.log( "Got result..." );
                    console.log( data );
                    
                    $( "#search-results-LAADAN" ).empty();

                    jQuery.each( data["data"], function( key, value ) {
                        if ( value[0] != "credit" ) {
                            var entryString = "<div class='entry'>";
                                entryString += "<p class='laadan conlang'>" + value["láadan"] + "</p>";
                                entryString += "<p class='english'>" + value["english"] + "</p>";
                                entryString += "<p class='description'>" + value["description"] + "</p>";
                            entryString += "</div>";

                            $( "#search-results-LAADAN" ).append( entryString );
                        }
                    } );
                    
                    $( "search-results-LAADAN" ).slideDown( "fast" );

                } );
            } );
        } );
    </script>

    <h3>Láadan Dictionary</h3>

    <p><a href="http://ayadanconlangs.com/tools/laadan-dictionary/">Go to Láadan dictionary &gt;&gt;</a></p>

    <div class="quick-searcher quick-searcher-laadan">
        <div class="search-term">
            <input type="text" id="search-term-LAADAN" placeholder="English/Láadan search term">
        </div>
        <div class="search-button">
            <input type="button" value="Search Láadan" id="search-button-LAADAN">
        </div>
        <div class="search-results" id="search-results-LAADAN">
            <div class="entry">
                <p class="conlang"></p>
                <p class="english"></p>
                <p class="description"></p>
            </div>
        </div>
    </div>
