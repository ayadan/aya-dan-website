<h2>Grammar Reference</h2>

<script>
    $( document ).ready( function() {
        $( ".aya-header" ).click( function() {
            if ( $( this ).next( ".aya-hidden" ).css( "display" ) == "none" )
            {
                $( this ).html( $( this ).html().replace( "▼", "▲" ) );
                $( this ).next( ".aya-hidden" ).slideDown( "fast" );
            }
            else
            {
                $( this ).html( $( this ).html().replace( "▲", "▼" ) );
                $( this ).next( ".aya-hidden" ).slideUp( "fast" );
            }
        } );

        $( "#expand-all" ).click( function() {
            if ( $( this ).val() == "▼" )
            {
                $( this ).val( "▲" );
                $( ".aya-hidden" ).fadeIn( "fast" );
            }
            else
            {
                $( this ).val( "▼" );
                $( ".aya-hidden" ).fadeOut( "fast" );
            }
        } );
    } );
</script>

<p><input type="button" class="btn btn-primary form-control" value="▼" id="expand-all" title="Expand all"></p>

<div class="aya-expandable">
    <div class="aya-header">Speech Act suffixes ▼</div>
    <div class="aya-hidden">
        <table class="table">
            <thead>
                <tr>
                    <th>Láadan</th><th>English</th>
                </tr>
            </thead>
            <tbody>
                <tr><td> -d </td><td> speech act suffix: said in anger </td></tr>
                <tr><td> -da </td><td> speech act suffix: said in jest </td></tr>
                <tr><td> -de </td><td> speech act suffix: said in narrative </td></tr>
                <tr><td> -di </td><td> speech act suffix: said in teaching </td></tr>
                <tr><td> -du </td><td> speech act suffix: said in poetry </td></tr>
                <tr><td> -lan </td><td> speech act suffix: said in celebration </td></tr>
                <tr><td> -li </td><td> speech act suffix: said in love </td></tr>
                <tr><td> -th </td><td> speech act suffix: said in pain </td></tr>
                <tr><td> -ya </td><td> speech act suffix: said in fear </td></tr>
            </tbody>
        </table>
    </div>
</div>

<div class="aya-expandable">
    <div class="aya-header">Pronouns ▼</div>
    <div class="aya-hidden">
        <table class="table">
            <thead>
                <tr>
                    <th>Láadan</th><th>English</th>
                </tr>
            </thead>
            <tbody>
                <tr><td> le </td><td> first person singular (I, me) </td></tr>
                <tr><td> lezh </td><td> first person plural: several (We) </td></tr>
                <tr><td> len </td><td> first person plural: many (We) </td></tr>
                <tr><td> ne </td><td> second person singular (You) </td></tr>
                <tr><td> nezh </td><td> second person plural: several (You) </td></tr>
                <tr><td> nen </td><td> second person plural: many (You) </td></tr>
                <tr><td> be </td><td> third person singular (That, They) </td></tr>
                <tr><td> bezh </td><td> third person plural: several (They) </td></tr>
                <tr><td> ben </td><td> third person plural: many (They) </td></tr>
            </tbody>
        </table>
    </div>
</div>

<div class="aya-expandable">
    <div class="aya-header">Beneficiary Markers ▼</div>
    <div class="aya-hidden">
        <table class="table">
            <thead>
                <tr>
                    <th>Láadan</th><th>English</th>
                </tr>
            </thead>
            <tbody>
                <tr><td> -da </td><td> beneficiary: voluntarily </td></tr>
                <tr><td> -dá </td><td> beneficiary: by force </td></tr>
                <tr><td> -daá </td><td> beneficiary: accidentally </td></tr>
                <tr><td> -dáa </td><td> beneficiary: obligatorily </td></tr>
                <tr><td> -dan </td><td> beneficiary: with pleasure </td></tr>
                <tr><td> -den </td><td> beneficiary: neutral </td></tr>
            </tbody>
        </table>
    </div>
</div>

<div class="aya-expandable">
    <div class="aya-header">Degree Markers ▼</div>
    <div class="aya-hidden">
        <table class="table">
            <thead>
                <tr>
                    <th>Láadan</th><th>English</th>
                </tr>
            </thead>
            <tbody>
                <tr><td> -hal </td><td> unusual degree </td></tr>
                <tr><td> -hel </td><td> trivial/slight degree </td></tr>
                <tr><td> -hele </td><td> troublesome degree (negative) </td></tr>
                <tr><td> -hil </td><td> minor degree </td></tr>
                <tr><td> -hile </td><td> severe degree (negative) </td></tr>
                <tr><td> -hul </td><td> extreme degree </td></tr>
                <tr><td> -hule </td><td> intolerable degree (negative) </td></tr>
                <tr><td> -háalish </td><td> extraordinary degree </td></tr>
            </tbody>
        </table>
    </div>
</div>

<div class="aya-expandable">
    <div class="aya-header">Duration Markers ▼</div>
    <div class="aya-hidden">
        <table class="table">
            <thead>
                <tr>
                    <th>Láadan</th><th>English</th>
                </tr>
            </thead>
            <tbody>
                <tr><td> na- </td><td> to start-to-VERB </td></tr>
                <tr><td> ná- </td><td> to continue-to-VERB </td></tr>
                <tr><td> ne- </td><td> to repeat-doing-VERB </td></tr>
                <tr><td> no- </td><td> to finish-VERB </td></tr>
                <tr><td> nó- </td><td> to cease-to-VERB </td></tr>
                <tr><td> thé- </td><td> to be-about-to-VERB (at any minute) </td></tr>
                <tr><td> thée- </td><td> to be-about-to-VERB (potentially) </td></tr>
                <tr><td> thó- </td><td> to have-just-VERBED </td></tr>
            </tbody>
        </table>
    </div>
</div>

<div class="aya-expandable">
    <div class="aya-header">Possession Markers ▼</div>
    <div class="aya-hidden">
        <table class="table">
            <thead>
                <tr>
                    <th>Láadan</th><th>English</th>
                </tr>
            </thead>
            <tbody>
                <tr><td> -tha </td><td> by reason of birth </td></tr>
                <tr><td> -the </td><td> for unknown/unacknowledged reason </td></tr>
                <tr><td> -thi </td><td> by reason of chance </td></tr>
                <tr><td> -tho </td><td> other (purchase, gift, law) </td></tr>
                <tr><td> -thu </td><td> partitive (false possessive) </td></tr>
            </tbody>
        </table>
    </div>
</div>

<div class="aya-expandable">
    <div class="aya-header">State of Consciousness ▼</div>
    <div class="aya-hidden">
        <table class="table">
            <thead>
                <tr>
                    <th>Láadan</th><th>English</th>
                </tr>
            </thead>
            <tbody>
                <tr><td> -iyon </td><td> ecstasy </td></tr>
                <tr><td> -ib </td><td> deliberately shut off to all feeling </td></tr>
                <tr><td> -ihed </td><td> in a sort of shock; numb </td></tr>
                <tr><td> -tha </td><td> linked empathetically with others </td></tr>
                <tr><td> -o </td><td> in meditation </td></tr>
                <tr><td> -óo </td><td> in hypnotic trance </td></tr>
                <tr><td> -imi </td><td> in bewilderment/astonishment, positive </td></tr>
                <tr><td> -imilh </td><td> in bewilderment/astonishment, negative </td></tr>
            </tbody>
        </table>
    </div>
</div>

<div class="aya-expandable">
    <div class="aya-header">Other Case Markers ▼</div>
    <div class="aya-hidden">
        <table class="table">
            <thead>
                <tr>
                    <th>Láadan</th><th>English</th>
                </tr>
            </thead>
            <tbody>
                <tr><td> -de </td><td> source; from </td></tr>
                <tr><td> -di, -dim </td><td> goal; to </td></tr>
                <tr><td> dó- </td><td> to cause-to-VERB </td></tr>
                <tr><td> du- </td><td> to try-to-VERB </td></tr>
                <tr><td> dúu- </td><td> to try-in-vain-to-VERB </td></tr>
                <tr><td> -ha </td><td> location marker (place in space) </td></tr>
                <tr><td> -ya </td><td> location marker (place in time) </td></tr>
                <tr><td> -mu </td><td> path marker </td></tr>
                <tr><td> -nal </td><td> manner marker (how one does something) </td></tr>
                <tr><td> -nan </td><td> by means of marker (an instrument used to do something) </td></tr>
                <tr><td> -wan </td><td> cause marker (for the purpose of) </td></tr>
                <tr><td> -wáan </td><td> reason marker (because of) </td></tr>
            </tbody>
        </table>
    </div>
</div>

<div class="aya-expandable">
    <div class="aya-header">Special Grammar ▼</div>
    <div class="aya-hidden">
        <table class="table">
            <thead>
                <tr>
                    <th>Láadan</th><th>English</th>
                </tr>
            </thead>
            <tbody>
                <tr><td> -háa </td><td> embed marker: relative clause </td></tr>
                <tr><td> -hé </td><td> embed marker: embed one statement inside another </td></tr>
                <tr><td> -hée </td><td> embed marker: embed a question </td></tr>
                <tr><td> me- </td><td> plural marker (for verbs) </td></tr>
                <tr><td> wo- </td><td> adjective marker </td></tr>
                <tr><td> -hóo </td><td> emphasis </td></tr>
                <tr><td> -i </td><td> diminutive suffix </td></tr>
                <tr><td> -lh- </td><td> bad/evil (intentionally) </td></tr>
                <tr><td> -yóo- </td><td> reflexive (myself, yourself) </td></tr>
                <tr><td> -(e)th </td><td> direct object marker </td></tr>
                <tr><td> ra- </td><td> non-, not </td></tr>
            </tbody>
        </table>
    </div>
</div>

<div class="aya-expandable">
    <div class="aya-header">Other Affixes ▼</div>
    <div class="aya-hidden">
        <table class="table">
            <thead>
                <tr>
                    <th>Láadan</th><th>English</th>
                </tr>
            </thead>
            <tbody>
                <tr><td> -á </td><td> suffix for doer </td></tr>
                <tr><td> á- </td><td> prefix for infant/baby </td></tr>
                <tr><td> háa- </td><td> prefix for child/youth </td></tr>
                <tr><td> yáa- </td><td> prefix for teenager </td></tr>
                <tr><td> -id </td><td> suffix for male </td></tr>
                <tr><td> -báa- </td><td> interrogative (who, what, when) </td></tr>
                <tr><td> e- </td><td> science of </td></tr>
                <tr><td> é- </td><td> potential </td></tr>
                <tr><td> ro- </td><td> prefix for wild </td></tr>
                <tr><td> -rada </td><td> against (to lean against something) </td></tr>
            </tbody>
        </table>
    </div>
</div>

